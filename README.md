# qcc-ide README

This is the README for extension "qcc-ide". including the following sections.

# Overview

Describe specific features of extension. Image paths are relative to this README file.

One overview UI screenshot:

![Figure 1-1](./media/tutorial/basic_usage/simple_setup_UI.png)

# Known Issues

1. without rich config
2. can not add new file in exist project
5. do not test on linux

# Release Notes

This is just internal release
1. The download sdk and tools will be in ${HOME}/.qcc_ide
2. The download file tree is like: 
![Figure 1-2](./media/tutorial/basic_usage/file_tree.png)

# 0.5.0
1. Add  M4 ramdump support
2. Add OpenOCD support
3. Add disassemble support
4. Add auto detect download port
5. Auto open QPST directory when import ramdump
   
# 0.4.0
1. add read memory command
2. add memory view in run and debug
3. optimize the interaction logical of gdb backend and vscode front, let it faster
4. add data watch point for local variable
5. choose port randonly of gdb backend to improve stablitiy

known issue:
1. can not set data watch point for gloabal variable

# 0.3.0
1. can download sdk, toolchain, python and jliink from IDE
2. can support sdk v3.5 and sdk v3.4
3. add header file intelliSense
4. add user login when download sdk
5. change name from QCA-IDE to QCC-IDE
6. enable create new project from sdk example demo
7. get sdk installed status from user space
8. If update template configure file in IDE, will also update configure file in user space if has


# 0.2.0
## Changelog:
1.	Add debug feature:
      -   Breakpoint
      -   Back trace
      -   Step in/out
      -   Step over
      -   Local variable, argument variable, and register watch;
      -   Add variable watch
      -   Reset debug
      -   Pause debug
      -   Stop debug

2. Change serialport with TS to pyserial 3.5
3. Change -data-evaluate-expression to -var-create

4.    Add welcome page:
      -   Auto create qccIdeProjConfig.json

## Bug Fix:
1.	Use cmd.exe as default executor


# 0.1.0

Added for QCA402X
1. build bsp & demo
2. flash demo
3. console demo
4. simple config bsp & demo.

-----------------------------------------------------------------------------------------------------------
**Enjoy!**
