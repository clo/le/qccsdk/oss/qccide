# Note:
1. File in this folder is the config file of what need to be downloaded by IDE, This folder will be copy to user space
2. Subfolder in ./chip folder should be chip name, it maintains the sdk version of the chip
3. Jlinserver.json and toolchain.json .etc in ./tools is shared by all chip
4. download_install.py download toolchain, jlinkserver , and others through the "dependent_tool" in SDK.json.
5. If sdk or tools is exist, it will not download again 