# -*- encoding: utf-8 -*-
'''
Filename         :download_jlink.py
Description      :
Time             :2021/09/08 13:56:20
Author           :sww
Version          :1.0
'''

# /* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import requests
import os
import logging

format = "%(asctime)s: %(message)s"
logging.basicConfig(format=format, level=logging.INFO,
                    datefmt="%H:%M:%S")


form_data = {
    'accept_license_agreement': 'accepted',
    'submit': 'Download software'
}

def download_jlink(url,download_filename):
    """
    @description  :
    ---------
    @param  :
    download_filename like : .qcc_ide/Tools/Jlink/v6.72e/JLink_Windows_V672e.exe
    -------
    @Returns  :
    -------
    """

    with requests.Session() as session:
        #the website sets the cookies first
        req1 = session.get(url)
        #Request again to download
        req2 = session.post(url,data=form_data, stream=True)

        content_size = int(req2.headers['content-length'])
        size = 0 
        with open(download_filename, "wb") as f:
            for chunk in req2.iter_content(chunk_size=4096): 
                # If you have chunk encoded response uncomment if
                # and set chunk_size parameter to None.
                #if chunk: 
                size += len(chunk) 
                print('Downloaded: {:.2%}, total size: {:.2f} Mb' .format(size/content_size,content_size/1024/1024), end='\r')
                f.write(chunk)