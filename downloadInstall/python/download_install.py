# -*- encoding: utf-8 -*-
'''
Filename         :download.py
Description      :This file download and install sdk and tools according sdk.json in specific chip
Time             :2021/09/01 12:25:24
Author           :sww
Version          :1.0
'''

# /* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import requests
from tqdm import tqdm
import time
import logging
import argparse
import os
import json
import platform
from pathlib import Path
import subprocess
import shlex
import zipfile
from zipfile import BadZipFile 
import sys
import errno
import shutil
import stat
import re
import py7zr
import hashlib

import download_jlink 

format = "%(asctime)s: %(message)s"
logging.basicConfig(format=format, level=logging.INFO,
                    datefmt="%H:%M:%S",filemode='w')


TOOL_RELATIVE_PATH_TO_SDK_PATH = '../../Tools'
SDK_RELATIVE_PATH_TO_TOOL_PATH = '../../Sdks'

ide_name = '.qcc_ide'
cfg_files_dir = str(Path.home() /ide_name/"CfgFiles/download")
sdk_scope_env_file_name = 'sdkScopeEnv.json'
sdk_dnld_env_file_name = 'sdkDnldEnv.json'
sdk_user_name =''
sdk_password =''
sdk_dir = ''
downloads_path = ''
max_attempts = 2
cfg_download_file_path = ''
cfg_sdk_status_file_path = ''
cfg_tools_status_file_path = ''
pythonRequirements = ''
sdkInstalled = False

http_proxy = os.environ.get('http_proxy')
https_proxy = http_proxy

class ToolDownloaderror(RuntimeError):
    def __init__(self, arg):
        self.args = arg


# see https://stackoverflow.com/questions/1213706/what-user-do-python-scripts-run-as-in-windows
def handleRemoveReadonly(func, path, exc):
    os.chmod(path, stat.S_IWRITE)
    func(path)

def cfgPython(path,pth_name):
    logging.info('cfgPython path is %s'%path)
    logging.info('pth_name is %s'%pth_name)
    
    if(platform.system()=='Windows'):
        result = subprocess.run(["powershell", "-Command", "Add-Content -Path ./{0} -Value 'import site'".format(pth_name)],cwd=path)

        if not os.path.exists(os.path.join(path, 'get-pip.py')):
            subprocess.run(["powershell", "-Command", "Invoke-WebRequest -Uri https://bootstrap.pypa.io/get-pip.py -OutFile get-pip.py"],cwd=path)
            subprocess.run(["powershell", "-Command", "./python ./get-pip.py "],cwd=path)
        
        global pythonRequirements
        if pythonRequirements:
            logging.info('pythonRequirements is %s'%pythonRequirements)
            subprocess.run(["powershell", "-Command", "./python -m pip install {0}".format(' '.join(pythonRequirements))],cwd=path)

    else:
        pass

def download(response, file_size, download_filename):
    if file_size != 0 and os.path.exists(download_filename):
        open_type = 'ab'
        initial_size = os.path.getsize(download_filename)
    else:
        open_type = 'wb' 
        initial_size = 0

    with open(download_filename, open_type) as file:
        # use tqdm to indicate download progress
        with tqdm(total=file_size, initial=initial_size, unit='B', unit_scale=True, unit_divisor=1024, desc=os.path.basename(download_filename)) as bar:
            for data in response.iter_content(chunk_size=1024):
                file.write(data)
                bar.update(len(data))

def check_file_integrity(md5='', file_size=0, download_filename='') -> bool:
    if download_filename == '' or (not os.path.exists(download_filename)):
        return False
    if md5.lower() not in ['', 'null']:
        return md5 == hashlib.md5(open(download_filename, 'rb').read()).hexdigest()
    if file_size == 0:
        logging.info('cannot acquire file_size, skipping checking file integrity')
        # Do not block other download process
        return True
    else:
        return file_size == os.path.getsize(download_filename)

def get_download_file_size(url, proxies) -> int:
    response = requests.get(url, stream=True, proxies=proxies, verify=False)
    ret = int(response.headers.get('content-length', 0)) if response.status_code == 200 else 0
    response.close()
    return ret

def download_file(url, downloads_path, proxy=False, md5 = '', download_filename='', need_login = False):
    """
    @description  :
    see https://stackoverflow.com/questions/16694907/download-large-file-in-python-with-requests
    ---------
    @param  :
    downloads_path like:'C:\\Users\\user\\.qcc_ide\\Sdks\\v3.5_4020+cdb+sdk\\../../../Tools\\GNU Tools ARM Embedded\\8 2019-q3-update'
    
    -------
    @Returns  :
    -------
    """

    download_filename = os.path.join(downloads_path,download_filename)
    logging.info('Start download to %s ...'%download_filename)
    logging.info('Download URI is %s '%url)

    # create directory first
    if not os.path.exists(downloads_path):
        try:
            os.makedirs(downloads_path)
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise ToolDownloaderror('Directory created failed.')
    else:
        logging.info('%s exist. ' % downloads_path )

    # download jlink
    if 'JLink' in download_filename:
        logging.info('Start download Jlink...')
        download_jlink.download_jlink(url,download_filename)
        return download_filename
    
    logging.info('http_proxy is %s in system environment. ' % http_proxy )
    if proxy:
        # set proxy
        proxies = {
            "http": http_proxy,
            "https": https_proxy
        }
        logging.info('Download %s with proxy %s. ' % (download_filename, http_proxy) )
    else:
        proxies = None
        logging.info('Download %s without proxy may fail. ' % download_filename )

    headers = None
    file_size = get_download_file_size(url, proxies)
    file_integral = False

    for amp in range(max_attempts) :
        if os.path.exists(download_filename):
            downloaded_size = os.path.getsize(download_filename)
            headers = {'Range': f'bytes={downloaded_size}-'}

        response = requests.get(url, headers=headers, stream=True, proxies=proxies, verify=False)

        if response.status_code == 200 or response.status_code == 206:
            download(response, file_size, download_filename)
            response.close()
            file_integral = check_file_integrity(md5, file_size, download_filename)
            if file_integral:
                break
    
    if not file_integral:
        logging.error("Download failed after %d attempts, please check the proxy and network stability" %(max_attempts))
        return ""

    logging.info("Download successfully")
    return download_filename

def git_clone(git_cmd, need_login,target_path):
    """
    @description  :
    ---------
    @param  :
    git_cmd like : git clone -b r00001.5a.429190.1.434929.1 --depth 1 https://chipmaster2.qti.qualcomm.com/home2/git/qualcomm/qca4020-or-3-4_qca_oem_sdk-cdb.git

    need_login : whether need log in

    target_path: the download target path
    -------
    @Returns  :
    -------
    """
    # create directory first
    if not os.path.exists(target_path):
        try:
            os.makedirs(target_path)
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise ToolDownloaderror('Directory created failed.')
    else:
        logging.info('%s exist. ' % target_path )
        return
    
    #  fill login_info
    if need_login and sdk_user_name and sdk_password:
        auth_info = '//' + sdk_user_name + ':' + sdk_password + '@'
        git_cmd = re.sub('//',auth_info,git_cmd)

    args = shlex.split(git_cmd)
    process = subprocess.Popen(args, stdout=sys.stdout, stderr=sys.stdout,cwd=target_path)
    (stdout,stderr) = process.communicate()
    
    
def download_and_install(cmd_url = None , unzip = 'false', need_exec = 'false', need_login = True, path = str(Path.home() / "Downloads"/ide_name),proxy=False, md5 = '', download_filename='', tool_env ={}, use_git='false'):
    """
    @description  :
    ---------
    @param  :
    need_login  : whether need log in
    path: the download target path
    -------
    @Returns  :
    -------
    """
    if cmd_url == 'null':
        # raise RuntimeError('url is empty.')
        return

    if use_git == 'true':
        # TODO check git is exist
        git_clone(cmd_url,need_login,path)

    else:
        # just download file here
        downloads_file_path = download_file(cmd_url, path, proxy, md5, download_filename, need_login)

        if (downloads_file_path == None or downloads_file_path == ""):
            # raise RuntimeError("Cannot download %s tool" %(download_filename))
            logging.error("Failed to download %s" %(cmd_url))
            logging.error('''
            ===========================================
            Install SDK dependencies Failed. Please follow steps to reinstall:
                1.  Check the proxy and network stability
                2.  Press F1 in VSCode
                3.  Input "QCC-IDE:Install SDK Dependencies" and enter
            ===========================================
                        ''')
            sys.exit(1)

        # process downloaded file
        if unzip == 'true':
            if downloads_file_path.split('.')[-1] == "zip":
                # unzip first
                # see https://stackoverflow.com/questions/3451111/unzipping-files-in-python
                with zipfile.ZipFile(os.path.abspath(downloads_file_path), 'r') as zip_ref:
                    logging.info('start extract %s ' %(downloads_file_path))
                    zip_ref.extractall(os.path.abspath(path))
                    logging.info('extract %s ended' %(downloads_file_path))
            
            if downloads_file_path.split('.')[-1] == "7z":
                with py7zr.SevenZipFile(os.path.abspath(downloads_file_path), 'r') as archive:
                    logging.info('start extract %s ' %(downloads_file_path))
                    archive.extractall(os.path.abspath(path))
                    logging.info('extract %s ended' %(downloads_file_path))

            # TODO:delete the zip file download above

        if need_exec == 'true':
            logging.info('executable file is ' + downloads_file_path)
            # seehttps://stackoverflow.com/questions/6977215/os-system-to-invoke-an-exe-which-lies-in-a-dir-whose-name-contains-whitespace
            if os.path.exists(downloads_file_path):
                os.system('"' + downloads_file_path + '"')
        
        #  if tool is python, may need install pip and normal lib
        # see https://pip.pypa.io/en/stable/installation/
        # https://stackoverflow.com/questions/42666121/pip-with-embedded-python
        if 'python'in downloads_file_path:
            cfgPython(path,tool_env['pth_file_name'])
            



def download_tool_according_json(tool_json_data, tool_name, tool_ver, path,tool_env, tool_status_json_data):
    """
    @description  :
    ---------
    @param  :
    path like: .../.qcc_ide/Tools/arm_gcc/8 2019-q3-update, where to be downloaded
    -------
    @Returns  :
    -------
    """

    if 'uri' in tool_json_data:
        logging.info('')
        logging.info('===========================================')
        logging.info('Has tools download url for ' + tool_name)
        
        # update tool reference count
        if tool_name+'_'+tool_ver in tool_status_json_data:
            tool_status_json_data[tool_name+'_'+tool_ver] = tool_status_json_data[tool_name+'_'+tool_ver]  + 1
        else:
            tool_status_json_data[tool_name+'_'+tool_ver] = 1

        # update tool's export path
        for export_path in tool_json_data['export_paths']:
            export_path = os.path.join(path,'../../',export_path)
            tool_env['export_paths'].append(export_path)

        for key,value in tool_json_data.items():
            if key != 'name' and key != 'ver' and key != 'uri' and key != 'export_paths' :
                if value.startswith('^'): 
                    # This is a relative path
                    value = value[1:]
                    tool_env[key] = os.path.join(path,'../../',value)
                else:
                    tool_env[key] = value

        if(platform.system()=='Windows'):
            url = tool_json_data['uri']['win32']['url']
            unzip = tool_json_data['uri']['win32']['unzip']
            need_exec = tool_json_data['uri']['win32']['exe']
            proxy = tool_json_data['uri']['win32']['proxy']
            output_name = tool_json_data['uri']['win32']['outputName']
            md5 = tool_json_data['uri']['win32']['MD5']

        if(platform.system()=='Linux'):
            url = tool_json_data['uri']['linux-amd64']['url']
            unzip = tool_json_data['uri']['linux-amd64']['unzip']
            need_exec = tool_json_data['uri']['linux-amd64']['exe']
            proxy = tool_json_data['uri']['linux-amd64']['proxy']
            output_name = tool_json_data['uri']['linux-amd64']['outputName']
            md5 = tool_json_data['uri']['linux-amd64']['MD5']

        download_filename = os.path.join(path,url.split('/')[-1]) 
        if output_name:
            download_filename =  os.path.join(path,output_name) 

        if os.path.exists(download_filename):
            if proxy:
                # set proxy
                proxies = {
                    "http": http_proxy,
                    "https": https_proxy
                }
            else:
                proxies = None
            file_integral = check_file_integrity(md5, get_download_file_size(url, proxies), download_filename)
            if file_integral:
                # has been downloaded before, so just return
                logging.info(download_filename + ' has been downloaded before.')

                if 'python'in download_filename:
                    cfgPython(path,tool_env['pth_file_name'])

                return

        logging.info('Start download ' + tool_json_data['name'] +' '+ tool_json_data['ver'])

        try:
            download_and_install(url,unzip,need_exec,False,path,proxy,md5,os.path.basename(download_filename),tool_env)                                
        except Exception as result:
            tool_status_json_data[tool_name+'_'+tool_ver] = tool_status_json_data[tool_name+'_'+tool_ver] - 1
            raise ToolDownloaderror('Tool download failed.')
        else:
            logging.info('===========================================')
            logging.info('')
                            

def download_sdk_according_json(sdk_json_data, sdk_download_path):
    """
    @description  : download file according the json config
    ---------
    @param  : 
    sdk_json_data: json config data
    sdk_download_path: download and install sdk path, like .qcc_ide/Sdks/v3.3_cdb+sdk
    -------
    @Returns  :  none
    -------
    """
    
    if 'Sdk_Uri' in sdk_json_data:
        logging.info('Could download dependency for ' + sdk_json_data['Sdk_Name'] + ' with version ' + sdk_json_data['Sdk_Ver'])

        tool_status_json_data = {}
        if os.path.exists(cfg_tools_status_file_path):
            with open(cfg_tools_status_file_path,'r') as f:
                tool_status_json_data = json.load(f)

        # tool_env store the path to Tools, and will be writen to sdkScopeEnv.json in sdk root path
        tool_env = {}
        tool_env['export_paths'] = []
        
        sdk_root_path_from_download_path = sdk_json_data['sdk_root_path_from_download_path']
        if 'pythonRequirements' in sdk_json_data:
            global pythonRequirements
            pythonRequirements = sdk_json_data['pythonRequirements']

        need_login = False
        if sdk_json_data['need_log_in']=='true':
            #  download sdk here with login
            need_login = True
        else:
            # download sdk without login
            need_login = False

        if sdk_json_data['Sdk_Uri'] and sdk_json_data['Sdk_Uri'] != 'null':
            logging.info('Can install sdk '+ sdk_json_data['Sdk_Name'] + ' with version ' + sdk_json_data['Sdk_Ver'] )

            global sdkInstalled
            
            try:
                download_filename = ''
                if sdk_json_data['use_git'] == 'false':
                    download_filename =  os.path.join(sdk_download_path,sdk_json_data['outputName']) 
                    
                    if os.path.exists(download_filename):
                        # has been downloaded before, just continue
                        logging.info(download_filename + ' has been downloaded before.')
                    else:
                        download_and_install(sdk_json_data['Sdk_Uri'], sdk_json_data['unzip'], sdk_json_data['need_exec'], need_login, sdk_download_path, True, os.path.basename(download_filename),tool_env,sdk_json_data['use_git'])
            except Exception as err:
                logging.error('%s happened when download sdk.' % (err))
                # Note: have risk to delete SDK in user disk
                # shutil.rmtree(sdk_download_path,ignore_errors=True, onerror=handleRemoveReadonly)  
                sys.exit(1)
            else:
                sdkInstalled = True

        else:
            logging.info('SDK itself not installed.')
            # status file should not be "installed", just install tool dependency
            sdkInstalled = False


        # update json data in user space
        tool_env['sdk_root_path_from_download_path'] = sdk_json_data['sdk_root_path_from_download_path']

        # PYTHONPATH is relative path from sdk path
        tool_env['PYTHONPATH'] = []
        for python_path in sdk_json_data['PYTHONPATH']:
            tool_env['PYTHONPATH'].append(python_path)

        # demoChipBoard may exist
        if sdk_json_data['demoChipBoard']:
            tool_env['demoChipBoard'] = sdk_json_data['demoChipBoard']

        # =======================================================
        # download tools here according dependent_tool
        dependent_tool = sdk_json_data['Tools']

        for tool in dependent_tool:
            toolname = tool['name']
            tool_json_data = tool

            logging.debug('=====================')
            logging.debug(tool_json_data)
            logging.debug('=====================')

            tool_path = os.path.join(sdk_download_path,TOOL_RELATIVE_PATH_TO_SDK_PATH)
            tool_path = os.path.join(tool_path, toolname,tool['ver'])

            # according the json data to download file and install it if need
            try:
                download_tool_according_json(tool_json_data,toolname,tool['ver'],tool_path,tool_env,tool_status_json_data)
            except ToolDownloaderror as e:
                # shutil.rmtree(sdk_download_path,ignore_errors=True, onerror=handleRemoveReadonly)  
                shutil.rmtree(tool_path,ignore_errors=True, onerror=handleRemoveReadonly)
                if  e.args == 'System http_proxy not set':
                    sys.exit(2)
                else:
                    sys.exit(1)                                  
                        
        # see https://stackoverflow.com/questions/13949637/how-to-update-json-file-with-python
        # after sdk and tool installed then update sdk and tool download configure file
        with open(cfg_sdk_status_file_path,'w') as f:
            f.seek(0,0) # relocate to file begin
            sdk_status_json_data = {}
            if sdkInstalled:
                sdk_status_json_data['Sdk_Status'] = 'installed'
            json.dump(sdk_status_json_data, f,indent=4)
            f.truncate()

        # update tool json config file
        with open(cfg_tools_status_file_path,'w') as f:
            f.seek(0,0)
            json.dump(tool_status_json_data,f,indent=4)
            f.truncate()

        # generate sdkScopeEnv.json in sdk root path
        sdk_scope_cfg_file_path = os.path.join(sdk_download_path,sdk_root_path_from_download_path)
        if not os.path.exists(sdk_scope_cfg_file_path):
            os.makedirs(sdk_scope_cfg_file_path)
        sdk_scope_cfg_file_path = os.path.join(sdk_download_path,sdk_root_path_from_download_path,sdk_scope_env_file_name)
        with open(sdk_scope_cfg_file_path,'w+') as sdk_Scope_Cfg_file:
            json.dump(tool_env,sdk_Scope_Cfg_file,indent=4)   

        # sdk_download_env store the value that needed in sdk download path
        sdk_download_env = {}
        sdk_download_env['sdk_root_path_from_download_path'] = tool_env['sdk_root_path_from_download_path']  
        with open(os.path.join(sdk_download_path,sdk_dnld_env_file_name), 'w+') as sdk_Scope_Cfg_file_download:
            json.dump(sdk_download_env,sdk_Scope_Cfg_file_download,indent=4)  


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Used for download sdk and toolchain according json config, if just specify -t, then download the default sdk in json config.')
    parser.add_argument('-f', '--file', type=str,required=True,
                help='the download json file, like v3.5_SDK024+CDB.json (version_sdk_name).')
    parser.add_argument('-p', '--path', type=str,required=False,
                    help='where download sdk.')
    parser.add_argument('-m', '--max_attempts', type=int,required=False,
                    help='Maximum number of attempts to download a resource', default=2)
    parser.add_argument('-U', '--user', type=str,required=False,
                    help='user name to download sdk.')
    parser.add_argument('-P', '--password', type=str,required=False,
                    help='password to downalod sdk.')

    # Execute the parse_args() method
    args = parser.parse_args()

    if args.user:
        sdk_user_name = args.user

    if args.password:
        sdk_password = args.password
   
    if args.file:
        # file name should be like v3.5^SDK024+CDB.json (version_sdkName), 1.1.0^qccsdk_qcc730.json
        file_name = os.path.basename(args.file)
        file_name_arr = file_name.split('.')
        sdk_dir = ".".join(file_name_arr[:-1])

    downloads_path = str(Path.home() / ide_name/"Sdks"/sdk_dir)

    if args.path:
        downloads_path = args.path

    if args.file:
        # LIKE the json file in .qcc_ide/CfgFiles/download/URI/v3.5_SDK024+CDB.json
        cfg_download_file_path = args.file

    if args.max_attempts >= 1:
        max_attempts = args.max_attempts

    # the status json file should like .qcc_ide/CfgFiles/download/v3.5_SDK024+CDB.json
    cfg_sdk_status_file_path = os.path.join(cfg_files_dir,sdk_dir + '.json')
    cfg_tools_status_file_path = os.path.join(cfg_files_dir,'Tools_Status.json')

    with open(cfg_download_file_path,'r') as file:
        data = json.load(file)
        logging.debug('==========SDK uri json============')
        logging.debug(data)
        # according the json data to download file and install it if need
        download_sdk_according_json(data, downloads_path)



