# -*- encoding: utf-8 -*-
'''
Filename         :uninstall.py
Description      :This file unstall sdk and according tools, if "refference_count" in tool.json is not 1, 
                  just decrease this number
Time             :2021/09/02 12:22:33
Author           :sww
Version          :1.0
'''
# /* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */
