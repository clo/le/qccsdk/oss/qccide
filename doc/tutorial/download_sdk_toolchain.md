Note: The downloaded SDK will be placed in ${home}\.qcc_ide\Sdks

   like: C:\Users\wenwshen\\.qcc_ide\Sdks

   ## 1.1. Enter qcc ide quick access from side bar    
   ![img](../../media/tutorial/basic_usage/qcc_ide_quick_access.png)

   ## 1.2. Click "New Project" and then choose board name & sdk name, then click Open SDK
![img](../../media/tutorial/basic_usage/choose_board_sdk.png)

   ## 1.3. After SDK download success, Click "Open SDK" agagin to open demo list
![img](../../media/tutorial/basic_usage/demo_list.png)


   Download has successed.

