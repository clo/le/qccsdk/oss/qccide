Note: For project that not created by QCC IDE or opened by QCC IDE before

## 1.1 Enter qcc ide quick access from side bar    
   ![img](../../media/tutorial/basic_usage/qcc_ide_quick_access.png)

## 1.2 Chose valid SDK root path or demo path    

   ![img](../../media/tutorial/basic_usage/select_project.png)

## 1.3 Chose board  
![img](../../media/tutorial/basic_usage/open_choose_board.png)

## 1.4 Chose sdk  
![img](../../media/tutorial/basic_usage/open_choose_sdk.png)

Then the project will be opened.