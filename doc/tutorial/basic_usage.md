# Basic use of the extension.

In this tutorial you will learn how to use the basic commands of this extension to develop your application with Qualcomm IOE devices.

To use QCC IDE, need [python3.7](https://www.python.org/ftp/python/3.7.8/python-3.7.8-amd64.exe) and [git ](https://github.com/git-for-windows/git/releases/download/v2.35.1.windows.2/Git-2.35.1.2-64-bit.exe)installed.
Then install QCC IDE extension on vscode.

## 1.1. Open an QCC IDE project

a. Enter QCC IDE quick access from the side bar
![img](../../media/tutorial/basic_usage/qcc_ide_quick_access.png)

b. click "Open Exist Project" or "New Project" for your choose
  "Open Exist Project" can open from sdk root path or demo path

  "New Project" will download sdk first, and then create new project from download sdk's demo path.
  ![img](../../media/tutorial/basic_usage/choose_board_sdk.png)

  ![img](../../media/tutorial/basic_usage/demo_list.png)

## 1.2. After open project, see status bar to "compile" "flash" "console". When you hover your mouse on this icon, it will display help information. 
![img](../../media/tutorial/basic_usage/activate_successfully.png)  

## 1.3. Press 'F5' to start debug. Could set breakpoint and control debug process. Default is openocd gdb debug server. 
![img](../../media/tutorial/basic_usage/simple_debug_ui_1.png)