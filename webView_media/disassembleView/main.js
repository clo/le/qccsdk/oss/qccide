/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

// @ts-nocheck

// This script will be run within the webview itself
// It cannot access the main VS Code APIs directly.
(function () {
    
    const vscode = acquireVsCodeApi();

    // Handle messages sent from the extension to the webview
    window.addEventListener('message', event => {
        const message = event.data; // The json data that the extension sent
        switch (message.type) {
            case 'addDisassembleData':
                {
                    addDisassembleData(message.data);
                    break;
                }
            default:
                break;
        }
    });

    function updateDisassembleData(data) {
        const div = document.querySelector('div');
        div.innerHTML = '';
        let p1 = document.createElement('p');

        array = JSON.parse(data);
        for (let i=0;i<array.length; i++){
            
            let srcLine = array[i]['line'];
                       
            if(srcLine){
                p1.textContent += srcLine  +  '\n';
            }

            line_asm_insn = array[i]['line_asm_insn'];
            for (let j = 0;j<line_asm_insn.length;j++){
                let funcname = line_asm_insn[j]['func-name'];
                p1.textContent += line_asm_insn[j]['address'] + ' :  ' + line_asm_insn[j]['inst'] + '\n';
            }
            p1.textContent += '\n';
                
            // p1.textContent = JSON.stringify(array[i],null,2);
        }
        div.appendChild(p1);

    }

    // display disassemble data that read
    /**  data like: 
     * [
        {
            line: "163 + ${code}",
            file: "..\\..\\src\\platform\\platform_demo.c",
            fullname: "C:\\Users\\wenwshen\\Documents\\Projects\\Quartz_261\\ioesw_proc\\quartz\\demo\\QCLI_demo\\src\\platform\\platform_demo.c",
            line_asm_insn: [
            {
                address: "0x0117b254",
                "func-name": "platform_demo_get_time",
                offset: "0",
                inst: "pusht{r4, r5, lr}",
            },
            {
                address: "0x0117b256",
                "func-name": "platform_demo_get_time",
                offset: "2",
                inst: "subtsp, #20",
            },
            ],
        },
        {
            line: "164 + ${code}",
            file: "..\\..\\src\\platform\\platform_demo.c",
            fullname: "C:\\Users\\wenwshen\\Documents\\Projects\\Quartz_261\\ioesw_proc\\quartz\\demo\\QCLI_demo\\src\\platform\\platform_demo.c",
            line_asm_insn: [
            ],
        },
        ...
        ]
  */
    function addDisassembleData(data){
        // process and format data
        
        updateDisassembleData(data);
    }

}());


