/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

// @ts-nocheck

// This script will be run within the webview itself
// It cannot access the main VS Code APIs directly.
(function () {
    
    const vscode = acquireVsCodeApi();


    // Handle messages sent from the extension to the webview
    window.addEventListener('message', event => {
        const message = event.data; // The json data that the extension sent
        switch (message.type) {
            case 'addMemData':
                {
                    addMemData(message.data);
                    break;
                }
            default:
                break;
        }
    });


    function updateMemData(data) {
        const div = document.querySelector('div');
        div.innerHTML = '';

        const p1 = document.createElement('p');
        p1.textContent = 'Begin address: ' + data['begin'];
        div.appendChild(p1);

        const p2 = document.createElement('p');

        p2.textContent = data['contents'].replace(/\s/g, '').replace(/(\w{2})(?=\w)/g, '$1 ');
        div.appendChild(p2);

        const p3 = document.createElement('p');
        p3.textContent = 'End address: ' + data['end'];
        div.appendChild(p3);

    }

    // display mem data that read
    // data like: {begin="0xbffff154",end="0xbffff15e",contents="01000000020000000300"}, is JSON
    function addMemData(data){
        // process and format data
        
        updateMemData(data);
    }

}());


