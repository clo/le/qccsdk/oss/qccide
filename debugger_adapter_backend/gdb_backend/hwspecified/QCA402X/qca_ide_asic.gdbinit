# Copyright (c) 2016-2023 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
# $QTI_LICENSE_QDN_SH$


# ONE-TIME when starting gdb
# https://sourceware.org/gdb/onlinedocs/gdb/Asynchronous-and-non_002dstop-modes.html
set target-async on

target remote localhost:3333
#target extended-remote ubuntu-dsl04:3333
monitor halt


set history filename output/gdb_history.log
set history save on
set print pretty on
set print object off
set print vtbl off
set pagination off
set output-radix 16

#Reset the chip
#set *(int*)0x50742094=4

source v2/v2_substitute_path_m4.gdb



#Load RAM symbols
symbol-file output/Quartz.elf

#Load ROM symbols
add-symbol-file ../../../../../bin/cortex-m4/IOE_ROM_IPT_IMG_ARNNRI_patched_syms.elf 0x10000

#Load SBL symbols
add-symbol-file ../../../../../build/tools/flash/output/sbl/sbl_img.elf

#Set the cookie to allow boot past SBL
set *(int*)0x10000484=0xDADBEDAD

#Set the value to disable deep sleep
set g_sleepAllowLowPowerModes =0
