# /* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import subprocess
import os.path
import platform
import os
import shlex
import sys
class jlink_server:
    '''
    jlink server class
    '''
    def __init__(self,jlink_options = None,path='qca_ide_debug.bat') -> None:
        # for future usage
        self.jlink_options = jlink_options
        self.path = path

    def run(self):
        if self.path and os.path.isfile(self.path):
            cmd = self.path
            try:
                JLINK_PATH = os.environ.get('JLINK_PATH')

                if JLINK_PATH == '':
                    if platform.system() == 'Windows':
                        JLINK_PATH = 'C:\\Program Files (x86)\\SEGGER\\JLink'            

                if JLINK_PATH != '':
                    # https://stackoverflow.com/questions/2231227/python-subprocess-popen-with-a-modified-environment
                    jlink_env = os.environ.copy()
                    jlink_env['JLINK_PATH'] = JLINK_PATH
                    subprocess.run(cmd,shell=False,timeout=5,env=jlink_env)
                else:
                    raise RuntimeError(
                        'The JLINK_PATH : %s is not validated',JLINK_PATH)

            except Exception as e:
                raise e
        else:        
            raise RuntimeError(
                        'The jlink options %s or jlink path %s is not validated',self.jlink_options,self.path)


class openocd_server:
    '''
    openocd server class
    '''
    def __init__(self,openocd_options = None,path='qcc_ide_openocd_0_11_0.cfg') -> None:
        # for future usage
        self.openocd_options = openocd_options
        self.path = path

    def run(self):
        if self.path and os.path.isfile(self.path):
            
            try:
           
                # https://stackoverflow.com/questions/2231227/python-subprocess-popen-with-a-modified-environment
                cmd = 'openocd' + ' -f ' + self.path
                # print(cmd)
                # should include openocd path in env
                openocd_env = os.environ.copy()
                cmds = shlex.split(cmd,posix="win" not in sys.platform)
                print(cmds)
                subprocess.Popen(cmds,env=openocd_env,shell=True)

            except Exception as e:
                raise e
        else:        
            raise RuntimeError(
                        'The openocd options %s or openocd path %s is not validated',self.openocd_options,self.path)
