# /* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

# ******* Socket server **************************************
BACKEND_PORT = 44433
BACKEND_TOTAL_TCP_CONNECTION = 5

# ******* GDB **************************************
# CMD
DEFAULT_GDB_LAUNCH_COMMAND = ["arm-none-eabi-gdb", "--interpreter=mi3"]
# Target states
TARGET_STATE_STOPPED = 1
TARGET_STATE_RUNNING = 2
# Target stop reasons
TARGET_STOP_ERROR = -1
TARGET_STOP_REASON_UNKNOWN = 0
TARGET_STOP_REASON_SIGINT = 1
TARGET_STOP_REASON_SIGTRAP = 2
TARGET_STOP_REASON_BP = 3
TARGET_STOP_REASON_WP = 4
TARGET_STOP_REASON_WP_SCOPE = 5
TARGET_STOP_REASON_STEPPED = 6
TARGET_STOP_REASON_FN_FINISHED = 7

# ******* GDB Server**************************************
# should not change the value
OPENOCD_SERVER = 'openocd'
JLINK_SERVER = 'jlink'
