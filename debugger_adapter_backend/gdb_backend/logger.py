# /* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import logging


def logger_init(log_name="Log", log_level=logging.WARNING, stream_handler=None, file_handler=None):
    """
    Parameters
    ----------
    log_name : str
    log_level : int
    stream_handler : logging.StreamHandler
    file_handler : logging.FileHandler

    Returns
    -------
    logger
    """
    logger = logging.getLogger(log_name)
    if log_level:
        logger.setLevel(log_level)
    if stream_handler:
        logger.addHandler(stream_handler)
    if file_handler:
        logger.addHandler(file_handler)
    return logger
