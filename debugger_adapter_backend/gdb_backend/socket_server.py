# /* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import socket
import threading
from . import gdb
from . import defs
from . import logger
import logging
from . import defs
import json
import sys
import time
from queue import Queue

class socket_server:
    def __init__(self,port=defs.BACKEND_PORT,log_level=logging.INFO) -> None:
        self.port = port
        self.socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.backend_socket = None
        self.socket.bind(('localhost',self.port))
        self.close_socket = False
        self.so_lock = threading.Lock()
        self.log_level = log_level
        self.socket.listen(defs.BACKEND_TOTAL_TCP_CONNECTION)
        self.mi_cmd_queue = Queue()

        # create console handler and set level to debug
        self.ch = logging.StreamHandler()
        self.ch.setLevel(log_level)
        # create formatter
        formatter = logging.Formatter('\n%(asctime)s-%(name)s-%(levelname)s: %(message)s')
        # add formatter to ch
        self.ch.setFormatter(formatter)
        self._logger = logger.logger_init(log_name="dbgBackend",log_level=log_level,stream_handler=self.ch)
        

    def accept(self):
        self.backend_socket,self.client_addr = self.socket.accept()
        # self.backend_socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)

    def run(self,args = None):
        self.accept()

        self.gdb = gdb.gdb(args = args, log_level = self.log_level)
        self.gdb.start_gdb()
        
        self.gdbmi_process_thread = threading.Thread(target=self.process_mi_cmd)
        self.gdbmi_process_thread.start()

        self.gdbmi_rsp_thread = threading.Thread(target=self.gdb_mi_rsp)
        self.gdbmi_rsp_thread.start()

        self.so_cmd_thread = threading.Thread(target=self.so_rcv)
        self.so_cmd_thread.start()

        self._logger.info('gdb debug backend has started.')

    def process_mi_cmd(self):
        while True:
            cmd =  self.mi_cmd_queue.get()
            cmd = cmd.split('\n') 
            print('received mi cmd: ',cmd)
            '''
            cmd format:
            ==========
            cmd_content
            ==============================
            example:
            ==========
            -exec-interrupt --all
            '''
            for icmd in cmd:
                # sleep to let gdb run cmd
                time.sleep(0.1)
                if icmd:
                    if icmd =='SIGTRAP' :
                        if self.gdb._target_running():
                            self._logger.info('interrupt the target')
                            self.gdb._interrupt_target()
                    else:
                        if self.gdb._wait_target_stop():
                            self._logger.info('write_gdbmi: %s', icmd)
                            self.gdb._mi_cmd_write(icmd.strip())

    def so_rcv(self):
        while True:
            if self.backend_socket:
                try:
                    cmd = self.backend_socket.recv(1024)
                    cmd = cmd.decode().strip()
                    self.mi_cmd_queue.put(cmd)
          
                except socket.error as e:
                      self._logger.info('socket error %s', e)
                      break


    def gdb_mi_rsp(self):
        while True:
            time.sleep(0.1)
            self.rsp = self.gdb._mi_get_rsp()
            if self.rsp:
                for rec in self.rsp:
                    result, result_body = self.gdb._parse_gdbmi_rsp(rec)
                    if result:
                        if result == 'exit':
                            self.close_socket = True
                        result_body = json.dumps(result_body)
                        to_front = result + '==' + result_body + '\n' 
                        if self.backend_socket:
                            self.backend_socket.send(to_front.encode()) 
                            if self.close_socket:
                                time.sleep(0.020)
                                self._logger.info('closed socket.')
                                self.backend_socket.shutdown(socket.SHUT_WR)
                                sys.exit(0)

