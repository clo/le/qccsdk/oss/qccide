# /* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

from enum import IntEnum

class Modes(object):
    def __init__(self, modes_list=[]):
        for m in modes_list:
            setattr(self, m, m)

    @classmethod
    def get_dict(cls):
        public = {}
        for k, v in cls.__dict__.items():
            if k.startswith('_'):
                continue
            else:
                public[k] = v
        return public

    @classmethod
    def get_modes(cls):
        return list(cls.get_dict().values())

class GdbBackendModes(Modes):
    QCA402X = 'QCA402X'
    QCA4010 = 'QCA4010'
    QCA4004 = 'QCA4004'


class DaRunState(IntEnum):
    STOPPED = -2
    STOP_PREPARATION = -1
    UNKNOWN = 0
    RUNNING = 1
    READY_TO_CONNECT = 2
    CONNECTED = 3
    INITIALIZED = 4
    CONFIGURED = 5
    READY = 6


class DaStates(object):
    no_debug = False  # argument of a launch request
    gdb_started = False
