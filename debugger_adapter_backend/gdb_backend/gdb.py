# /* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import time
from .internal_class import GdbBackendModes,DaStates
import os
import tempfile
from . import logger
from . import gdb_server
from .defs import *
from pygdbmi.gdbcontroller import GdbController
import platform
import logging
from pprint import pformat
import signal

class gdb:
    """
    Gdb class
    """
    def __init__(self, args,
                 log_level=logging.DEBUG):

        self.args = args
        self.build_dir = args.build_dir
        self._gdbmi = None
        self._gdbcmd = args.gdb_cmd
        self._gdb_server = None
        self._curr_frame = None
        self._rsp_cache = []
        self._target_state = TARGET_STATE_STOPPED
        self._target_stop_reason = TARGET_STOP_ERROR
        self.tmo_scale_factor = 1
        self.log_level = log_level
        self._gdb_args = None
        self.state = DaStates()  # type: DaStates

        # create console handler and set level to debug
        self.ch = logging.StreamHandler()
        self.ch.setLevel(log_level)
        # create formatter
        formatter = logging.Formatter('%(asctime)s-%(name)s-%(levelname)s: %(message)s')
        # add formatter to ch
        self.ch.setFormatter(formatter)

    def create_gdb(self,chip_name=None,
               gdb_path=None,
               remote_target=None,
               extended_remote_mode=None,
               gdb_log_file=None,
               log_level=None,
               log_stream_handler=None,
               log_file_handler=None):
        """
            Creates GDB descriptor for specified chip name.

            Returns
            -------
            Gdb descriptor for specified chip name
        """
        gdb_init_args = {}
        if chip_name is not None:
            gdb_init_args['chip_name'] = chip_name
        if gdb_path is not None:
            gdb_init_args['gdb_path'] = gdb_path
        if remote_target is not None:
            gdb_init_args['remote_target'] = remote_target
        if extended_remote_mode is not None:
            gdb_init_args['extended_remote_mode'] = extended_remote_mode
        if gdb_log_file is not None:
            gdb_init_args['gdb_log_file'] = gdb_log_file
        if log_level is not None:
            gdb_init_args['log_level'] = log_level
        if log_stream_handler is not None:
            gdb_init_args['log_stream_handler'] = log_stream_handler
        if log_file_handler is not None:
            gdb_init_args['log_file_handler'] = log_file_handler
        self._logger = logger.logger_init(log_name="gdb",log_level=log_level,stream_handler=log_stream_handler,file_handler=log_file_handler)
        return gdb_init_args

    def is_gdb_exist(self):
        """
        Check if there is gdbmi instance for using
        -------
        bool
            Result
        """
        return self._gdbmi is not None

    def start_gdb(self):  
            """
            Starting GDB (both client and server) 
            and write the result into state.gdb_started attribute
            """
            try:
                if not self.is_gdb_exist():
                    # below is default support device
                    # if self.args.dev == GdbBackendModes.QCA402X:

                    if not self._gdb_args:
                        self._gdb_args = self.create_gdb(chip_name = self.args.dev,
                                                            log_level = self.log_level,
                                                            log_stream_handler=self.ch,
                                                            gdb_log_file=os.path.join(tempfile.gettempdir(), "gdb_quartz_proc.log")
                                                )
                    
                    if self.args.server_type == OPENOCD_SERVER:
                        # start gdb openocd server
                        openocd_server_cfg_path = self.args.initFile

                        if platform.system().lower() =='linux':
                            self._logger.error("openocd_server is not supported in linux for now")
                            raise RuntimeError("openocd_server is not supported in linux for now")

                        self._gdb_server = gdb_server.openocd_server(path = openocd_server_cfg_path)
                        self._gdb_server.run()
                        self._logger.info("Started openocd_server for : %s" , self._gdb_args['chip_name'])

                    elif self.args.server_type == JLINK_SERVER:
                        # start gdb jlink server
                        jlink_server_script_path = None
                        if platform.system().lower() =='windows':
                            jlink_server_script_path = os.path.join(self.build_dir,'qca_ide_debug.bat')
                        elif platform.system().lower() =='linux':
                            self._logger.error("jlink_server is not supported in linux for now")
                            raise RuntimeError("jlink_server is not supported in linux for now")

                        self._gdb_server = gdb_server.jlink_server(path = jlink_server_script_path)
                        self._gdb_server.run()
                        self._logger.info("Started jlink_server for : %s" , self._gdb_args['chip_name'])
                    else:
                        raise RuntimeError('gdb server type only support Openocd and J-link')

                    # elif self.args.dev == GdbBackendModes.QCA4010:
                    #     if not self._gdb_args:
                    #         self._gdb_args = self.create_gdb(chip_name=self.args.dev,
                    #                                          log_level = self.log_level,
                    #                                          log_stream_handler=self.ch,
                    #                                          gdb_log_file=os.path.join(tempfile.gettempdir(), "gdb_ruby_proc.log")
                    #                             )
                    #     # TODO:start gdb jlink server

                    # elif self.args.dev == GdbBackendModes.QCA4004:
                    #     if not self._gdb_args:
                    #         self._gdb_args = self.create_gdb(chip_name=self.args.dev,
                    #                                          log_level = self.log_level,
                    #                                          log_stream_handler=self.ch,
                    #                                          gdb_log_file=os.path.join(tempfile.gettempdir(), "gdb_kingfisher_proc.log")
                    #                             )
                    #     # TODO:start gdb jlink server

                    # else:
                    #     self._logger.error("The gdb target %s is not supported." , self._gdb_args['chip_name'])
                    #     raise RuntimeError(
                    #         'The gdb target %s is not supported', self._gdb_args['chip_name'] )

                    self._logger.info("Created gdb for: %s" , self._gdb_args['chip_name'])
                    # wait gdb server start
                    time.sleep(1)  
                    # start gdb client
                    self._gdbmi = GdbController(command = self._gdbcmd)
                    self.state.gdb_started = True

            except Exception as e:
                raise e            

    def _mi_get_rsp(self):
        """
        Return: the List of parsed GDB responses
             like [{'type': 'notify', 'message': 'memory-changed', 'payload': {'thread-group': 'i1', 'addr': '0x10000484', 'len': '0x4'}, 'token': None, 'stream': 'stdout'}, {'type': 'result', 'message': 'done', 'payload': {'bkpt': {'number': '1', 'type': 'breakpoint', 'disp': 'keep', 'enabled': 'y', 'addr': '0x10047590', 'func': 'app_start', 'file': '..\\..\\src\\qcli\\pal.c', 'fullname': 'C:\\Users\\wenwshen\\Documents\\Projects\\Quartz_187\\ioesw_proc\\quartz\\demo\\QCLI_demo\\src\\qcli\\pal.c', 'line': '496', 'thread-groups': ['i1'], 'times': '0', 'original-location': 'app_start'}}, 'token': None, 'stream': 'stdout'}, {'type': 'result', 'message': 'running', 'payload': None, 'token': None, 'stream': 'stdout'}, {'type': 'notify', 'message': 'running', 'payload': {'thread-id': 'all'}, 'token': None, 'stream': 'stdout'}]
        """
        try:
            return self._gdbmi.get_gdb_response(timeout_sec=0,raise_error_on_timeout=False)
        except Exception as e:
            raise e

    def _mi_cmd_write(self,cmd):
        try:
            self._gdbmi.write(cmd, read_response=False)
        except Exception as e:
            raise e

    def _interrupt_target(self):
        if self._target_state == TARGET_STATE_RUNNING:
            self._mi_cmd_write('-exec-interrupt --all')
            self._logger.debug('send interrupt to gdb backend')
    
    def _on_notify(self,rec):
        result = None
        result_body = None
        if rec['message'] == 'stopped':
            self._target_state = TARGET_STATE_STOPPED
            self._curr_frame = rec['payload']['frame']
            if 'reason' in rec['payload']:
                if rec['payload']['reason'] == 'breakpoint-hit':
                    self._target_stop_reason = TARGET_STOP_REASON_BP
                elif rec['payload']['reason'] == 'watchpoint-trigger':
                    self._target_stop_reason = TARGET_STOP_REASON_WP
                    self._curr_wp_val = rec['payload']['value']
                elif rec['payload']['reason'] == 'watchpoint-scope':
                    self._target_stop_reason = TARGET_STOP_REASON_WP_SCOPE
                elif rec['payload']['reason'] == 'end-stepping-range':
                    self._target_stop_reason = TARGET_STOP_REASON_STEPPED
                elif rec['payload']['reason'] == 'function-finished':
                    self._target_stop_reason = TARGET_STOP_REASON_FN_FINISHED
                elif rec['payload']['reason'] == 'signal-received':
                    if rec['payload']['signal-name'] == 'SIGINT':
                        self._target_stop_reason = TARGET_STOP_REASON_SIGINT
                    elif rec['payload']['signal-name'] == 'SIGTRAP':
                        self._target_stop_reason = TARGET_STOP_REASON_SIGTRAP
                        self._logger.info('TARGET_STOP_REASON_SIGTRAP.')
                    else:
                        self._logger.warning('Unknown signal received "%s"!', rec['payload']['signal-name'])
                        self._target_stop_reason = TARGET_STOP_REASON_UNKNOWN
                else:
                    self._logger.warning('Unknown target stop reason "%s"!', rec['payload']['reason'])
                    self._target_stop_reason = TARGET_STOP_REASON_UNKNOWN
            else:
                self._target_stop_reason = TARGET_STOP_ERROR

        elif rec['message'] == 'running':
            self._target_state = TARGET_STATE_RUNNING
        
        result = rec['message']
        result_body = rec['payload']
        return result,result_body

    def _parse_gdbmi_rsp(self,rec):
        """
        rec: one record of  gdb mi rsp
            like {'type': 'result', 'message': 'done', 'payload': {'bkpt': {'number': '1', 'type': 'breakpoint', 'disp': 'keep', 'enabled': 'y', 'addr': '0x10047590', 'func': 'app_start', 'file': '..\\..\\src\\qcli\\pal.c', 'fullname': 'C:\\Users\\wenwshen\\Documents\\Projects\\Quartz_187\\ioesw_proc\\quartz\\demo\\QCLI_demo\\src\\qcli\\pal.c', 'line': '496', 'thread-groups': ['i1'], 'times': '0', 'original-location': 'app_start'}}, 'token': None, 'stream': 'stdout'}
        return:
            result, a string state message
            result_body, a dict message
        """
        result = None
        result_body = None
        old_state = self._target_state

        if rec['type'] == 'log':
            self._logger.debug('LOG: %s', pformat(rec['payload']))

        elif rec['type'] == 'console':
            self._logger.debug('CONSOLE: %s', pformat(rec['payload']))

        elif rec['type'] == 'target':
            self._logger.debug('TGT: %s', pformat(rec['payload']))

        elif rec['type'] == 'notify':
            self._logger.info('NOTIFY: %s %s', rec['message'], pformat(rec['payload']))
            result, result_body = self._on_notify(rec)

        elif rec['type'] == 'result':
            self._logger.info('RESULT: %s %s', rec['message'], pformat(rec['payload']))
            result = rec['message']
            result_body = rec['payload']

        return result, result_body


    def _wait_target_state(self, state,tmo=10):
        """
        Parameters
        ----------
        state : expect target state
        tmo : wait timeout
        Returns
        -------
        reason : the reason
        """
        end = time.time()
        if tmo is not None:
            end += tmo * self.tmo_scale_factor
        # _target_state updated by _parse_gdbmi_rsp()
        while self._target_state != state :
            if time.time() >= end:
                self._logger.info('target state: %s.', self._target_state)
                break
        return self._target_state 

    def _wait_target_stop(self):
        return self._wait_target_state(TARGET_STATE_STOPPED) == TARGET_STATE_STOPPED
 
    def _target_running(self):
        return self._target_state == TARGET_STATE_RUNNING
        