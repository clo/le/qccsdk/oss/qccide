# /* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

from gdb_backend import socket_server,defs

import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Used for gdb backend.')
    parser.add_argument('-d', '--dev', type=str,required=True,
                    help='chip family name for the device, like QCA402X.')
    parser.add_argument('-P', '--port', type=int,
                    help='backend server listening port, default is 44433.')
    parser.add_argument('-b', '--build_dir', type=str,required=True,
                    help='chip build path for the device, like quartz\\demo\\QCLI_demo\\build\\gcc.')
    parser.add_argument('-g', '--gdb_cmd', nargs ='+',type=str,required=True,
                    help='executable gdb + options, like \'arm-none-eabi-gdb --interpreter=mi3.\'')
    parser.add_argument('-s', '--server_type', type=str,required=True,default='openocd',
                    help='gdb debug server type, openocd or jlink')
    parser.add_argument('-f', '--initFile', type=str,required=True,default='',
                    help='gdb debug server init file for openocd or jlink')

    # Execute the parse_args() method
    args = parser.parse_args()
    args.gdb_cmd = args.gdb_cmd[0].split(' ')
    port = defs.BACKEND_PORT
    if args.port:
        port = args.port
    # Start the scoket server
    gdb_backend_server = socket_server.socket_server(port = port)
    gdb_backend_server.run(args)

