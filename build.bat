@echo off
call npm install
echo npm install completed

call vsce package --baseImagesUrl https://none

setlocal enabledelayedexpansion

set "current_directory=%CD%"
for %%I in ("%current_directory%") do set "current_directory_name=%%~nxI"
echo current_directory_name is: %current_directory_name%
rem Get the current directory of the script
set "scriptDir=%~dp0"
echo scriptDir is: !scriptDir!

set "buildDirCrm=%scriptDir%.."
echo buildDirCrm is: %buildDirCrm%

rem Set the output file path
set "outputFile=%scriptDir%BuildProducts.txt"

set "outputCrmFile=%buildDirCrm%\BuildProducts.txt"

rem Delete the existing output file
if exist "%outputFile%" del "%outputFile%"
if exist "%outputCrmFile%" del "%outputCrmFile%"

rem Iterate through all .vsix files in the current directory
for %%i in ("%scriptDir%*.vsix") do (
  rem Get the file name (without path)
  set "fileName=%%~nxi"
  
  rem Get the relative path and add the ./ prefix
  set "relativePath=%%~nxi"
  echo !relativePath!
  set "relativePath=!relativePath:%scriptDir%=!"
  set "relativeCrmPath=./%current_directory_name%/!relativePath!"
  set "relativePath=./!relativePath!"
  
  rem Write the file name and relative path to the output file
  echo !relativePath! >> "%outputFile%"
  echo !relativeCrmPath! >> "%outputCrmFile%"
)

echo Done! Output written to %outputFile%
echo Done! Output written to %outputCrmFile%

