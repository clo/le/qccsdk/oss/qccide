#!/bin/bash

#========================================================================
#Copyright (c) Qualcomm Innovation Center, Inc. All rights reserved.
#SPDX-License-Identifier: BSD-3-Clause-Clear
#
# QCCIDE integration entry
#========================================================================
cp /pkg/qct/software/npminstall/node_modules.tar.gz .
tar -zxvf node_modules.tar.gz
rm node_modules.tar.gz
source /usr/local/nvm/nvm.sh
npm -v
node -v
vsce package --baseImagesUrl https://none
mkdir -p ../../prebuilt_HY11
mkdir -p ../../prebuilt_HY11_ART
cp qcc-ide-extension-1.0.0.vsix  ../../prebuilt_HY11/
cp qcc-ide-extension-1.0.0.vsix  ../../prebuilt_HY11_ART/
