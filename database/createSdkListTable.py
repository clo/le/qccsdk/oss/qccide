# /* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import sqlite3
from sqlite3 import Error

# Connecting to sqlite
# connection object
connection_obj = sqlite3.connect('ide.db')

# cursor object
cursor_obj = connection_obj.cursor()

# Drop the table if already exists.
# cursor_obj.execute("DROP TABLE IF EXISTS SDKLIST")

# Creating table
table = """ CREATE TABLE BOARDLIST (
			Board VARCHAR(255) NOT NULL,
			Chip VARCHAR(255) NOT NULL,
			Sdk_Name VARCHAR(255) NOT NULL,
			Sdk_Intr VARCHAR(255) NOT NULL
			Sdk_Cfg VARCHAR(255) NOT NULL
		); 
		"""

cursor_obj.execute(table)


table = """ CREATE TABLE SDKLIST (
			Sdk_Name VARCHAR(255) NOT NULL,
			Sdk_Ver VARCHAR(255) NOT NULL,
			Sdk_Uri VARCHAR(255) NOT NULL,
			Tools VARCHAR(255) NOT NULL,
			Key_Value VARCHAR(255)
		); 
		"""

cursor_obj.execute(table)


table = """ CREATE TABLE TOOLLIST (
			Tool_Name VARCHAR(255) NOT NULL,
			Tool_Ver VARCHAR(255) NOT NULL,
			Tool_Uri VARCHAR(255) NOT NULL,
			Key_Value VARCHAR(255)
		);
		"""

cursor_obj.execute(table)


print("Table is Ready")

# Close the connection
connection_obj.close()
