# QCC IDE database guide
There are three table in this database, BOARDLIST, SDKLIST, TOOLLIST


## BOARDLIST TABLE
format:
| Board        | Chip      | Sdk_Name   | Sdk_Intr  | Sdk_Cfg   |
| ------------ | --------- | ---------  | --------- | --------- |
| Ehong_MW24   | QCA4024   | SDK024+CDB | ...       | ...       |

Board: the supported boardname
Chip: the supported chip name
Sdk_Name: the supported sdk name
Sdk_Intr: the general information of supported sdk
Sdk_Cfg: the general configure information of supported sdk 

Sdk_Cfg example:
```
{
    "cfg": {
        "1": "Used and updated by extension, you should not change it manually if you don't know why",
        "bspBuildScriptLinux": "build.sh",
        "bspBuildScriptWin": "build.cmd",
        "chipID": "4024",
        "chipTarget": "QCA402X",
        "chipVersion": "v2",
        "defaultDemo": "QCLI_demo",
        "demoBuildScriptLinux": "make",
        "demoBuildScriptWin": "build.bat",
        "demoChipBoard": "eh",
        "demoFlashScriptLinux": "qflash.py",
        "demoFlashScriptWin": "qflash.py",
        "demoPath": "./quartz/demo",
        "demoRTOS": "FreeRTOS",
        "elfName": "Quartz.elf",
        "entryFun": "app_start",
        "sdkConfigFile": "qccIdeProjConfig.json",
        "sdkPath": ""
    },
    "cpptoolProp": {
        "configurations": [
            {
                "browse": {
                    "limitSymbolsToIncludedHeaders": false,
                    "path": [
                        "${config:qualIoe.staticCfg.sdkPath}/include",
                        "${workspaceFolder}"
                    ]
                },
                "cStandard": "c11",
                "compilerPath": "${default}",
                "cppStandard": "c++17",
                "includePath": [
                    "${config:qualIoe.staticCfg.sdkPath}/include/**",
                    "${workspaceFolder}/**"
                ],
                "name": "QCC-IDE"
            }
        ],
        "version": 4
    }
}
```

Note about BOARDLIST table:
1. one Board should have different sdk, but will not have the same name SDK in BOARDLIST table
2. one SDK may be used for different board
3. one SDK should not  be used for different chip in BOARDLIST table
4. sdk name is format as "chip info" +" sdk info"
5. Sdk_Cfg in BOARDLIST  should be information that is general for different version of a specify sdk. Same of Sdk_Intr.



## SDKLIST TABLE 
format:
| Sdk_Name     | Sdk_Ver   | Sdk_Uri | Tools  | Key_Value |
| ------------ | --------- | ------- | ------ | --------- |
| 4020+cdb+sdk | v3.5      | ....    | ...    | ...       |

Sdk_Name: the supported sdk name
Sdk_Ver: the supported sdk version
Sdk_Uri: the supported sdk download uri with cmd
Tools: the dependency tools of according sdk
Key_Value: the specify configure information of according sdk 

Tools example:
```
"tool_name" : "version"
```
```
{
    "arm_gcc": "8 2019-q3-update",
    "jlink": "v6.72e",
    "openocd": "2017",
    "python": "3.7.8"
}
```

Key_Value example:
```
PYTHONPATH: the path that need export to python
demoPath: relative path of demo from sdk root path
demoRegex: demo folder name regex
need_exec: whether sdk need executate after download
need_log_in: whether need log in when download sdk
sdk_root_path_from_download_path: the sdk root path from download path
unzip: whether need unzip after download
use_git: whether use git to download sdk
ver_intr: the information of the specify sdk version
```
```
{
    "PYTHONPATH": "./build/scripts",
    "demoPath": "./quartz/demo",
    "demoRegex": ".*demo$",
    "need_exec": "false",
    "need_log_in": "true",
    "sdk_root_path_from_download_path": "./qca4020-or-3-5_qca_oem_sdk-cdb/target",
    "unzip": "false",
    "use_git": "true",
    "ver_intr": "fix some bug after  v3.4"
}
```

## TOOLLIST TABLE 
format:
| Tool_Name     | Tool_Ver             | Tool_Uri | Key_Value  |
| ------------  | -------------------  | -------  | ------     |
| arm_gcc       | 8 2019-q3-update     | ....     | ...        |

Tool_Name: the supported tool name
Tool_Ver: the supported tool version
Tool_Uri: the supported tool download uri 
Key_Value: the specify configure information of according tool

Tool_Uri example:
```
{
    "linux-amd64": {
        "MD5": " 6341f11972dac8de185646d0fbd73bfc",
        "exe": "false",
        "unzip": "true",
        "url": "https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/8-2019q3/RC1.1/gcc-arm-none-eabi-8-2019-q3-update-linux.tar.bz2"
    },
    "win32": {
        "MD5": "5fa382a547abe0b0d5c0a6e9eaa75c7b",
        "exe": "false",
        "unzip": "true",
        "url": "https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/8-2019q3/RC1.1/gcc-arm-none-eabi-8-2019-q3-update-win32.zip"
    }
}
```

Key_Value example:
```
{
    "export_paths": [
        [
            "arm_gcc",
            "8 2019-q3-update",
            "bin"
        ]
    ]
}
```


