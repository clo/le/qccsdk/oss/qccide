/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import * as vscode from "vscode";
import * as check from "../utils/check";


export function readParameter (
    param: string,
    scope?: vscode.ConfigurationScope
  ) {
    const paramValue = vscode.workspace
      .getConfiguration()
      .get(param);
    if (typeof paramValue === "undefined") {
      return undefined ;
    }
    return paramValue;
}

export async function writeParameter(
    param: string,
    newValue:any,
    target: vscode.ConfigurationTarget,
    wsFolder?: vscode.WorkspaceFolder
){

    if (target === vscode.ConfigurationTarget.WorkspaceFolder) {
      if (wsFolder) {
        return await vscode.workspace
          .getConfiguration("", wsFolder.uri)
          .update(param, newValue, target);
      } else {
        const workspaceFolder = await vscode.window.showWorkspaceFolderPick({
          placeHolder: `Pick Workspace Folder to which ${param} should be applied`,
        });
        if (workspaceFolder) {
          return await vscode.workspace
            .getConfiguration("", workspaceFolder.uri)
            .update(param, newValue, target);
        }
      }
    } else {
      return await vscode.workspace
        .getConfiguration()
        .update(param, newValue, target);
    }
  }



  export async function updateConfParameter(
    confParamName:string,
    confParamDescription:string,
    currentValue:any,
    label:string
  ) {
    const newValue = await vscode.window.showInputBox({
      placeHolder: confParamDescription,
      value: currentValue,
    });
    if (typeof newValue === "undefined") {
      return;
    }
    if (newValue.indexOf("~") !== -1) {
      const msg =
        "Character ~ is not valid for extension configuration settings.";
      console.log(msg);
      throw new Error(msg);
    }

    let valueToWrite;
    valueToWrite = newValue;

    const target = readParameter("qualIoe.saveScope");
    if (
      !check.isWorkspaceFolderOpen() &&
      target !== vscode.ConfigurationTarget.Global
    ) {
      const noWsOpenMSg = `Open a workspace or folder first.`;
      console.log(noWsOpenMSg);
      throw new Error(noWsOpenMSg);
    }
    await writeParameter(confParamName, valueToWrite, target as  vscode.ConfigurationTarget);
    const updateMessage = " has been updated";
    vscode.window.showInformationMessage(label + updateMessage);
  }



