/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

export namespace GNAME{
    export const extensionID = "qcom.qcc-ide-extension";
    export const quartzElfName = "Quartz.elf";
    export const quartzFlashUsbName = "QDLoader";
    export const noneSdkSupported = "None supported sdk for this chip";
    export const sdkScopeEnvFileName = 'sdkScopeEnv.json';
    export const sdkDnldEnvFileName = 'sdkDnldEnv.json';
    export const ramdumpQPSTLogPath = 'C:\\ProgramData\\Qualcomm\\QPST\\Sahara';
    export const openocdGdbSer = 'openocd';
    export const jlinkGdbSer = 'jlink';
}