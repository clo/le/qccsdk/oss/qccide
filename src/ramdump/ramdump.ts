/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

export namespace RAMDUMP{
    export const quartzM4 = "SRAM.BIN";
    export const quartzM0 = "M0_CLM.BIN";
    export const quartzXIP = "XIP.BIN";
    export const quartzInfo = "dump_info.txt";
    export const quartzLoad = "load.cmm";
}
