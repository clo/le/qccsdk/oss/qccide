/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

/*---------------------------------------------------------
 * Copyright (C) Microsoft Corporation. All rights reserved.
 *--------------------------------------------------------*/

import { EventEmitter } from 'events';
import { ChildProcess, spawn } from "child_process";
import * as vscode from 'vscode';
import { DebugProtocol } from 'vscode-debugprotocol';
import {Breakpoint} from 'vscode-debugadapter';
import { basename,extname,join } from "path";
import * as qualConfig from '../config/globalConfigure';
import * as Net from 'net';
import PromiseSocket from "promise-socket";
import { kill } from "process";
import {extensionPYdebugBackendPath} from '../extension';
import {delay,getSdkEnv,appendPathToEnv} from '../utils/utils';
import {CPUARCHINFO,GDBMI,DBGBACKSTATE,getGdbMiCmd,parseGdbmiRec} from './gdbmi/gdbmi';
import * as PortFinder from 'portfinder';
import { FORMERR } from 'dns';
import {GNAME} from '../config/globalName';
import * as utils from '../utils/utils';
import {activePrjCfg, sdkScopeEnv,extensionPath} from '../extension';

let restart = false;
let dbgBackendPort = 44433;
let cacheData = '';

export interface FileAccessor {
	readFile(path: string): Promise<string>;
}

export interface IBreakpoint {
	id: number;
	line: number;
	oriLine:number;
	filename:string;
	verified: boolean;
}



interface IStackFrame {
	index: number;
	name: string;
	file: string;
	line: number;
	address:string;
	column?: number;
}

interface IStack {
	count: number;
	frames: IStackFrame[];
}


interface IVarItem {
	name: string;
	arg?:string;
	type: string;
	value: string;
}

interface IMemItem {
	begin: string;
	end:string;
	contents: string;
}

interface IVariables {
	count: number;
	variables: IVarItem[];
}

interface IRegItem {
	name?: string;
	value?: string;
}


interface IRegs {
	count: number;
	regs: IRegItem[];
}

/**
 * A GDB runtime with debugger functionality.
 */
export class QcaGdbRuntime extends EventEmitter {

	// the current file we are deal
	private _sourceFile: string = '';
	public get sourceFile() {
		return this._sourceFile;
	}

	// the contents (= lines) of file
	private _sourceLines= new Map<string,string[]>();

	// This is the next line that will be 'executed'
	private _currentLine = 0;
	private _currentColumn: number | undefined;

	// maps from sourceFile to array of breakpoints
	public _breakPoints = new Map<string, IBreakpoint[]>();
	// use this because gdb backend need time to rsp of set bpkt, _breakPoints will be delayed for getNewBreakpoints()
	private _cacheBreakPoints = new Map<string, number[]>();

	private _entryFun = '';

	public _dbgBackendState = DBGBACKSTATE.EXITED;
	public _couldDetectStop = true;
	public _wantUIStopped = false;

	// watch point 
	//  <point name, point ID>
	private _breakAddresses = new Map<string, number>();

	private _noDebug = false;

	private _namedException: string | undefined;
	private _otherExceptions = false;

	private _clientSocket = new Net.Socket();
	private _promiseSocket = new PromiseSocket(this._clientSocket);

	private _backtraceDone = false;
	private _frames = new Array<IStackFrame>();

	private _evaluateValueRsp = '';
	private _setExpressionValueRsp = '';
	private _createVarObjValueRsp = '';
	private _listVarObjChildRsp = [];
	private _exprEventQueue :string[]  = [];
	private _createVarObjEventQueue :string[]  = [];
	private _createVarObjQueue :string[]  = [];
	private _listVarObjChildEventQueue :string[]  = [];
	private _variables = new Array<IVarItem>();
	private _regs = new Array<IRegItem>();
	private _memRead:IMemItem={
		begin:'',
		end:'',
		contents:''
	};
	private _disassembleRead = '';
	private _disassembleList  = [];

	// Process to execute gdb backend
	private _debugBackendTerminal: vscode.Terminal | undefined =undefined;


	constructor(private _fileAccessor: FileAccessor) {
		super();
		// this.setMaxListeners(1);
	}

	public dbgbackendIsExit(){
		if(this._dbgBackendState === DBGBACKSTATE.EXITED)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public dbgbackendIsRunning(){
		if(this._dbgBackendState === DBGBACKSTATE.RUNNING)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public dbgbackendIsStopped(){
		if(this._dbgBackendState === DBGBACKSTATE.STOPPED)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Start executing the given elf.
	 */
	public async start(): Promise<void> {

		// start gdb python backend
		let gdbCMD:string ='';

		
		let chipTarget = qualConfig.readParameter('qualIoe.staticCfg.chipTarget');
		// let demoBuildPath = qualConfig.readParameter('qualIoe.staticCfg.demoBuildPath');
		let buildRunPath = qualConfig.readParameter('qualIoe.staticCfg.buildRunPath');
		let chipTargetVer  = qualConfig.readParameter('qualIoe.staticCfg.chipVersion');
		let elfPath  = qualConfig.readParameter('qualIoe.staticCfg.elfPath') as string;
		let openocdGdbServerFilePath  = qualConfig.readParameter('qualIoe.staticCfg.openocdGdbServerFilePath');
		let gdbInitFilePath  = qualConfig.readParameter('qualIoe.staticCfg.gdbInitFilePath');

		let gdbServer = qualConfig.readParameter('qualIoe.commonCfg.defaultGdbServer');

		// parse var if has ${} placeholder
		if(elfPath.indexOf('$') !== -1)
		{
			let activePrjCfgCopy = utils.parseJsonWithVar(activePrjCfg);
			elfPath = activePrjCfgCopy.elfPath;
		}

		if(vscode.workspace.workspaceFolders)
		{
			//TODO: cmd may use and read from setting file
			// switch (chipTarget){
			// 	case "QCA402X":
			// 		gdbCMD ="arm-none-eabi-gdb" + " -x "+join(demoBuildPath as string, chipTargetVer as string,"qca_ide_asic.gdbinit") + " --interpreter=mi3";
			// 		break;
			// 	default:
			// 		return;
			// }
			gdbCMD ="arm-none-eabi-gdb" + " -x "+join(vscode.workspace.workspaceFolders[0].uri.fsPath,gdbInitFilePath as string) + " -s " + join(vscode.workspace.workspaceFolders[0].uri.fsPath,elfPath as string) + " --interpreter=mi3";

			console.log('runTime: create dbgBackend Cosole.');

			await this.createdbgBackendCosole(chipTarget as string,join(vscode.workspace.workspaceFolders[0].uri.fsPath,buildRunPath as string),gdbCMD, gdbServer as string,join(vscode.workspace.workspaceFolders[0].uri.fsPath,openocdGdbServerFilePath as string));

			// delay and wait backend started
			await delay(5000);
			await this.connectDbgBackend(dbgBackendPort);  
			this._dbgBackendState = DBGBACKSTATE.STARTED;
		}
	}

	public ifStopOnEntry(stopOnEntry:boolean){
		if(stopOnEntry){
			// we set breakpoint to main entry and run to it
			this._entryFun = qualConfig.readParameter('qualIoe.staticCfg.entryFun') as string;
			this.setBreakPointToGdb(this._entryFun );
			console.log('runTime: set entry bpkt: %s when start.', this._entryFun );
		}
	}

	/**
	 * Continue execution.
	 */
	public continue() {
		let cmdStr = getGdbMiCmd(GDBMI.execContinue);
		this.backendWrite(cmdStr);
	}

	/**
	 * Step to the next non empty line.
	 */
	public step() {
		this.stepOverToGdb();
	}

	public stepInstr() {
		this.stepInstrToGdb();
	}

	public stepIn(targetId: number | undefined) {
		this.stepInToGdb();
	}

	public stepInInstr() {
		this.stepInInstrToGdb();
	}


	public stepOut() {
		this.stepOutToGdb();
	}

	/**
	 * Returns a 'backtrace' .
	 */
	public stack(startFrame: number, endFrame: number) {
		this._backtraceDone = false;
		this.getBackTraceToGdb();
	}

	public setExpression(param:string){
		this.setExpressionToGdb(param);
	}

	public getExpressionValue(){
		return this._setExpressionValueRsp;
	}

	public getFrames(): IStack{
		return {
			frames: this._frames,
			count: this._frames.length
		};
	}

	public getValue(){
		return this._evaluateValueRsp;
	}

	public getCreateVarObjValue(){
		return this._createVarObjValueRsp;
	}

	public getListVarObjChildValue(){
		return this._listVarObjChildRsp;
	}

	public clearValue(){
		this._evaluateValueRsp = '';
	}

	/**
	 * @description remove the bpkt from _cacheBreakPoints
	 * @param file the basename of the current file
	 * @param line the line of the bpkt
	 */
	private rmCacheBpkts(file: string, line: number){
		let cacheBps = this._cacheBreakPoints.get(file);
		if(cacheBps){
			let index = cacheBps.indexOf(line);
			if(index !== -1){
				cacheBps.splice(index,1);
			}
		}
	}

/**
 * @description Set breakpoint in file with given line.
 * @param file  the basename of file
 * @param line the line that has been verified
 * @returns 
 */
	public async setBreakPoint(file: string, line: number): Promise<IBreakpoint> {
		return new Promise((resolve,reject)=>{
			let bp:IBreakpoint={
				filename: file,
				id: 0,
				oriLine:line,
				line: line,
				verified: false
			};

			// send to gdb backend
			if(this._dbgBackendState !== DBGBACKSTATE.EXITED)
			{
				this.setBreakPointToGdb(file+':'+ bp.line);
			}

			let eventName = file + ':' + bp.line;
			let timeout = setTimeout(()=> {
				// evt has not been fired
				bp.verified = false;
				this.removeAllListeners(`${eventName}`);
				this.rmCacheBpkts(file,line);
				return resolve(bp);
			}, 30000);

			// triggled by onBkpt()
			this.once(`${eventName}`, (bp)=>{
				clearTimeout(timeout);
				this.rmCacheBpkts(bp.filename,bp.line);
				return resolve(bp);
			});
		});	
	}

	/**
	 * @deprecated use createVarObj() and listVarObjChild() instead
	 * @description be carefull with this function. since its rsp may error like 'no symbol', processed by onError()
	 * @param expr 
	 */
	public evaluateExpr(expr:string){
		this._evaluateValueRsp = '';
		this._exprEventQueue.push(expr);
		this.evaluateExprToGdb(expr);
	}

	public createVarObj(expr:string){
		if(this._createVarObjQueue.indexOf(expr) === -1){
			this._createVarObjQueue.push(expr);
			this._createVarObjValueRsp = '';
			this.createVarObjToGdb(expr);
			return true;
		}
		else{
			return false;
		}
	}

	public createVarObjForPointer(expr:string,type:string){
		let length = (type.match(/\*/g) || []).length;
		let exprName = expr;
		if(length === 1)
		{
			expr = '*'+expr;
		}
		else if(length === 2){
			expr = '**'+expr;
		}
		else{
			return;
		}
		
		
		if(this._createVarObjQueue.indexOf(exprName) === -1){
			this._createVarObjQueue.push(exprName);
			this._createVarObjValueRsp = '';
			this.createVarObjToGdb(expr, exprName);
			return true;
		}
		else{
			return false;
		}
	}


	public pushListVarObjChildEvent(expr:string){
		this._listVarObjChildRsp = [];
		this._listVarObjChildEventQueue.push(expr);
	}


	public deleteVarObj(expr:string){
		let index = this._createVarObjQueue.indexOf(expr);
		if(index !== -1){
			this._createVarObjQueue.splice(index,1);
			this.deleteVarObjToGdb(expr);
			return true;
		}
		else{
			return false;
		}
	}


	public updateVarObjChild(expr:string){
		this.updateVarObjValueToGdb(expr);
	}

	public listVarObjChild(expr:string){
		this.listVarObjValueToGdb(expr);
	}

	public listCurArgs(){
		this.listCurArgsToGdb();
	}

	public listVariables(){
		this.listVarToGdb();
	}

	public listGlobalVariables(){
		this.listGlobalVarToGdb();
	}

	public listRegName(){
		this.listRegNameToGdb();
	}

	public listRegValue(){
		this.listRegValueToGdb();
	}

	public changeFrame(frameId:number){
		this.changeFrameToGdb(frameId);
	}


	public getVar():IVariables{
		return{
			count:this._variables.length,
			variables:this._variables
		};
	}

	public getReg():IRegs{
		return{
			count:this._regs.length,
			regs:this._regs
		};
	}

	/**
	 * @description Clear all breakpoints for file.
	 * @param file file name 
	 */
	public clearBreakpoints(file: string): void {
		let bps = this._breakPoints.get(file);
		if(bps !==undefined){
			for (let i =0; i< bps?.length;i++)
			{
				this.delBreakPointToGdb(bps[i].id.toString());
			}
		}
		this._breakPoints.delete(file);
	}

	public deleteBreakpoints(file: string,srcBpkts:DebugProtocol.SourceBreakpoint[]|undefined){
		let deleteLines = this.getDeleteBreakpoints(file,srcBpkts);
		let deleteIds = this.getBpktsIdFromLine(deleteLines,file);
		if(deleteIds &&deleteIds.length >= 1){
			for (let i =0; i< deleteIds?.length;i++)
			{
				this.delBreakPointToGdb(deleteIds[i].toString());
			}
		}
	}

	/**
	 * 
	 * @param lines lines that need to be deleted
	 * @param file the current file
	 * @returns the id array according lines
	 */
	private getBpktsIdFromLine(lines:number[],file:string){
		let bps = this._breakPoints.get(file);
		let bpktId:number[] = [];
		if(bps && lines){
			for(let i=0; i < bps?.length;i++)
			{
				for(let j =0 ; j< lines.length;j++){
					if (bps[i].oriLine  === lines[j]){
						bpktId.push(bps[i].id);
						// update bps in _breakPoints
						bps.splice(i,1);
					}
				}
			}
		}
		return bpktId;
	}

	/**
	 * 
	 * @param file The current file basename
	 * @param srcBpkts the srcBpkts from UI front
	 * @returns the bpkts line need be deleted
	 */
	 private getDeleteBreakpoints(file: string, srcBpkts:DebugProtocol.SourceBreakpoint[]|undefined){
		let bps = this._breakPoints.get(file);
		let arrayDst:number[] = [];
		let arraySrc:number[] = [];
		if(bps){
			for(let i=0;i<bps.length;i++){
				 arrayDst[i] = bps[i].oriLine;
			}
		}
		if(srcBpkts){
			for(let i=0;i<srcBpkts.length;i++){
				arraySrc[i] = srcBpkts[i].line;
		   }
		}
		 //    see https://stackoverflow.com/questions/1187518/how-to-get-the-difference-between-two-arrays-in-javascript
		 return  arrayDst.filter(x => !arraySrc.includes(x));
	}

/**
 * @description this need be called after deleteBreakpoints(), so just use  _breakPoints.get(file) to fill 
 * @param file the basename of the current file
 */
	public getRetainBreakpoints(file: string):Breakpoint[]{
		let retainBpkts:Breakpoint[]= [];
		let bps = this._breakPoints.get(file);
		
		if(bps){
			for(let i=0;i<bps?.length;i++){
				let temp = new Breakpoint(bps[i].verified,bps[i].line);
				temp.setId (bps[i].id);
				retainBpkts.push(temp);
			}
		}		
		return retainBpkts;
	}

	/**
	 * 
	 * @param file The current file basename
	 * @param srcBpkts the srcBpkts from UI front
	 * @returns the bpkts line need be setted newly
	 */
	public getNewBreakpoints(file: string, srcBpkts:DebugProtocol.SourceBreakpoint[]|undefined){
		let bps = this._breakPoints.get(file);
		let arrayDst:number[] = [];
		let arraySrc:number[] = [];
		if(bps){
			for(let i=0;i<bps.length;i++){
				arraySrc[i] = bps[i].oriLine;
			}
		}
		if(srcBpkts){
			for(let i=0;i<srcBpkts.length;i++){
				arrayDst[i] = srcBpkts[i].line;
		   }
		}
		//    see https://stackoverflow.com/questions/1187518/how-to-get-the-difference-between-two-arrays-in-javascript
		let newBpkts = arrayDst.filter(x => !arraySrc.includes(x));
		let cacheBps = this._cacheBreakPoints.get(file);
		if(!cacheBps){
			cacheBps = new Array<number>();
			this._cacheBreakPoints.set(file, cacheBps);
		}
		for(let i =0 ; i<newBpkts.length;i++){
			// if cacheBps has the bkpt, so it should be duplicate bpkt. so delete it
			if(cacheBps.includes(newBpkts[i])){
				newBpkts.splice(i,1);
			}else{
				// if not has, update cacheBps, it will be removed in setBreakPoint() when set bpkt down
				cacheBps.push(newBpkts[i]);
			}
		}
		return newBpkts;
	}

	/*
	 * Set data breakpoint.
	 */
	public async setDataBreakpoint(address: string):Promise<IBreakpoint> {
		return new Promise((resolve,reject)=>{
			let bp:IBreakpoint={
				filename: '',
				id: 0,
				oriLine:0,
				line: 0,
				verified: false
			};

			if (address) {
				// this._breakAddresses.add(address);
				this.setWatchPointToGdb(address);
			}
			else {
				return resolve(bp);
			}

			let eventName = address;
			let timeout = setTimeout(()=> {
				// evt has not been fired
				bp.verified = false;
				this.removeAllListeners(`${eventName}`);
				return resolve(bp);
			}, 10000);

			// triggled by onWpt()
			this.once(`${eventName}`, ()=>{
				clearTimeout(timeout);

				let id = this._breakAddresses.get(address);
				
				bp.id = id as number;
				bp.verified = true;
				return resolve(bp);
			});
		});
	}

	public setExceptionsFilters(namedException: string | undefined, otherExceptions: boolean): void {
		this._namedException = namedException;
		this._otherExceptions = otherExceptions;
	}

	/*
	 * Clear all data breakpoints.
	 */
	public clearAllDataBreakpoints(): void {
		
		for (let value of this._breakAddresses.values())
		{
			this.delBreakPointToGdb(value.toString());
		}
		this._breakAddresses.clear();
	}

	/**
	 * 
	 * @param file is the absolute file path
	 */
	public async loadSource(file: string): Promise<void> {
		if (this._sourceFile !== file) {
			this._sourceFile = file;
			// _fileAccessor define is in qualcomm-iot-embedded-device-ide\src\debugAdapter\activateQcaDebugAdapter.ts
			const contents = await this._fileAccessor.readFile(file);
			let path = basename(file);
			this._sourceLines.set(path,contents.split(/\r?\n/));
		}
	}

	public exitDbgbackend(){
		let cmd = getGdbMiCmd(GDBMI.exit);
		this.backendWrite(cmd);
	} 

	public interruptTarget(){
		let cmdStr = getGdbMiCmd(GDBMI.stop);
		this.backendWrite(cmdStr);
	}

	public readMem(params:string){
		let cmdStr = getGdbMiCmd(GDBMI.readMem,params);
		this.backendWrite(cmdStr);
	}

	public readDisassemble(params:string){
		let cmdStr = getGdbMiCmd(GDBMI.readDisassemble,params);
		this.backendWrite(cmdStr);
	}

	/**
	 * 
	 * @returns the memory string read from target
	 */
	public getMem(){
		return this._memRead;
	}

	/**
	 * 
	 * @returns the memory string read from target
	 */
	public getDisassemble(){
		return this._disassembleRead;
	}

		/**
	 * 
	 * @returns the disassemble List  read from target
	 */
		public getDisassembleList(){
			return this._disassembleList;
		}

	/**
	 * @description this fun dispose the this._debugBackendTerminal, and the backend exit itself in socket_server.py
	 */
	public async killDbgbackendConsole(){
		
		this._debugBackendTerminal?.dispose();
		this._debugBackendTerminal = undefined;

		console.log('dbgBackend console disposed.');
	}

	/**
	 * 
	 * @param file is basename of the file
	 * @param srcBpkts the srcBpkts from UI front
	 * @returns 
	 */
	public verifyAllBpkts(file:string, srcBpkts:DebugProtocol.SourceBreakpoint[]|undefined){
		let sourceLines = this._sourceLines.get(file);
		if(!sourceLines || !srcBpkts)
		{
			return;
		}

		for(let i=0;i<srcBpkts?.length;i++){
			if (srcBpkts[i].line <= sourceLines.length) {
				let verified = false;
				do{
					//  _sourceLines is zero-based
					const srcLine = sourceLines[srcBpkts[i].line-1].trim();
					
					// if a line is empty or starts with '#' we don't allow to set a breakpoint but move the breakpoint down
					if (!this.isLineCode(srcLine)) {
						srcBpkts[i].line++;
					}
					else{
						verified =true;
					}
				}while(!verified && (srcBpkts[i].line <= sourceLines.length));
			}
		}
	}

	// private methods	

	private sendEvent(event: string, ... args: any[]) {
		this.emit(event, ...args);
	}

	private isLineCode(srcLine:string){
		if(srcLine.length === 0 || srcLine.indexOf('/') === 0 || srcLine.indexOf('#') === 0)
		{	
			return false;
		}
		return true;
	} 

	/**
	 * @description
	 * recJson like this:  {'number': '1', 'type': 'breakpoint', 'disp': 'keep', 'enabled': 'y', 'addr': '0x10047590', 'func': 'app_start', 'file': '..\\..\\src\\qcli\\pal.c', 'fullname': 'C:\\Users\\ioesw_proc\\quartz\\demo\\QCLI_demo\\src\\qcli\\pal.c', 'line': '496', 'thread-groups': ['i1'], 'times': '0', 'original-location': 'app_start'}
	 * 
	 * {"bkpt": {"number": "2", "type": "breakpoint", "disp": "keep", "enabled": "y", "addr": "0x0001ae68", "func": "omtm_do_mode_switch", "file": "/local/mnt/workspace/CRMBuilds/CNSS_W.QZ.1.0.r4-00013-QZFPGA-1_20170509_214818/b/ioesw_proc/core/v2/rom/drivers/power/omtm/src/apps_proc//omtm.c", "fullname": "/local/mnt/workspace/CRMBuilds/CNSS_W.QZ.1.0.r4-00013-QZFPGA-1_20170509_214818/b/ioesw_proc/core/v2/rom/drivers/power/omtm/src/apps_proc//omtm.c", "line": "468", "thread-groups": ["i1"], "times": "0", "original-location": "omtm.c:468"}}
	 * 
	 * @param recJson 
	 */
	private onBkpt(recJson:any){
		let line = parseInt(recJson['line'],10);
		let id = parseInt(recJson['number'],10);
		// 
		if(recJson['original-location'] === this._entryFun){
			this.updateEntryBkptToFront(recJson['line'],recJson['fullname'],parseInt(recJson['number']) );
		}
		else{
			let file = basename(recJson['fullname']);

			let bps = this._breakPoints.get(file);
			if (!bps) {
				bps = new Array<IBreakpoint>();
				this._breakPoints.set(file, bps);
			}
			let bp:IBreakpoint={
				filename: file,
				id: parseInt(recJson['number']),
				oriLine:parseInt(recJson['original-location'].split(':')[1]),
				line:parseInt(recJson['line']),
				verified: true
			};

			bps.push(bp);
			let eventName = recJson['original-location'];
			this.sendEvent(`${eventName}`,bp);
		}
		
	}

	/**
	 * @description
	 * 
	 * @param recJson 
	 */
	private onWpt(recJson:any){
		let eventName = recJson['exp'];
		this._breakAddresses.set(eventName,recJson['number']);
		this.sendEvent(`${eventName}`);
	}

	private onBkptTable(recJson:any){
		//TODO:list all bpkt
	}

	/**
	 * @description
	 * value like: {"value": "{n  year = 0x7e4, n  month = 0x1, n  day = 0x1, n  hour = 0x0, n  minute = 0x14, n  second = 0x5, n  day_Of_Week = 0x2n}"}
	 * @deprecated use onCreateVarObjValue() and onListVarObjChild() instead
	 * @param value 
	 */
	private onValue(value:string){
		this._evaluateValueRsp = value;
		let event = this._exprEventQueue.shift();
		console.log('runTime: evaluateValueRsp is: %s for expr: %s',this._evaluateValueRsp, event);
		this.sendEvent(`${event}`);
	}

	/**
	 * @description
	 * value like: {"value": "{n  year = 0x7e4, n  month = 0x1, n  day = 0x1, n  hour = 0x0, n  minute = 0x14, n  second = 0x5, n  day_Of_Week = 0x2n}"}
	 * @param value 
	 */
	private onExpressionValue(value:string){
		this._setExpressionValueRsp = value;
		console.log('runTime: onExpressionValue is: %s',this._setExpressionValueRsp);
		this.sendEvent(`setExpressionReady`);
	}

/**
 * @description 
 * {"name": "tm", "numchild": "7", "value": "{...}", "type": "qapi_Time_t", "has_more": "0"}
 * @param recJson 
 */
	private onCreateVarObjValue(recJson:any){

		this._createVarObjValueRsp = JSON.stringify(recJson);
		let event = recJson['name'];
		console.log('runTime: CreateVarObjValue is: %s for expr: %s',this._createVarObjValueRsp, event);
		this.sendEvent(`${event}`);

	}

/**
 * @description
 * {"numchild": "3", "children": [{"name": "parameters.String_Value", "exp": "String_Value", "numchild": "1", "value": "0x1008137e <QCLI_Context+30> \"get\"", "type": "char *"}, {"name": "parameters.Integer_Value", "exp": "Integer_Value", "numchild": "0", "value": "0x0", "type": "int32_t"}, {"name": "parameters.Integer_Is_Valid", "exp": "Integer_Is_Valid", "numchild": "0", "value": "0x0", "type": "qbool_t"}], "has_more": "0"}
 * @param recJson 
 */
	private onListVarObjChild(childList:any){

		this._listVarObjChildRsp = childList;
		let curName = childList[0]['name'] as string;
		let curNameArray = curName.split('.');
		curName = curNameArray[0];
		for(let i =1;i<curNameArray.length - 1 ;i++){
			curName = curName + '.' + curNameArray[i];
		}
		let event = 'listVarObjChild==' + curName;

		// triggle event in processStructedVar()
		this.sendEvent(`${event}`);

	}

/**
 * @description
 * only process level 0 now, see case GDBMI.listCurrentArgs in gdbmi.ts.
 * like {"stack-args": [{"level": "0", "args": [{"name": "parameters_count", "value": "0x1"}, {"name": "parameters", "value": "0x10080c58 <QCLI_Context+312>"}]}]}
 * @param stackArgList 
 */
	private onStackArgs(stackArgList:any){

	}

	/**
	 * @description deal with the backtrace rsp from gdb backend
	 * done=={"stack": [{"level": "0", "addr": "0x010c39b0", "func": "platform_demo_time", "file": "..\\..\\src\\platform\\platform_demo.c", "fullname": "C:\\ioesw_proc\\quartz\\demo\\QCLI_demo\\src\\platform\\platform_demo.c", "line": "334"}, {"level": "1", "addr": "0x10046e10", "func": "Execute_Command", "file": "..\\..\\src\\qcli\\qcli.c", "fullname": "C:\\ioesw_proc\\quartz\\demo\\QCLI_demo\\src\\qcli\\qcli.c", "line": "1085"}, {"level": "2", "addr": "0x10046e10", "func": "Process_Command", "file": "..\\..\\src\\qcli\\qcli.c", "fullname": "C:\\ioesw_proc\\quartz\\demo\\QCLI_demo\\src\\qcli\\qcli.c", "line": "1479"}, {"level": "3", "addr": "0x10046e10", "func": "QCLI_Process_Input_Data", "file": "..\\..\\src\\qcli\\qcli.c", "fullname": "C:\\ioesw_proc\\quartz\\demo\\QCLI_demo\\src\\qcli\\qcli.c", "line": "1629"}, {"level": "4", "addr": "0x10047292", "func": "QCLI_Thread", "file": "..\\..\\src\\qcli\\pal.c", "fullname": "C:\\ioesw_proc\\quartz\\demo\\QCLI_demo\\src\\qcli\\pal.c", "line": "414"}, {"level": "5", "addr": "0x1002ef28", "func": "??"}]}
	 * 
	 * @param stackList the input should be the stack list, beacuse we putin recJson['stack'] 
	 */
	private onStack(stackList:any){
		this._frames = [];
		//push all stack
		if(!this._backtraceDone){
			for (let i = 0; i < stackList.length; i++) {
				if('fullname' in stackList[i])
				{
					const name = stackList[i]['func'] as string;	// use a word of the func as the stackframe name
					const stackFrame: IStackFrame = {
						index: i,
						name: `${name}(${i})`,
						file: stackList[i]['fullname'] as string,
						line: parseInt(stackList[i]['line']),
						address:stackList[i]['addr']
					};
					this._frames.push(stackFrame);
				}
			}

			this._backtraceDone = true;
			console.log('runTime: backtrace is done.');
			this.sendEvent('backtraceReady');
		}
	}

	/**
	 * @description process the variable list from gdb
	 * like: done=={"variables": [{"name": "parameters_count", "arg": "1", "type": "uint32_t", "value": "<optimized out>"}, {"name": "parameters", "arg": "1", "type": "QCLI_Parameter_t *", "value": "<optimized out>"}, {"name": "tm", "type": "qapi_Time_t"}, {"name": "status", "type": "int", "value": "0x0"}]}
	 *
	 *  @param varList 
	 */
	private onVariables(varList:any){
		this._variables = [];
	
		for(let i = 0; i < varList.length;i++){
			const varItem:IVarItem ={
					name:varList[i]['name'],
					type:varList[i]['type'],
					value:varList[i]['value']?varList[i]['value']:''
			};
			this._variables.push(varItem);
		}
		this.sendEvent('variablesReady');
	}

	/**
	 * @description Process reg name here
	 * done=={"register-names": ["r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "sp", "lr", "pc"]}

	 * @param regNameList 
	 */
	private onRegName(regNameList:any){
		this._regs = [];

		//TODO: may need change to different cpu arch
		for(let i = 0; i < Math.min(regNameList.length, CPUARCHINFO.armRegCount);i++){
			const regItem:IRegItem ={
					name:regNameList[i]
			};
			this._regs.push(regItem);
		}
	}

	/**
	 * @description Process reg value here, and need process reg name before this. 
	 * done=={"register-values": [{"number": "0", "value": "0x1"}, {"number": "1", "value": "0x10080c4c"}, {"number": "2", "value": "0x15"}, {"number": "3", "value": "0x10c3985"}, {"number": "4", "value": "0x1"}, {"number": "5", "value": "0x10080c4c"}, {"number": "6", "value": "0x10080c4c"}, {"number": "7", "value": "0x10a6e58"}, {"number": "8", "value": "0x0"}, {"number": "9", "value": "0x2"}, {"number": "10", "value": "0x4"}, {"number": "11", "value": "0x1"}, {"number": "12", "value": "0x0"}, {"number": "13", "value": "0x1009cf90"}, {"number": "14", "value": "0x10046e11"}, {"number": "15", "value": "0x10c39a4"}]}

	 * @param regValueList 
	 */
	private onRegValue(regValueList:any){

		for(let i = 0; i < Math.min(regValueList.length, this._regs.length);i++){
			this._regs[i].value = regValueList[i]['value']?regValueList[i]['value']:'';
		}

		this.sendEvent('regsReady');

	}

	/**
	 * @description process memory here
	 *  like: memory:[{begin="0xbffff154",offset="0x00000000",end="0xbffff15e",contents="01000000020000000300"}]
	 * @param mem 
	 */
	private onMem(mem:any){
		this._memRead.contents =  mem[0]['contents'];
		this._memRead.begin =  mem[0]['begin'];
		this._memRead.end =  mem[0]['end'];

		this.sendEvent('memoryVscode');
		this.sendEvent('memory');
	}


	/**
	 * @description process disassemble here
	 *  like: asm_insns:[src_and_asm_line={line="163",file="..\\..\\src\\platform\\platform_demo.c",fullname="C:ioesw_proc\\quartz\\demo\\QCLI_demo\\src\\platform\\platform_demo.c",line_asm_insn=[{address="0x0117b254",func-name="platform_demo_get_time",offset="0",inst="push\t{r4, r5, lr}"},{address="0x0117b256",func-name="platform_demo_get_time",offset="2",inst="sub\tsp, #20"}]},...]
	 * @param disassemble 
	 */
	private onDisassemble(disassemble:any){
		// for customer disassemble view 
		this._disassembleRead = disassemble;
		this.sendEvent('disassemble');

		// for vscode disassemble view 
		this._disassembleList = disassemble;
		this.sendEvent('disassembleList');
	}

	/**
	 * @description
	 * like: {"msg": "No symbol \"tm\" in current context."}
	 * 
	 * @param error the error msg
	 */
	private onError(error:string){
		if(error.indexOf('No symbol table is loaded') === -1){
			vscode.window.showErrorMessage(error);
		}
		
	}

	/**
	 * @description
	 * recJson like: {'reason': 'signal-received', 'signal-name': 'SIGTRAP', 'signal-meaning': 'Trace/breakpoint trap', 'frame': {'addr': '0x1002c944', 'func': 'xTaskResumeAll', 'args': [], 'file': 'C:\\Users\\ioesw_proc\\thirdparty\\aws_freertos\\lib\\FreeRTOS\\tasks.c', 'fullname': 'C:\\Users\\ioesw_proc\\thirdparty\\aws_freertos\\lib\\FreeRTOS\\tasks.c', 'line': '2175'}, 'thread-id': '1', 'stopped-threads': 'all'}
	 * @param recJson 
	 */
	private onTargetStopped(recJson:any){
		let frameJson = recJson['frame'];
		if(!this._couldDetectStop){
			return;
		}
		this._couldDetectStop = false;
		switch(recJson['reason']){
			case 'signal-received':
				if(this._wantUIStopped){
					this._wantUIStopped = false;
					this.sendEvent('stopOnPause');
				}
				break;

			case 'breakpoint-hit':
				// send 'stopped' event
				// after rececive the event, front will send stackTraceRequest()
				this.sendEvent('stopOnBreakpoint');
				break;

			case 'watchpoint-trigger':
				this.sendEvent('stopOnDataBreakpoint');
				break;
				
			case 'watchpoint-scope':
				vscode.window.showInformationMessage('watchpoint out scope and be deleted.');
				this.sendEvent('stopOnDataBreakpoint');
				break;

			case 'end-stepping-range':
				this.sendEvent('stopOnStep');
				break;

			case 'function-finished':
				this.sendEvent('stopOnStep');
				break;

			default:
				// 1st stopped is here when attach gdb
				this.sendEvent('stopOnPause');
				break;
		}
	}

	private onTargetExited(){
		
	}

	private onGdbmiRsp(rec:string){
		let recArray = parseGdbmiRec(rec);
		let recJson = undefined;
		if (recArray[1] !== 'None' && recArray[1] !== 'null')
		{
			try{
				recJson = JSON.parse(recArray[1]);
			}
			catch (e){
				return console.error(e);
			}
		}
		else{
			return;
		}
		switch (recArray[0]){
			case 'done':

				// rsp to -break-insert
				if('bkpt' in recJson)
				{
					this.onBkpt(recJson['bkpt']);
				}

				// rsp to -break-watch
				if('wpt' in recJson)
				{
					this.onWpt(recJson['wpt']);
				}

				// rsp to -break-list
				if('BreakpointTable' in recJson)
				{
					this.onBkptTable(recJson['BreakpointTable']);
				}

				// rsp to -stack-list-frames
				if('stack' in recJson)
				{
					this.onStack(recJson['stack']);
				}

				// rsp to -data-evaluate-expression
				if('value' in recJson && (recJson['numchild'] === null || recJson['numchild'] === undefined))
				{
					this.onExpressionValue(recJson['value']);
				}

				// rsp to -var-create
				if('numchild' in recJson && (recJson['children'] === null || recJson['children'] === undefined))
				{
					this.onCreateVarObjValue(recJson);
				}

				// rsp to -var-list-children --simple-values
				if('children' in recJson)
				{
					this.onListVarObjChild(recJson['children']);
				}

				// rsp to -stack-list-arguments 1 0 0
				if('stack-args' in recJson)
				{
					this.onStackArgs(recJson['stack-args']);
				}

				// rsp to -stack-list-variables  --simple-values
				if('variables' in recJson)
				{
					this.onVariables(recJson['variables']);
				}

				// rsp to -data-list-register-names
				if('register-names' in recJson)
				{
					this.onRegName(recJson['register-names']);
				}

				// rsp to -data-list-register-values x
				if('register-values' in recJson)
				{
					this.onRegValue(recJson['register-values']);
				}

				// rsp to -data-read-memory-bytes
				if('memory' in recJson)
				{
					this.onMem(recJson['memory']);
				}

				// rsp to -data-disassemble
				if('asm_insns' in recJson)
				{
					this.onDisassemble(recJson['asm_insns']);
				}

				break;
	
			case 'stopped':
				this._dbgBackendState = DBGBACKSTATE.STOPPED;
				if(recJson && recJson !== undefined)
				{
					this.onTargetStopped(recJson);
				}
				break;
	
			case 'running':
				this._dbgBackendState = DBGBACKSTATE.RUNNING;
				this._couldDetectStop = true;
				break;
	
			case 'error':
				console.error('runTime: the record is error: %s', recArray[1]);
				this.onError(recJson['msg']);
				break;

			case 'exit':
				console.info('runTime: gdb backend exited');
				this.onTargetExited();
				break;
			
			case 'breakpoint-deleted':
				const bp: IBreakpoint = { verified: false, oriLine:0,line:0, filename:'',id: recJson['id']};
				vscode.window.showInformationMessage('breakpoint-deleted by gdb, id: ' + recJson['id']);
				this.sendEvent('breakpointRemoved',bp);
				break;
	
			default:
				// console.info('the record do not support:%s', recArray[0]);
				break;
		}
	}

	private async connectDbgBackend(port:Number){

		this._clientSocket.on('data', (data) => {
			// deal gdbmi rsp
			// may over several packets,so cache 
			cacheData += data.toString();
			// console.log("===Data start==== \r\n" + data.toString() + "====Data end==== \r\n");
			console.log("===cacheData start==== \r\n" + cacheData + "====cacheData end==== \r\n");

			if (cacheData.lastIndexOf('\n') === cacheData.length-1){
				// may have several record together,so split 
				let rspArray = cacheData.trim().split('\n');
				console.log("rspArray length: " + rspArray.length);
				for(let i = 0; i< rspArray.length;i++){
					this.onGdbmiRsp(rspArray[i]);
					console.log("rspArray " + i +' '+ rspArray[i]);
				}
				cacheData = '';
			}
			
		  });

		this._clientSocket.on('end', () => {
			console.log('runTime: disconnected from debug backend');
			this._dbgBackendState = DBGBACKSTATE.EXITED;
		  });

		this._clientSocket.on('error',(error)=>{
			console.log('runTime: connected from debug backend error:%s, try restart JLINK and Target',error);
			vscode.window.showErrorMessage('connect dbgbackend ' + error);
			this._dbgBackendState = DBGBACKSTATE.EXITED;
			this.killDbgbackendConsole();
			this._clientSocket.destroy();
		});

		this._clientSocket.on('close',(hadError)=>{
			console.log('runTime: _clientSocket fully closed');
			this._dbgBackendState = DBGBACKSTATE.EXITED;
			this.killDbgbackendConsole();
			this._clientSocket.destroy();
		});

		await this._promiseSocket.connect(Number(port), '127.0.0.1');

		console.log('runTime: Connect debug backend success');
	}


	private backendWrite(cmdStr:string){

		if(this._dbgBackendState === DBGBACKSTATE.EXITED){
			console.error('runTime: try to send cmd to debug backend when it is not ready:%s.',cmdStr);
			return;
		}

		if(cmdStr && cmdStr !== '' && !this._clientSocket.destroyed)
		{
			cmdStr = cmdStr+'\n';
			this._clientSocket.write(cmdStr);
		}

	}

	private getBackTraceToGdb(){
		let cmdStr = getGdbMiCmd(GDBMI.backtrace);
		this.backendWrite(cmdStr);
	}

	private setBreakPointToGdb(bpkt:string){
		let cmdStr = getGdbMiCmd(GDBMI.bpinsert,bpkt);
		this.backendWrite(cmdStr);
	}

	private setExpressionToGdb(param:string){
		let cmdStr = getGdbMiCmd(GDBMI.setExpression,param);
		this.backendWrite(cmdStr);
	}

	private setWatchPointToGdb(wpkt:string){
		let cmdStr = getGdbMiCmd(GDBMI.wptinsert,wpkt);
		this.backendWrite(cmdStr);
	}

	private delBreakPointToGdb(bpktId:string){
		let cmdStr = getGdbMiCmd(GDBMI.bpdelete,bpktId);
		this.backendWrite(cmdStr);
	}

	private stepOverToGdb(){
		let cmdStr = getGdbMiCmd(GDBMI.execNext);
		this.backendWrite(cmdStr);
	}

	private stepInstrToGdb(){
		let cmdStr = getGdbMiCmd(GDBMI.execNextInstr);
		this.backendWrite(cmdStr);
	}

	private stepInToGdb(){
		let cmdStr = getGdbMiCmd(GDBMI.execStepIn);
		this.backendWrite(cmdStr);
	}

	private stepInInstrToGdb(){
		let cmdStr = getGdbMiCmd(GDBMI.execStepInInstr);
		this.backendWrite(cmdStr);
	}

	private stepOutToGdb(){
		let cmdStr = getGdbMiCmd(GDBMI.execStepOut);
		this.backendWrite(cmdStr);
	}

	/**
	 * @deprecated use createVarObjToGdb() and listVarObjValueToGdb() instead
	 * @param expr 
	 */
	private evaluateExprToGdb(expr:string){
		let cmdStr = getGdbMiCmd(GDBMI.evaluate,expr);
		this.backendWrite(cmdStr);
	}

	private createVarObjToGdb(expr:string,exprName?:string){
		let cmdStr = getGdbMiCmd(GDBMI.createVar,expr,exprName);
		this.backendWrite(cmdStr);
	}

	private deleteVarObjToGdb(expr:string){
		let cmdStr = getGdbMiCmd(GDBMI.deleteVar,expr);
		this.backendWrite(cmdStr);
	}

	private updateVarObjValueToGdb(expr:string){
		let cmdStr = getGdbMiCmd(GDBMI.updateVarObjChild,expr);
		this.backendWrite(cmdStr);
	}

	private listVarObjValueToGdb(expr:string){
		let cmdStr = getGdbMiCmd(GDBMI.listVarObjChild,expr);
		this.backendWrite(cmdStr);
	}

	private listCurArgsToGdb(){
		let cmdStr = getGdbMiCmd(GDBMI.listCurrentArgs);
		this.backendWrite(cmdStr);
	}

	private listLocalVarToGdb(){
		let cmdStr = getGdbMiCmd(GDBMI.listLocalVar);
		this.backendWrite(cmdStr);
	}

	private listVarToGdb(){
		let cmdStr = getGdbMiCmd(GDBMI.listVariables);
		this.backendWrite(cmdStr);
	}

	private listGlobalVarToGdb(){
		let cmdStr = getGdbMiCmd(GDBMI.listGlobalVariables);
		this.backendWrite(cmdStr);
	}

	private listRegValueToGdb(){
		let cmdStr = getGdbMiCmd(GDBMI.listRegValue);
		this.backendWrite(cmdStr);
	}

	private listRegNameToGdb(){
		let cmdStr = getGdbMiCmd(GDBMI.listRegNames);
		this.backendWrite(cmdStr);
	}

	private changeFrameToGdb(frameId:number){
		let cmdStr = getGdbMiCmd(GDBMI.changeFrame,frameId);
		this.backendWrite(cmdStr);
	}


	private updateEntryBkptToFront(entryLine:string,entryFile:string, id:number){

		entryFile = basename(entryFile);
		const bp: IBreakpoint = { verified: true, oriLine: parseInt(entryLine) ,line:parseInt(entryLine),filename:entryFile, id: id};
		let bps = this._breakPoints.get(entryFile);
		if (!bps) {
			bps = new Array<IBreakpoint>();
			this._breakPoints.set(entryFile, bps);
		}
		bps?.push(bp);
		this.sendEvent('breakpointValidated', bp);
	}

	private async createdbgBackendCosole(chipTarget:string,buildDir:string,gdbCmd:string, gdbServer?:string,gdbServerInitFile?:string){

		if (this._debugBackendTerminal === undefined) {

			let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath');
	
			let nameInEnv;
			if (process.platform === "win32") {
			  nameInEnv = "Path";
			} else {
			  nameInEnv = "PATH";
			}
			let extEnv = appendPathToEnv(nameInEnv,sdkScopeEnv);

			this._debugBackendTerminal = vscode.window.createTerminal({
			  name: "debug backend console",
			  shellArgs: [],
			  shellPath: vscode.env.shell,
			  cwd:buildDir,
			  //use self env
			  env:extEnv,
			  strictEnv: false,
			});
			
			let pythonPath = qualConfig.readParameter('qualIoe.commonCfg.pythonBinPath');

			// default use openocd as gdb server
			if(!gdbServer){
				gdbServer = GNAME.openocdGdbSer;
			}
			// get a free port for backend
			await PortFinder.getPortPromise()
			.then((port) => {
				//
				// `port` is guaranteed to be a free port
				// in this scope.
				//
				// call python debug backend 
				dbgBackendPort = port;
				this._debugBackendTerminal?.sendText(`${pythonPath} ${extensionPYdebugBackendPath} -d ${chipTarget} -b ${buildDir} -P ${port} -g "${gdbCmd}" -s ${gdbServer} -f ${gdbServerInitFile}`);
				this._debugBackendTerminal?.show();

			})
			.catch((err) => {
				vscode.window.showInformationMessage('There is no free port for debug backend.');
				this._debugBackendTerminal?.dispose();
				this._debugBackendTerminal = undefined;
				return;
			});

		  }
		  else
		  {
			vscode.window.showInformationMessage('debug backend console has been opened before.');
		  }
		  
	}
}