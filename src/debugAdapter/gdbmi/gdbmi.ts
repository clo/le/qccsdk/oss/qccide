/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

/** detail see:
 *  https://www.sourceware.org/gdb/onlinedocs/gdb/GDB_002fMI.html#GDB_002fMI
 * */ 

export namespace GDBMI{
    export const bpinsert = "breakpoint insert with locatioon";
    export const wptinsert = "watchpoint insert";
    export const bpdelete = "delete breakpoint";
    export const backtrace = "List the frames currently on the stack";
    export const changeFrame = "Change the selected frame";
    export const execContinue = "run the code until breakpoint";
    export const execStepIn = "step in the fun";
    export const execStepInInstr = "step in the fun with instruction";
    export const execStepOut = "step out the fun";
    export const execNext = "step over";
    export const exit = "exist the gdb";
    export const interrupt = "interrupt the target";
    export const stop = "stop the target";
    export const evaluate = "evaluate an expression";
    export const setExpression = "set an expression";
    export const listCurrentArgs = "list the arguments for current frame";
    export const listLocalVar = "list the local variables";
    export const listVariables = "list local variables and function arguments for the selected frame";
    export const listGlobalVariables = "list global variables";
    export const setVariablesFormat = "set variables format";
    export const listRegNames = "list all registers' name";
    export const listRegValue = "list all registers' value";
    export const readMem = "read memory from address";
    export const readDisassemble = "read disassemble from address";
    export const createVar = "creates a variable object, which allows the monitoring of a variable";
    export const deleteVar = "delete a variable object";
    export const listVarObjChild = "list a variable object child";
    export const updateVarObjChild = "update a variable object child";
    export const execNextInstr = "step over one instruction";

}

export namespace DBGBACKSTATE{
    export const STARTED = "gdb backend started";
    export const RUNNING = "gdb backend is running";
    export const STOPPED = "gdb backend stopped";
    export const EXITED  = "gdb backend exited";
}

export namespace CPUARCHINFO{
    export const armRegCount = 16;

}

/**
 * @function getGdbMiCmd
 * @description get the cmd string according the type
 * @param type the type of gdbmi cmd
 * @param param the parameter of the cmd
 * @returns 
 */
export function getGdbMiCmd(type:string,param?:any,param2?:any){
    let cmdStr = '';

    switch(type){
        case GDBMI.bpinsert:
            if(!param || param === undefined)
            {
                break;
            }
            cmdStr = '-break-insert '+ param ;
            break;
        
        case GDBMI.wptinsert:
            if(!param || param === undefined)
            {
                break;
            }
            cmdStr = '-break-watch '+ param ;
            break;
        
        case GDBMI.bpdelete:
            if(!param || param === undefined)
            {
                break;
            }
            cmdStr = '-break-delete '+ param ;
            break;

        case GDBMI.backtrace:
            cmdStr = '-stack-list-frames';
            break;
            
        case GDBMI.execContinue:
            cmdStr = '-exec-continue';
            break;

        case GDBMI.exit:
            cmdStr = '-gdb-exit';
            break;
        
        // Note: stop cmd issued by front (include pause request), 
        // comment here for note that only if target is stopped, then gdbmi cmd can send to target and work
        case GDBMI.interrupt:
            cmdStr = '-exec-interrupt --all';
            break;

        case GDBMI.stop:
            cmdStr = 'SIGTRAP';
            break;

        case GDBMI.execStepIn:
            cmdStr = '-exec-step';
            break;
        
        case GDBMI.execStepInInstr:
            cmdStr = '-exec-step-instruction';
            break;

        case GDBMI.execStepOut:
            cmdStr = '-exec-finish';
            break;

        case GDBMI.execNext:
            cmdStr = '-exec-next';
            break;
        
        // https://sourceware.org/gdb/current/onlinedocs/gdb.html/GDB_002fMI-Program-Execution.html#GDB_002fMI-Program-Execution
        case GDBMI.execNextInstr:
            cmdStr = '-exec-next-instruction';
            break;
        
        // this command is deprecated, use createVar and listVarObjChild instead
        case GDBMI.evaluate:
            if(!param || param === undefined)
            {
                break;
            }
            cmdStr = '-data-evaluate-expression ' + param;
            break;
        
        case GDBMI.listCurrentArgs:
            // see https://www.sourceware.org/gdb/onlinedocs/gdb/GDB_002fMI-Stack-Manipulation.html#GDB_002fMI-Stack-Manipulation
            cmdStr = '-stack-list-arguments 1 0 0';
            break;

        case GDBMI.listLocalVar:
            cmdStr = '-stack-list-locals --simple-values';
            break;

        case GDBMI.listVariables:
            // see https://www.sourceware.org/gdb/onlinedocs/gdb/GDB_002fMI-Stack-Manipulation.html#GDB_002fMI-Stack-Manipulation
            cmdStr = '-stack-list-variables --simple-values';
            break;

        case GDBMI.listGlobalVariables:
            // see https://www.sourceware.org/gdb/onlinedocs/gdb/GDB_002fMI-Symbol-Query.html#GDB_002fMI-Symbol-Query
            // NOTE: not supported for now for mi3
            cmdStr = '-symbol-info-variables  --max-results 10';
            break;

        case GDBMI.listRegNames:
            // see https://www.sourceware.org/gdb/onlinedocs/gdb/GDB_002fMI-Data-Manipulation.html#GDB_002fMI-Data-Manipulation
            cmdStr = '-data-list-register-names';
            break;

        case GDBMI.listRegValue:
            // see https://www.sourceware.org/gdb/onlinedocs/gdb/GDB_002fMI-Data-Manipulation.html#GDB_002fMI-Data-Manipulation
            cmdStr = '-data-list-register-values x';
            break;

        case GDBMI.readMem:
            // see https://www.sourceware.org/gdb/onlinedocs/gdb/GDB_002fMI-Data-Manipulation.html#GDB_002fMI-Data-Manipulation
            // -data-read-memory-bytes address count
            if(!param || param === undefined)
            {
                break;
            }
            cmdStr = '-data-read-memory-bytes ' + param;
            break;
        
        case GDBMI.readDisassemble:
            // see https://www.sourceware.org/gdb/onlinedocs/gdb/GDB_002fMI-Data-Manipulation.html#GDB_002fMI-Data-Manipulation
            if(!param || param === undefined)
            {
                break;
            }
            cmdStr = '-data-disassemble ' + param + ' -- 4';
            break;
        
        case GDBMI.setExpression:
            if(!param || param === undefined)
            {
                break;
            }
            cmdStr = '-var-assign ' + param;
            break;
            
        case GDBMI.createVar:
            // https://www.sourceware.org/gdb/onlinedocs/gdb/GDB_002fMI-Variable-Objects.html#GDB_002fMI-Variable-Objects
            if(!param)
            {
                break;
            }
            if(!param2)
            {
                param2 = param;
            }
            // param2 is the name, param is the exp
            cmdStr = '-var-create ' + param2 +' @ ' + param;
            break;

        case GDBMI.deleteVar:
            // https://www.sourceware.org/gdb/onlinedocs/gdb/GDB_002fMI-Variable-Objects.html#GDB_002fMI-Variable-Objects
            if(!param || param === undefined)
            {
                break;
            }
            cmdStr = '-var-delete ' + param;
            break;

        case GDBMI.listVarObjChild:
            // https://www.sourceware.org/gdb/onlinedocs/gdb/GDB_002fMI-Variable-Objects.html#GDB_002fMI-Variable-Objects
            if(!param || param === undefined)
            {
                break;
            }
            cmdStr = '-var-list-children --simple-values ' + param;
            break;

        case GDBMI.updateVarObjChild:
            // https://sourceware.org/gdb/current/onlinedocs/gdb.html/GDB_002fMI-Variable-Objects.html#GDB_002fMI-Variable-Objects
            if(!param || param === undefined)
            {
                break;
            }
            cmdStr = '-var-update --simple-values ' + param;
            break;
        
        case GDBMI.changeFrame:
            // https://sourceware.org/gdb/current/onlinedocs/gdb.html/GDB_002fMI-Stack-Manipulation.html#GDB_002fMI-Stack-Manipulation
            if(!param || param === undefined)
            {
                break;
            }
            cmdStr = '-stack-select-frame ' + param;
            break;

        case GDBMI.setVariablesFormat:
            // https://sourceware.org/gdb/current/onlinedocs/gdb.html/GDB_002fMI-Stack-Manipulation.html#GDB_002fMI-Stack-Manipulation
            if(!param || param === undefined)
            {
                break;
            }
            cmdStr = '-var-set-format ' + param;
            break;

        default:
            cmdStr='';

    }
    return cmdStr;
}

/**
 * @function parseGdbmiRec
 * @description parse the rsp from gdb backend
 * ======rec fprmat========
 * rsult + ' ' + result body + '\n'
 * example:
 * done {'bkpt': {'number': '1', 'type': 'breakpoint', 'disp': 'keep', 'enabled': 'y', 'addr': '0x10047590', 'func': 'app_start', 'file': '..\\..\\src\\qcli\\pal.c', 'fullname': 'C:\\Users\\ioesw_proc\\quartz\\demo\\QCLI_demo\\src\\qcli\\pal.c', 'line': '496', 'thread-groups': ['i1'], 'times': '0', 'original-location': 'app_start'}}
 * 
 * stopped {'reason': 'signal-received', 'signal-name': 'SIGTRAP', 'signal-meaning': 'Trace/breakpoint trap', 'frame': {'addr': '0x1002c944', 'func': 'xTaskResumeAll', 'args': [], 'file': 'C:\\Users\\ioesw_proc\\thirdparty\\aws_freertos\\lib\\FreeRTOS\\tasks.c', 'fullname': 'C:\\Users\\ioesw_proc\\thirdparty\\aws_freertos\\lib\\FreeRTOS\\tasks.c', 'line': '2175'}, 'thread-id': '1', 'stopped-threads': 'all'}
 * 
 * 
 * @param rec one record string from gdb backend 
 */
export function parseGdbmiRec(rec:string){
    return  rec.trim().split('==');
}