/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import * as vscode from 'vscode';
import { WorkspaceFolder, DebugConfiguration, ProviderResult, CancellationToken,DebugSessionCustomEvent } from 'vscode';
import * as Net from 'net';
import { QCAdebugSession } from './qcaDebugAdapter';
import { FileAccessor } from './qcaGdbRuntime';
import * as qualConfig from '../config/globalConfigure';
import { join,basename } from "path";
import {canAccessFile,cpFileIfnotExist} from "../utils/utils";
import {activePrjCfg, extensionPath} from '../extension';
import * as fs from 'fs';
import {MemViewProvider} from './memView';
import {DisassembleViewProvider} from './disassembleView';
import * as utils from '../utils/utils';

let memViewProvider:MemViewProvider | undefined = undefined;
let disassembleViewProvider:DisassembleViewProvider | undefined = undefined;

export const workspaceFileAccessor: FileAccessor = {
	async readFile(path: string) {
		try {
			const uri = vscode.Uri.file(path);
			const bytes = await vscode.workspace.fs.readFile(uri);
			const contents = Buffer.from(bytes).toString('utf8');
			return contents;
		} catch(e) {
			try {
				const uri = vscode.Uri.parse(path);
				const bytes = await vscode.workspace.fs.readFile(uri);
				const contents = Buffer.from(bytes).toString('utf8');
				return contents;
			} catch (e) {
				return `cannot read '${path}'`;
			}
		}
	}
};


function onCustomEvent(event:DebugSessionCustomEvent){
	if (event.session.type === 'qcaIde') {
		switch (event.event) {
			case 'readMem':
				let memRsp = event.body.body;
				if(memViewProvider){
					memViewProvider.addMemData(memRsp);
				}
				break;
			
			case 'readDisassemble':
				let disassembleRsp = event.body.body;
				if(disassembleViewProvider){
					let fullname = '';
					let srcCode ;

					for (let i=0; i<disassembleRsp.length;i++){
						let line = disassembleRsp[i]['line'];
						if (disassembleRsp[i]['fullname'] !== fullname){
							fullname = disassembleRsp[i]['fullname'] ;
							if(fs.existsSync(fullname)){
								srcCode = fs.readFileSync(disassembleRsp[i]['fullname'], {encoding :'utf-8'}).split('\n');
							}
						}
						if(srcCode){
							disassembleRsp[i]['line'] = line + ' : ' + srcCode[parseInt(line)-1];
						}

					}
					disassembleViewProvider.addDisassembleData(JSON.stringify(disassembleRsp));
				}

				break;

			default:
				break;
		}
	}

}

export function activateQcaDebugAdapter(context: vscode.ExtensionContext, factory?: vscode.DebugAdapterDescriptorFactory) {

	// register a configuration provider for 'qcaIde' debug type
	const provider = new QcaDebugConfigurationProvider();
	context.subscriptions.push(vscode.debug.registerDebugConfigurationProvider('qcaIde', provider));
	if(!factory)
	{
		factory = new QcaDebugAdapterServerDescriptorFactory();
	}
	if(factory)
	{
		context.subscriptions.push(vscode.debug.registerDebugAdapterDescriptorFactory('qcaIde', factory));
		if ('dispose' in factory) {
			context.subscriptions.push(factory as any);
		}
	}

	// https://github.com/firefox-devtools/vscode-firefox-debug/blob/8b7c81a6360ae005a40d3e626c877e2537cd8248/src/extension/main.ts#L68
	// register custom event listener
	context.subscriptions.push(
        vscode.debug.onDidReceiveDebugSessionCustomEvent(
            (event) => onCustomEvent(event)
        )
    );

	// register memeory web view
	memViewProvider = new MemViewProvider(context.extensionUri);
	context.subscriptions.push(
		vscode.window.registerWebviewViewProvider(MemViewProvider.viewType, memViewProvider));

	// register memeory web view
	disassembleViewProvider = new DisassembleViewProvider(context.extensionUri);
	context.subscriptions.push(
		vscode.window.registerWebviewViewProvider(DisassembleViewProvider.viewType, disassembleViewProvider));

}


class QcaDebugAdapterServerDescriptorFactory implements vscode.DebugAdapterDescriptorFactory {

	private server?: Net.Server;

	createDebugAdapterDescriptor(session: vscode.DebugSession, executable: vscode.DebugAdapterExecutable | undefined): vscode.ProviderResult<vscode.DebugAdapterDescriptor> {

		if (!this.server) {
			// start listening on a random port
			this.server = Net.createServer(socket => {
				const session = new QCAdebugSession(workspaceFileAccessor);
				session.setRunAsServer(true);
				session.start(socket as NodeJS.ReadableStream, socket);
			}).listen(0);
		}

		// make VS Code connect to debug server
		return new vscode.DebugAdapterServer((this.server.address() as Net.AddressInfo).port);
	}

	dispose() {
		if (this.server) {
			this.server.close();
		}
	}
}

class QcaDebugConfigurationProvider implements vscode.DebugConfigurationProvider {
	/**
	 * Massage a debug configuration just before a debug session is being launched,
	 * e.g. add all missing attributes to the debug configuration.
	 * Do some pre-operation before begin debug
	 */
	resolveDebugConfiguration(
		folder: WorkspaceFolder | undefined, 
		config: DebugConfiguration, 
		token?: CancellationToken): ProviderResult<DebugConfiguration> {

		// if launch.json is missing or empty
		if (!config.type && !config.request && !config.name) {
			
			config.type = 'qcaIde';
			config.request = 'launch';
			config.name = 'QCC Debug';
			config.stopOnEntry = true;
		
		}

		// check elf is exist, and return the path of it
		let elfPath = qualConfig.readParameter('qualIoe.staticCfg.elfPath') as string;

		if(elfPath && elfPath !=='')
		{
			if(elfPath.indexOf('$') !== -1)
			{
				let activePrjCfgCopy = utils.parseJsonWithVar(activePrjCfg);
				elfPath = activePrjCfgCopy.elfPath;
			}

			if(vscode.workspace.workspaceFolders)
			{
				elfPath = join(vscode.workspace.workspaceFolders[0].uri.fsPath,elfPath as string);
				if(canAccessFile(elfPath as string))
				{
					config.program = elfPath;
				}
				else
				{
					vscode.window.showInformationMessage('Can not access elf from ' + elfPath);
					return;
				}
			}
		}
		else
		{
			//TODO: auto build here
			vscode.window.showInformationMessage('Please build demo first before debug or set elfPath.');
			return;
		}

		return config;
	}
}