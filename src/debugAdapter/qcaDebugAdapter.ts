/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import {
	Logger, logger,
	LoggingDebugSession,
	InitializedEvent, TerminatedEvent, StoppedEvent, BreakpointEvent, OutputEvent,
	ProgressStartEvent, ProgressUpdateEvent, ProgressEndEvent, InvalidatedEvent,
	Thread, StackFrame, Variable,Scope, Source, Handles, Breakpoint
} from 'vscode-debugadapter';
import { DebugProtocol } from 'vscode-debugprotocol';
import { basename,extname,join} from 'path';
import { QcaGdbRuntime, IBreakpoint, FileAccessor } from './qcaGdbRuntime';
import * as vscode from 'vscode';
import * as qualConfig from '../config/globalConfigure';
import {canAccessFile} from '../utils/utils';
import { relative } from 'node:path';
const awaitNotify = require('await-notify');

function timeout(ms: number) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

enum VarMemType{
	normal,
	pointer,
	p2Pointer
}

interface IVariablesMemRefer {
	name: string;
	type: VarMemType;  
}

/**
 * This interface describes the mock-debug specific launch attributes
 * (which are not part of the Debug Adapter Protocol).
 * The schema for these attributes lives in the package.json of the mock-debug extension.
 * The interface should always match this schema.
 */
interface ILaunchRequestArguments extends DebugProtocol.LaunchRequestArguments {
	/** An absolute path to the "program" to debug. */
	program: string;

	/** chip target to debug. */
	chipTarget:string;

	/** chip target to debug. */
	demoBuildPath:string;

	/** Automatically stop target on entry after launch. If not specified, target does not stop. */
	stopOnEntry?: boolean;
	/** enable logging the Debug Adapter Protocol */
	trace?: boolean;
	/** run without debugging */
	noDebug?: boolean;
}

export class QCAdebugSession extends LoggingDebugSession {

	// we don't support multiple threads, so we can use a hardcoded ID for the default thread
	private static threadID = 1;

	// a QCA-IDE GDB runtime
	private _runtime: QcaGdbRuntime;
	private _waitingBackraceRsp = false;
	private _waitingEvaluateRsp = false;

	private _variableHandles = new Handles<string|IVariablesMemRefer>();

	private _configurationDone = new awaitNotify.Subject();

	private _cancelationTokens = new Map<number, boolean>();
	private _isLongrunning = new Map<number, boolean>();

	private _reportProgress = false;
	private _progressId = 10000;
	private _cancelledProgressId: string | undefined = undefined;
	private _isProgressCancellable = true;

	private _useInvalidatedEvent = false;

	private _scopeFrameId = 0;

	private _timeoutArray = new Map<string, NodeJS.Timeout>();

	private _variableRefMap = new Map<string,number>();

	/**
	 * Creates a new debug adapter that is used for one debug session.
	 * We configure the implementation of QCA-IDE debug adapter here.
	 */
	public constructor(fileAccessor: FileAccessor) {
		super("qca_debug_adapter_log.txt");

		// this debugger uses 1-based lines and columns
		this.setDebuggerLinesStartAt1(true);
		this.setDebuggerColumnsStartAt1(true);

		this._runtime = new QcaGdbRuntime(fileAccessor);

		// setup event handlers
		this._runtime.on('stopOnEntry', () => {
			this.sendEvent(new StoppedEvent('entry', QCAdebugSession.threadID));
		});
		this._runtime.on('stopOnStep', () => {
			this.sendEvent(new StoppedEvent('step', QCAdebugSession.threadID));
		});
		this._runtime.on('stopOnBreakpoint', () => {
			this.sendEvent(new StoppedEvent('breakpoint', QCAdebugSession.threadID));
		});
		this._runtime.on('stopOnPause', () => {
			this.sendEvent(new StoppedEvent('pause', QCAdebugSession.threadID));
		});
		this._runtime.on('stopOnDataBreakpoint', () => {
			this.sendEvent(new StoppedEvent('data breakpoint', QCAdebugSession.threadID));
		});
		this._runtime.on('stopOnException', (exception) => {
			if (exception) {
				this.sendEvent(new StoppedEvent(`exception(${exception})`, QCAdebugSession.threadID));
			} else {
				this.sendEvent(new StoppedEvent('exception', QCAdebugSession.threadID));
			}
		});
		this._runtime.on('breakpointValidated', (bp: IBreakpoint) => {
			this.sendEvent(new BreakpointEvent('changed', { verified: bp.verified, id: bp.id,line:bp.line } as DebugProtocol.Breakpoint));
		});
		this._runtime.on('breakpointRemoved', (bp: IBreakpoint) => {
			this.sendEvent(new BreakpointEvent('removed', { verified: bp.verified, id: bp.id,line:bp.line } as DebugProtocol.Breakpoint));
		});
		this._runtime.on('output', (text, filePath, line, column) => {
			const e: DebugProtocol.OutputEvent = new OutputEvent(`${text}\n`);

			if (text === 'start' || text === 'startCollapsed' || text === 'end') {
				e.body.group = text;
				e.body.output = `group-${text}\n`;
			}

			e.body.source = this.createSource(filePath);
			e.body.line = this.convertDebuggerLineToClient(line);
			e.body.column = this.convertDebuggerColumnToClient(column);
			this.sendEvent(e);
		});
		this._runtime.on('end', () => {
			this.sendEvent(new TerminatedEvent());
		});
	}

	/**
	 * The 'initialize' request is the first request called by the frontend
	 * to interrogate the features the debug adapter provides.
	 */
	protected async initializeRequest(response: DebugProtocol.InitializeResponse, args: DebugProtocol.InitializeRequestArguments) {

		if (args.supportsProgressReporting) {
			this._reportProgress = true;
		}
		if (args.supportsInvalidatedEvent) {
			this._useInvalidatedEvent = true;
		}

		// build and return the capabilities of this debug adapter:
		response.body = response.body || {};

		// the adapter implements the configurationDoneRequest.
		response.body.supportsConfigurationDoneRequest = true;

		// make VS Code use 'evaluate' when hovering over source
		response.body.supportsEvaluateForHovers = false;

		// make VS Code show a 'step back' button
		response.body.supportsStepBack = false;

		// make VS Code support data breakpoints
		response.body.supportsDataBreakpoints = true;

		// make VS Code support completion in REPL
		response.body.supportsCompletionsRequest = false;
		response.body.completionTriggerCharacters = [ ".", "[" ];

		// make VS Code send cancelRequests
		response.body.supportsCancelRequest = false;

		response.body.supportsDelayedStackTraceLoading = false;

		// make VS Code send the breakpointLocations request
		response.body.supportsBreakpointLocationsRequest = false;

		// make VS Code provide "Step in Target" functionality
		response.body.supportsStepInTargetsRequest = false;
		
		// make VS Code provide "ReadMemory" functionality
		response.body.supportsReadMemoryRequest  = true;

		// make VS Code provide "Disassemble" functionality
		response.body.supportsDisassembleRequest  = true;

		response.body.supportsValueFormattingOptions = true;

		// response.body.supportsSetVariable = true;
		response.body.supportsSetExpression = true;

		response.body.supportsInstructionBreakpoints = false;

		// support step granularity 
		// https://microsoft.github.io/debug-adapter-protocol/specification
		response.body.supportsSteppingGranularity = true;

		// the adapter defines two exceptions filters, one with support for conditions.
		response.body.supportsExceptionFilterOptions = false;
		response.body.exceptionBreakpointFilters = [
			{
				filter: 'namedException',
				label: "Named Exception",
				description: `Break on named exceptions. Enter the exception's name as the Condition.`,
				default: false,
				supportsCondition: true,
				conditionDescription: `Enter the exception's name`
			},
			{
				filter: 'otherExceptions',
				label: "Other Exceptions",
				description: 'This is a other exception',
				default: true,
				supportsCondition: false
			}
		];

		// make VS Code send exceptionInfoRequests
		response.body.supportsExceptionInfoRequest = false;

		// start debug backend
		await this._runtime.start();

		this.sendResponse(response);

		// since this debug adapter can accept configuration requests like 'setBreakpoint' at any time,
		// we request them early by sending an 'initializeRequest' to the frontend.
		// The frontend will end the configuration sequence by calling 'configurationDone' request.
		this.sendEvent(new InitializedEvent());
	}

	/**
	 * Called at the end of the configuration sequence.
	 * Indicates that all breakpoints etc. have been sent to the DA and that the 'launch' can start.
	 */
	protected configurationDoneRequest(response: DebugProtocol.ConfigurationDoneResponse, args: DebugProtocol.ConfigurationDoneArguments): void {
		super.configurationDoneRequest(response, args);

		// notify the launchRequest that configuration has finished
		this._configurationDone.notify();
	}

	protected async launchRequest(response: DebugProtocol.LaunchResponse, args: ILaunchRequestArguments) {

		// make sure to 'Stop' the buffered logging if 'trace' is not set
		logger.setup(args.trace ? Logger.LogLevel.Verbose : Logger.LogLevel.Stop, false);

		// wait until configuration has finished (and configurationDoneRequest has been called)
		await this._configurationDone.wait(1000);

		// continue the program
		this._runtime.ifStopOnEntry(!!args.stopOnEntry);
		// this._runtime.continue();

		this.sendResponse(response);
	}

	protected async setBreakPointsRequest(response: DebugProtocol.SetBreakpointsResponse, args: DebugProtocol.SetBreakpointsArguments): Promise<void> {

		let path = args.source.path as string;
		let needContinue = false;

		// for rom file, the absolute file path is not valid in gdb, so just use basename
		let file = basename(path);

		let bps = this._runtime._breakPoints.get(file);

	
		/** Note: not use, since gdb will check itself
		// load file source first
		// await this._runtime.loadSource(path);
		// check invalid bp and update it, exclude null line and "#""//",
		// this._runtime.verifyAllBpkts(file, args.breakpoints);
		*/

		// if target not stopped, need stop first
		if(this._runtime.dbgbackendIsRunning()){
			this._runtime.interruptTarget();
			needContinue = true;
		}

		// 1. delete breakpoints that do not need for this file
		this._runtime.deleteBreakpoints(file,args.breakpoints);
		// 2. get retain bpkts
		let ratainBpkts: Breakpoint[]= this._runtime.getRetainBreakpoints(file);
		// 3. get new bpkts that need to be added
		let newBpktsLines = this._runtime.getNewBreakpoints(file,args.breakpoints);

		let rspBpkts:Breakpoint[];
		if(newBpktsLines.length >0){
			const actualBreakpoints0 = newBpktsLines.map(async l => {
				const { verified, line, id } =  await this._runtime.setBreakPoint(file, this.convertClientLineToDebugger(l));
				const bp = new Breakpoint(verified, this.convertDebuggerLineToClient(line)) as DebugProtocol.Breakpoint;
				bp.id= id;
				return bp;
			});
			const actualBreakpoints = await Promise.all<DebugProtocol.Breakpoint>(actualBreakpoints0);
			// @ts-expect-error
			rspBpkts= actualBreakpoints.concat(ratainBpkts);
		}
		else{
			rspBpkts= ratainBpkts;
		}

		// need be sort before rsp, since UI will not be in orderd
		rspBpkts.sort((a:any,b:any)=>{
			return a.line - b.line;
		});

		if(needContinue){
			this._runtime.continue();
		}

		// send back the actual breakpoint positions
		response.body = {
			breakpoints: rspBpkts
		};
		this.sendResponse(response);
	}


	//TODO:
	protected async setExceptionBreakPointsRequest(response: DebugProtocol.SetExceptionBreakpointsResponse, args: DebugProtocol.SetExceptionBreakpointsArguments): Promise<void> {

		let namedException: string | undefined = undefined;
		let otherExceptions = false;

		if (args.filterOptions) {
			for (const filterOption of args.filterOptions) {
				switch (filterOption.filterId) {
					case 'namedException':
						namedException = args.filterOptions[0].condition;
						break;
					case 'otherExceptions':
						otherExceptions = true;
						break;
				}
			}
		}

		if (args.filters) {
			if (args.filters.indexOf('otherExceptions') >= 0) {
				otherExceptions = true;
			}
		}

		this._runtime.setExceptionsFilters(namedException, otherExceptions);

		this.sendResponse(response);
	}

	//TODO:
	protected exceptionInfoRequest(response: DebugProtocol.ExceptionInfoResponse, args: DebugProtocol.ExceptionInfoArguments) {
		response.body = {
			exceptionId: 'Exception ID',
			description: 'This is a descriptive description of the exception.',
			breakMode: 'always',
			details: {
				message: 'Message contained in the exception.',
				typeName: 'Short type name of the exception object',
				stackTrace: 'stack frame 1\nstack frame 2',
			}
		};
		this.sendResponse(response);
	}

	//TODO:
	protected threadsRequest(response: DebugProtocol.ThreadsResponse): void {

		// runtime supports no threads so just return a default thread.
		response.body = {
			threads: [
				new Thread(QCAdebugSession.threadID, "thread 1")
			]
		};
		this.sendResponse(response);
	}


	protected setExpressionRequest (response: DebugProtocol.SetExpressionResponse, args: DebugProtocol.SetExpressionArguments, request?: DebugProtocol.Request): void {

		if(!this._runtime.dbgbackendIsStopped()){
			console.error('stack trace when target not stopped is invalid.'); 
			this.sendResponse(response);
			return;
		}
		let param = args.expression + ' ' + args.value;
		this._runtime.setExpression(param);
		console.log('dbgAdapter: setExpression ' + param +' to backend.');	
		
		let timeout = setTimeout(()=> {
			// evt has not been fired
			this.sendResponse(response);
			this._runtime.removeAllListeners('setExpressionReady');

		}, 3000);

		this._runtime.once('setExpressionReady',()=>{
			clearTimeout(timeout);
			const value = this._runtime.getExpressionValue();

			response.body = {
				value:value
			};

			console.log('dbgAdapter: send setExpressionRequest rsp to front');
			this.sendResponse(response);

		});

		
	}

	
	protected  disassembleRequest(response: DebugProtocol.DisassembleResponse, args: DebugProtocol.DisassembleArguments, request?: DebugProtocol.Request): void{
		const instructionsRspArray : DebugProtocol.DisassembledInstruction[]=[];

		if(!this._runtime.dbgbackendIsStopped()){
			console.error('read disassemble when target not stopped is invalid.'); 
			vscode.window.showErrorMessage('read disassemble when target not stopped is invalid.');
			this.sendResponse(response);
			return;
		}
		//here memoryReference is instructionPointerReference
		this._runtime.readDisassemble("-s " + ("0x" + (parseInt(args.memoryReference, 16) + (args.instructionOffset ? args.instructionOffset : 0)).toString(16)) + " -e " + ("0x" + (parseInt(args.memoryReference, 16) + (args.instructionOffset ? args.instructionOffset : 0) + args.instructionCount).toString(16)) );
 
		let eventName = 'disassembleList';
		let timeout = setTimeout(() => {
			this.sendResponse(response);
			this._runtime.removeAllListeners(eventName);
		}, 25000);

		this._runtime.once(eventName,()=>{
			clearTimeout(timeout);

			const srcLines :any= this._runtime.getDisassembleList();

			for(let i=0; i< srcLines.length;i++)
			{
				let instructions = srcLines[i].line_asm_insn;
				for(let j=0; j< instructions.length;j++)
				{
					const loc = this.createSource(srcLines[i].fullname);
					const instr : DebugProtocol.DisassembledInstruction = {
							address: instructions[j].address,
							instruction: instructions[j].inst,
							location:loc,
							line:this.convertDebuggerLineToClient(srcLines[i].line)
					};

					if(instructionsRspArray.length >= args.instructionCount)
					{
						break;
					}

					instructionsRspArray.push(instr);
				}
			}

			if(instructionsRspArray.length < args.instructionCount)
			{
				for (let i = 0; i < (args.instructionCount - instructionsRspArray.length); i++) {
					let tepInstr : DebugProtocol.DisassembledInstruction = {
						address: 'null',
						instruction: 'invalid instruction'
				};
					instructionsRspArray.push(tepInstr);
				}
			}
	
			response.body = {
				instructions: instructionsRspArray
			};

			this.sendResponse(response);
		});

		
	}

	/**
	 * @description this will be sent by front when it receives stop event, like when meet one bkpt 
	 * @param response 
	 * @param args 
	 */
	protected stackTraceRequest(response: DebugProtocol.StackTraceResponse, args: DebugProtocol.StackTraceArguments): void {

		const startFrame = typeof args.startFrame === 'number' ? args.startFrame : 0;
		const maxLevels = typeof args.levels === 'number' ? args.levels : 1000;
		const endFrame = startFrame + maxLevels;

		if(!this._runtime.dbgbackendIsStopped()){
			console.error('stack trace when target not stopped is invalid.'); 
			this.sendResponse(response);
			return;
		}

		this._runtime.stack(startFrame, endFrame);
		console.log('dbgAdapter: send backtrace list cmd to backend.');	
		
		let timeout = setTimeout(()=> {
			// evt has not been fired
			this.sendResponse(response);
			this._runtime.removeAllListeners('backtraceReady');

		}, 30000);

		this._runtime.once('backtraceReady',()=>{
			clearTimeout(timeout);
			const stk = this._runtime.getFrames();

			response.body = {
				stackFrames: stk.frames.map(f => {
					// f.file do some process including rom file and .o.i file
					f.file = this.preProcessFile(f.file);
					const sf:DebugProtocol.StackFrame = new StackFrame(f.index, f.name, this.createSource(f.file), this.convertDebuggerLineToClient(f.line));
					sf.instructionPointerReference = f.address;
					return sf;
				}),

				totalFrames: stk.count
			};

			console.log('dbgAdapter: send backtrace rsp to front');
			this.sendResponse(response);

		});

	}

	protected scopesRequest(response: DebugProtocol.ScopesResponse, args: DebugProtocol.ScopesArguments): void {
		this._scopeFrameId = args.frameId;
		this._runtime.changeFrame(this._scopeFrameId);
		response.body = {
			scopes: [
				new Scope("Local", this._variableHandles.create("local"), false),
				// new Scope("Global", this._variableHandles.create("global"), true),
				new Scope("Register", this._variableHandles.create("register"), true)
			]
		};
		this.sendResponse(response);
	}
	
	protected async readMemoryRequest(response: DebugProtocol.ReadMemoryResponse, args: DebugProtocol.ReadMemoryArguments) {
		const variable :IVariablesMemRefer= this._variableHandles.get(Number(args.memoryReference)) as IVariablesMemRefer;

		if(!this._runtime.dbgbackendIsStopped()){
			console.error('read memory when target not stopped is invalid.'); 
			vscode.window.showErrorMessage('read memory when target not stopped is invalid.');
			this.sendResponse(response);
			return;
		}
		// default count is 200
		let param ;
		if(variable.type === VarMemType.normal)
		{
			param = '&' + variable.name +' ' + String(200);
		}else if(variable.type === VarMemType.pointer){
			param = variable.name +' ' + String(200);
		}else{
			param = '*'+variable.name +' ' + String(200);
		}

		this._runtime.readMem(param);

		let eventName = 'memoryVscode';
		let timeout = setTimeout(() => {
			this.sendResponse(response);
			this._runtime.removeAllListeners(eventName);
		}, 3000);

		this._runtime.once(eventName,()=>{
			clearTimeout(timeout);
			let memData = this._runtime.getMem();
			let memBase64 = Buffer.from(memData.contents, 'hex').toString('base64');
			response.body = {
				address: memData.begin,
				data: memBase64
			};
			this.sendResponse(response);
		});
		
	}

	protected async variablesRequest(response: DebugProtocol.VariablesResponse, args: DebugProtocol.VariablesArguments, request?: DebugProtocol.Request) {

		let variableHandlesValue = this._variableHandles.get(args.variablesReference);

		if (variableHandlesValue === 'local'){
			this.processLocalVarReq(response);
		}

		else if(variableHandlesValue === 'register'){
			this.processRegVarReq(response);
		}

		// Note: not supported
		else if(variableHandlesValue === 'global'){
			this.proceeGlobalVarReq(response);
			this.sendResponse(response);
		}

		// variable is structed child
		else if(variableHandlesValue){
			this.processStructedVar(response,variableHandlesValue as string);
		}

		else{
			this.sendResponse(response);
		}


	}

	protected continueRequest(response: DebugProtocol.ContinueResponse, args: DebugProtocol.ContinueArguments): void {
		this._runtime.continue();
		this.sendResponse(response);
	}


	protected nextRequest(response: DebugProtocol.NextResponse, args: DebugProtocol.NextArguments): void {
		if(this._runtime.dbgbackendIsStopped()){
			if(args.granularity !== 'instruction')
			{
				this._runtime.step();
			}
			else{
				this._runtime.stepInstr();
			}
		}
		else{
			console.error('stepover when target not stopped is invalid.'); 
		}
		this.sendResponse(response);
	}


	protected stepInRequest(response: DebugProtocol.StepInResponse, args: DebugProtocol.StepInArguments): void {
	
		if(args.granularity !== 'instruction')
		{
			this._runtime.stepIn(args.threadId);
		}
		else{
			this._runtime.stepInInstr();
		}
		this.sendResponse(response);
	}

	protected stepOutRequest(response: DebugProtocol.StepOutResponse, args: DebugProtocol.StepOutArguments): void {
		this._runtime.stepOut();
		this.sendResponse(response);
	}

	protected async evaluateRequest(response: DebugProtocol.EvaluateResponse, args: DebugProtocol.EvaluateArguments): Promise<void> {

		args.expression = args.expression.replace(/\s*/g,"");
		if(args.context === 'watch'){
			if(this._runtime.dbgbackendIsStopped()){
				
				if(!this._runtime.createVarObj(args.expression)){
					// 
					this._runtime.deleteVarObj(args.expression);
					this._runtime.createVarObj(args.expression);
				}

			}
			else{
				console.error('evaluate when target not stopped is invalid.'); 
				vscode.window.showErrorMessage('evaluate when target not stopped is invalid.');
				this.sendResponse(response);
				return;
			}

			let timeout = setTimeout(()=> {
				// evt has not been fired
				this.sendResponse(response);
				this._runtime.removeAllListeners(`${args.expression}`);

			}, 3000);

			this._timeoutArray.set(args.expression,timeout);

			this._runtime.once(`${args.expression}`, ()=>{

				let accordingTimeout = this._timeoutArray.get(args.expression);
				if(accordingTimeout){
					clearTimeout(accordingTimeout);
				}

				let value = JSON.parse(this._runtime.getCreateVarObjValue());
				let variablesReference =0 ;
				if('numchild' in value && value['numchild'] > 0){
					// is a struct, or struct pointer
					variablesReference = this._variableHandles.create(value['name']);
				}
				else{
					variablesReference = 0;
				}

				let type = VarMemType.normal;
				if(value.type)
				{
					if(value.type.indexOf('**')!==-1)
					{
						type = VarMemType.p2Pointer;
					}else if(value.type.indexOf('*')!==-1){
						type = VarMemType.pointer;
					}else{
						type = VarMemType.normal;
					}

				}

				let memReferVar:IVariablesMemRefer={
					name:value.name,
					type:type
				};

				response.body = {
					result: value['value'],
					variablesReference: variablesReference,
					memoryReference:String(this._variableHandles.create(memReferVar))
				};
				this.sendResponse(response);
			});
		}	

	}


	protected dataBreakpointInfoRequest(response: DebugProtocol.DataBreakpointInfoResponse, args: DebugProtocol.DataBreakpointInfoArguments): void {
		// TODO: enable this
		response.body = {
            dataId: null,
            description: "cannot break on data access",
            accessTypes: undefined,
            canPersist: false
        };

		if (args.variablesReference && args.name) {
			const id = this._variableHandles.get(args.variablesReference);
			if (id === "global") {
				response.body.dataId = args.name;
				response.body.description = args.name;
				response.body.accessTypes = [ "write" ];
				response.body.canPersist = true;
			} else if (id === "local"){
				response.body.dataId = args.name;
				response.body.description = args.name;
				response.body.accessTypes = [ "write" ];
				response.body.canPersist = false;
			}

		}

		this.sendResponse(response);
	}

	protected async setDataBreakpointsRequest(response: DebugProtocol.SetDataBreakpointsResponse, args: DebugProtocol.SetDataBreakpointsArguments): Promise<void> {

		let needContinue = false;
		if (args.breakpoints.length === 0){
			this.sendResponse(response);
			return;
		}

		if(this._runtime.dbgbackendIsRunning()){
			this._runtime.interruptTarget();
			needContinue = true;
		}
		// clear all data breakpoints
		this._runtime.clearAllDataBreakpoints();

		const actualBreakpoints0 =  args.breakpoints.map(async dbp => {
			const { verified, id } = await this._runtime.setDataBreakpoint(dbp.dataId);
			const bp = new Breakpoint(verified) as DebugProtocol.Breakpoint;
			bp.id= id;
			return bp;
		});

		const actualBreakpoints = await Promise.all<DebugProtocol.Breakpoint>(actualBreakpoints0);
		
		if(needContinue){
			this._runtime.continue();
		}

		// send back the actual watchpoint
		response.body = {
			breakpoints: actualBreakpoints
		};
		this.sendResponse(response);

	}

	protected completionsRequest(response: DebugProtocol.CompletionsResponse, args: DebugProtocol.CompletionsArguments): void {

		response.body = {
			targets: [
				{
					label: "item 10",
					sortText: "10"
				},
				{
					label: "item 1",
					sortText: "01"
				},
				{
					label: "item 2",
					sortText: "02"
				},
				{
					label: "array[]",
					selectionStart: 6,
					sortText: "03"
				},
				{
					label: "func(arg)",
					selectionStart: 5,
					selectionLength: 3,
					sortText: "04"
				}
			]
		};
		this.sendResponse(response);
	}

	protected cancelRequest(response: DebugProtocol.CancelResponse, args: DebugProtocol.CancelArguments) {
		if (args.requestId) {
			this._cancelationTokens.set(args.requestId, true);
		}
		if (args.progressId) {
			this._cancelledProgressId= args.progressId;
		}
	}

	protected customRequest(command: string, response: DebugProtocol.Response, args: any) {
		if (command === 'readMem') {
			
			this.readMem(args,response);

		}else if(command === 'readDisassemble'){
			this.readDisassemble(args,response);
		} 
		else {
			super.customRequest(command, response, args);
		}
	}

	protected disconnectRequest(response: DebugProtocol.DisconnectResponse, args: DebugProtocol.DisconnectArguments, request?: DebugProtocol.Request):void{
		console.log('dbgAdapter: exit dbgbackend.');

		if(this._runtime.dbgbackendIsRunning()){
			this._runtime.interruptTarget();
		}

		this._runtime.exitDbgbackend();
		
		// async process to wait gdb run time socket client be closed

		this.sendResponse(response);
	
		
	}

    protected pauseRequest(response: DebugProtocol.PauseResponse, args: DebugProtocol.PauseArguments, request?: DebugProtocol.Request): void{
		console.log('dbgAdapter: pause the target');
		this._runtime._wantUIStopped = true;
		this._runtime.interruptTarget();
		this.sendResponse(response);
	}

	//---- helpers-------

	private createSource(filePath: string): Source {
		return new Source(basename(filePath), this.convertDebuggerPathToClient(filePath), undefined, undefined);
	}

	/**
	 * @deprecated it's use for process rsp of -data-evaluate-expression 
	 * @description 
	 * value like:  "{n  year = 0x7e4, n  month = 0x1, n  day = 0x1, n  hour = 0x0, n  minute = 0x14, n  second = 0x5, n  day_Of_Week = 0x2n}"
	 * @param valueRsp 
	 */
	private parseValueRsp(valueRsp:string){
		let valueArray = valueRsp.replace(/{n/g,"").replace(/n}/g,"").split(',');
		return valueArray ;
	}

	private isPointerType(type:string):boolean{
		if(type.indexOf('*') !== -1){
			return true;
		}
		else
		{
			return false;
		}
	}



	private processLocalVarReq(response: DebugProtocol.VariablesResponse){
		const variables: DebugProtocol.Variable[] = [];
		this._runtime.listVariables();
		let timeout = setTimeout(()=> {
			// evt has not been fired
			this.sendResponse(response);
			this._runtime.removeAllListeners(`variablesReady`);

		}, 15000);
		this._runtime.once('variablesReady',()=>{
			clearTimeout(timeout);
			const variablesRsp = this._runtime.getVar();
			variablesRsp.variables.map(v=>{
				let variablesReference = 0;
				
				if(this.isPointerType(v.type) && v.value.indexOf('optimized') === -1){
					variablesReference = this._variableHandles.create(v.name);
				}
				else if(!v.value){
					// -stack-list-variables --simple-values, print the name, type and value for simple data types, and the name and type for arrays, structures and unions.
					// https://www.sourceware.org/gdb/onlinedocs/gdb/GDB_002fMI-Stack-Manipulation.html#GDB_002fMI-Stack-Manipulation
					variablesReference = this._variableHandles.create(v.name);
				}

				if(variablesReference > 0){
					if(!this._runtime.createVarObj(v.name)){
						// 
						// this._runtime.deleteVarObj(v.name);
						// this._runtime.createVarObj(v.name);
					}

				}
				
				let type = VarMemType.normal;
				if(v.type.indexOf('**')!==-1)
				{
					type = VarMemType.p2Pointer;
				}else if(v.type.indexOf('*')!==-1){
					type = VarMemType.pointer;
				}else{
					type = VarMemType.normal;
				}

				let memReferVar:IVariablesMemRefer={
					name:v.name,
					type:type
				};

				variables.push({
					name: v.name,
					type: v.type,
					value: v.value,
					variablesReference: variablesReference,
					memoryReference:String(this._variableHandles.create(memReferVar))
				});
			});

			response.body = {
				variables: variables
			};
			this.sendResponse(response);

		});
	}

	private proceeGlobalVarReq(response: DebugProtocol.VariablesResponse){
		const variables: DebugProtocol.Variable[] = [];
		this._runtime.listGlobalVariables();

	}

	private processRegVarReq(response: DebugProtocol.VariablesResponse){
		const variables: DebugProtocol.Variable[] = [];
		this._runtime.listRegName();
		this._runtime.listRegValue();
		let timeout = setTimeout(()=> {
			// evt has not been fired
			this.sendResponse(response);
			this._runtime.removeAllListeners(`regsReady`);

		}, 15000);
		this._runtime.once('regsReady',()=>{
			clearTimeout(timeout);
			const variablesRsp = this._runtime.getReg();
			variablesRsp.regs.map(v=>{
				
				variables.push({
					name: v.name?v.name:'',
					value: v.value?v.value:'',
					variablesReference: 0
				});
			});

			response.body = {
				variables: variables
			};
			this.sendResponse(response);

		});
	}

	
	/**
	 * @description this fun will be call many times when you watch structed variable
	 * @param response 
	 * @param variableHandlesValue 
	 */
	private processStructedVar(response: DebugProtocol.VariablesResponse,
		variableHandlesValue:string){
		const variables: DebugProtocol.Variable[] = [];
		
		// variableHandlesValue is variable name
		let eventName = 'listVarObjChild=='+variableHandlesValue;
		this._runtime.updateVarObjChild(variableHandlesValue);
		this._runtime.listVarObjChild(variableHandlesValue);

		let timeout = setTimeout(()=> {
			// evt has not been fired
			this.sendResponse(response);
			this._runtime.removeAllListeners(`${eventName}`);

		}, 10000);

		this._timeoutArray.set(eventName,timeout);
		
		// triggled by onListVarObjChild()
		this._runtime.once(`${eventName}`,()=>{
			
			let accordingTimeout = this._timeoutArray.get(eventName);
			if(accordingTimeout){
				clearTimeout(accordingTimeout);
			}

			const listVarObjChildRsp = this._runtime.getListVarObjChildValue();

			if(listVarObjChildRsp.length>0){
				listVarObjChildRsp.map( (v :any)=>{
					let variablesReference = 0;
					if(v['numchild'] > 0){
						variablesReference = this._variableHandles.create(v['name']);
					}

					let type = VarMemType.normal;
					if(v.type)
					{
						if(v.type.indexOf('**')!==-1)
						{
							type = VarMemType.p2Pointer;
						}else if(v.type.indexOf('*')!==-1){
							type = VarMemType.pointer;
						}else{
							type = VarMemType.normal;
						}
	
					}

					let memReferVar:IVariablesMemRefer={
						name:v.name,
						type:type
					};
		

					variables.push({
						name: v['exp'],
						value: v['value']?v['value']:'',
						variablesReference: variablesReference,
						memoryReference:String(this._variableHandles.create(memReferVar))
					});
				});
			}

			response.body = {
				variables: variables
			};
			this.sendResponse(response);

		});
	}

	private preProcessFile(file:string){
		let sdkParentDir = qualConfig.readParameter('qualIoe.staticCfg.sdkParentDir');
		let newFile = file;
		if(!canAccessFile(file)){
			// for rom and asm file
			let reg = new RegExp(`${sdkParentDir}.*`);
			
			let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath') as string;
			sdkPath = sdkPath.replace(`${sdkParentDir}`,'');

			let extensionName = extname(file);

			let filePathArray = file.match(reg);
			let filePath ='';
			if(filePathArray && filePathArray.length >0){
				filePath = join(sdkPath, filePathArray[0]);
				newFile = filePath;
			}
			else{
				// should not happen
				return newFile;
			}

			if(extensionName ==='.i'){
				// asm file
				let fileName = basename(file);
				// try .S
				let newFileName = fileName.replace('.o.i','.S');
				newFile = filePath.replace(`${fileName}`,`${newFileName}`);
				if(!canAccessFile(newFile)){
					// try .s 
					newFileName = fileName.replace('.o.i','.s');
					newFile = filePath.replace(`${fileName}`,`${newFileName}`);
				}
			}
			return newFile;
		}
		else{
			return newFile;
		}
	}

	private readMem(args:any, response:DebugProtocol.Response){

		if(!this._runtime.dbgbackendIsStopped()){
			console.error('read memory when target not stopped is invalid.'); 
			vscode.window.showErrorMessage('read memory when target not stopped is invalid.');
			this.sendResponse(response);
			return;
		}

		this._runtime.readMem(args);

		let eventName = 'memory';
		let timeout = setTimeout(() => {
			this.sendCustomEvent('readMem',response);
			this.sendResponse(response);
			this._runtime.removeAllListeners(eventName);
		}, 30000);

		this._runtime.once(eventName,()=>{
			clearTimeout(timeout);
			let memData = this._runtime.getMem();
			response.body = memData;
			this.sendCustomEvent('readMem',response);
			this.sendResponse(response);
		});

	}

	private readDisassemble(args:any, response:DebugProtocol.Response){

		if(!this._runtime.dbgbackendIsStopped()){
			console.error('read disassemble when target not stopped is invalid.'); 
			vscode.window.showErrorMessage('read disassemble when target not stopped is invalid.');
			this.sendResponse(response);
			return;
		}

		this._runtime.readDisassemble(args);

		let eventName = 'disassemble';
		let timeout = setTimeout(() => {
			this.sendCustomEvent('readDisassemble',response);
			this.sendResponse(response);
			this._runtime.removeAllListeners(eventName);
		}, 3000);

		this._runtime.once(eventName,()=>{
			clearTimeout(timeout);
			let disassembleData = this._runtime.getDisassemble();
			response.body = disassembleData;
			this.sendCustomEvent('readDisassemble',response);
			this.sendResponse(response);
		});

	}

	// https://github.com/firefox-devtools/vscode-firefox-debug/blob/8b7c81a6360ae005a40d3e626c877e2537cd8248/src/adapter/adapter/addonManager.ts#L51
	public sendCustomEvent(eventType: string, eventBody: any): void {
		let event:DebugProtocol.Event ={
			event:eventType,
			body:eventBody,
			seq:0,
			type:'event'
		};
		this.sendEvent(event);
	}

}


