/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

// The module is to parse the tools\qccide\qccIdeProjConfig.json

import {isStrDirectory} from "../utils/check";
import * as fs from 'fs';
import { join,basename,delimiter } from "path";


export enum PrjCfgSetTypeIndex {
    chipIdIndex = 0,
    boardIndex,
    rtosIndex,
    demoIndex
}

export enum PrjCfgBuildTypeIndex {
    buildIndex = 0,
    rebuildIndex
}

export enum PrjCfgCleanTypeIndex {
    cleanIndex = 0
}

export enum PrjCfgFlashTypeIndex {
    flashIndex = 0,
    flashResetIndex
}

export function isSdkRootPath(fsPath:string,demoPathFromRoot:string){
	if(!isStrDirectory(fsPath)){
        return false;
    }

    if(fs.existsSync(join(fsPath,demoPathFromRoot)))
    {
        return true;
    }
    else{
        return false;
    }
}


export function isDemoPath(fsPath:string,demoFolderName:string){
	if(!isStrDirectory(fsPath)){
        return false;
    }
	if(fsPath.indexOf(demoFolderName)){
		return true;
	}
	else{
		return false;
	}
}



export function getSdkFromDemoPath(fsPath:string,demoPathFromRoot:string,demoFolderName:string){
	if(!isStrDirectory(fsPath)){
        return '';
    }
	if(isDemoPath(fsPath,demoFolderName)){
        let reg = new RegExp(`.*(?=`+ demoPathFromRoot +`)`);
        let path  = fsPath.match(reg);
    
        if(path && path[0]){
            return path[0];
        }
        else {
            return '';
        }
	}
	else{
		return '';
	}
}
