/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import * as sqlite3 from 'sqlite3';
import { join } from 'path';
import * as fs from 'fs';
import * as vscode from 'vscode';
import{GNAME} from '../config/globalName';

import * as utils from '../utils/utils'; 
import {ideUserPath,extensionPath, ideUserCfgFolderPath,ideUserDownloadCfgFolderPath,ideUserSdkDownloadFolderPath,ideUserDownloadCfgURIFolderPath,ideUserPyVenvFolderPath,cCppPropertiesName, updateSdkScopeEnv} from '../extension';
import * as qualConfig from '../config/globalConfigure';

let dbRelativePath='database/ide.db';
export let boardDepFileRelativePath='database/Boards';


function cpSdkScopeEnvFileToLocal(sdkName:string,sdkVer:string)
{
  if(vscode.workspace.workspaceFolders){
    // if(!fs.existsSync(join(vscode.workspace.workspaceFolders[0].uri.fsPath,GNAME.sdkScopeEnvFileName)))
    {
      let srcPath;
      let dnldSdkpath = join(ideUserSdkDownloadFolderPath,sdkVer + '^'+ sdkName,GNAME.sdkDnldEnvFileName);
      let cfgContent = fs.readFileSync(dnldSdkpath).toString();
      let cfgJson = JSON.parse(cfgContent);
      if(cfgJson && "sdk_root_path_from_download_path" in cfgJson){
        srcPath = join(ideUserSdkDownloadFolderPath, sdkVer + '^'+ sdkName, cfgJson['sdk_root_path_from_download_path'], GNAME.sdkScopeEnvFileName);
        fs.copyFileSync(srcPath,join(vscode.workspace.workspaceFolders[0].uri.fsPath,GNAME.sdkScopeEnvFileName));
      }

      updateSdkScopeEnv();
    }
  }
}

/**
 * @description this fun create uri json file from local database and install all dependency use python
 * @deprecated
 * @param sdkName 
 * @param sdkVer 
 */
function installByLocalDb(sdkName:string,sdkVer:string,ideDb:sqlite3.Database,boardName?:string){

    let sqlQuery ;

    // 1. check install status from user space's cfg
    sqlQuery = `SELECT * FROM SDKLIST WHERE Sdk_Name="${sdkName}" AND Sdk_Ver="${sdkVer}"`;
    console.log(sqlQuery);

    ideDb.all(sqlQuery,(err,rows:any)=>{

      console.log(rows);

      if(rows.length === 1){

        let choosenSdk = `${sdkVer}_${sdkName}`;
        
        // should check user space's cfg
        let sdkStatusFileName = choosenSdk+'.json';
        utils.createFile(ideUserDownloadCfgFolderPath,sdkStatusFileName);
        let sdkStatusContent = fs.readFileSync(join(ideUserDownloadCfgFolderPath,sdkStatusFileName)).toString();
        let sdkStatusJson = JSON.parse(sdkStatusContent);

        if(sdkStatusJson.Sdk_Status===undefined ||  sdkStatusJson.Sdk_Status !== "installed"){

          // 2. if not installed, call python script to install it
          console.log(`Start install ${sdkName} ${sdkVer} ...` );
          vscode.window.showInformationMessage(`Start install ${sdkName} ${sdkVer} ...` );

          // 2.1 generate download json file to user space's cfg for python use
          // 2.1.1 generate sdk download uri json
          let downloadInfoJson:any = {} ;
          let needLogin = 'false';
          downloadInfoJson['Sdk_Name'] = rows[0].Sdk_Name;
          downloadInfoJson['Sdk_Ver'] = rows[0].Sdk_Ver;
          downloadInfoJson['Sdk_Uri'] = rows[0].Sdk_Uri;
          let sdkKeyValueJson  =  JSON.parse(rows[0].Key_Value);
          for (let key in sdkKeyValueJson) {
            downloadInfoJson[key] = sdkKeyValueJson[key];
            if(key === 'need_log_in'){
                needLogin = sdkKeyValueJson[key];
            }
          }
          if(boardName)
          {
            downloadInfoJson['demoChipBoard'] = boardName;
          }
          
          // 2.1.2 generate tools download uri json
          let toosNeedDownloadJson = JSON.parse(rows[0].Tools);
          let toolsNeedNameList = Object.keys(toosNeedDownloadJson);

          let toolSqlQuery = `SELECT * FROM TOOLLIST WHERE Tool_Name in ("${toolsNeedNameList.join("\",\"")}")  `;
          console.log(toolSqlQuery); 
          
          ideDb.all(toolSqlQuery,(err,rows)=>{

            console.log(rows);

            let toolsList :any= [];

            rows.forEach((row:any)=>{
              if(row.Tool_Ver === toosNeedDownloadJson[`${row.Tool_Name}`]){
                let toolInfoJson:any = {};
                toolInfoJson['name'] = row.Tool_Name;
                toolInfoJson['ver'] = row.Tool_Ver;
                toolInfoJson['uri'] = JSON.parse(row.Tool_Uri);
                let toolKeyValueJson  =  JSON.parse(row.Key_Value);
                for (let key in toolKeyValueJson) {
                  toolInfoJson[key] = toolKeyValueJson[key];
                }

                toolsList.push(toolInfoJson);
              }
            });
            
            downloadInfoJson['Tools'] = toolsList;

            // 2.1.3 write to json file
            fs.writeFileSync(join(ideUserDownloadCfgURIFolderPath,sdkStatusFileName),JSON.stringify(downloadInfoJson, null,2));

            let requirementPath = join(extensionPath,'downloadInstall','python');
            let pythonOutput = vscode.window.createOutputChannel('QCCIDE Dependency');
            let pyVenvPath =  ideUserPyVenvFolderPath;

            // 2.2 check python exist and its version, and install module
            vscode.window.withProgress({
              cancellable: true,
              location: vscode.ProgressLocation.Notification,
              title: "Install python venv..."},
              (progress,token)=>{
                pythonOutput.clear();
                pythonOutput.show();
                return utils.checkAndInstallPyVenv(pyVenvPath,requirementPath,pythonOutput);

            })
            .then(async() => {
                 // 2.3 download sdk with progress
                 let userName:string|undefined;
                 let password:string|undefined;

                if(needLogin === 'true')
                {
                    // 2.3.1 enter user name and user pass
                    // input user:password for git clone sdk
                    vscode.window.showInformationMessage("Please enter user name and password in above box.");
                    userName  = await vscode.window.showInputBox({placeHolder:'User name provided by Qualcomm',ignoreFocusOut:false});
                    password = await vscode.window.showInputBox({placeHolder:'User password provided by Qualcomm', ignoreFocusOut:false,password:true});
              
                }

              let pythonPath = qualConfig.readParameter('qualIoe.commonCfg.pythonBinPath');

              vscode.window.withProgress({
                cancellable: true,
                location: vscode.ProgressLocation.Notification,
                title: "Download SDK..."},
                (progress,token)=>{
                  pythonOutput.clear();
                  pythonOutput.show();

                  return utils.usePythonToInstall(pythonPath as string, join(ideUserDownloadCfgURIFolderPath,sdkStatusFileName),userName,password,pythonOutput);
              }).then(()=>{
                // 2.4 if success install, info user to open sdk again
                vscode.window.showInformationMessage(`${sdkName} ${sdkVer} install success, you can now open the SDK` );

                // 2.5 if dep installed request by local Project, need copy sdkScopeEnv.json from .qccide/Sdks/*/sdkScopeEnv.json to local path
                cpSdkScopeEnvFileToLocal(sdkName,sdkVer);
              });

            });

          });
        }else{
          vscode.window.showInformationMessage("Dependency of "+ sdkName+" "+sdkVer+" has been installed.");
          // if dep installed request by local Project, need copy sdkScopeEnv.json from .qccide/Sdks/*/sdkScopeEnv.json to local path
          cpSdkScopeEnvFileToLocal(sdkName,sdkVer);
        }
      }
      else{
        vscode.window.showWarningMessage("Has two record for "+ sdkName+" "+sdkVer+" in database, Pls help report to developer");
      }
    });

}

function getDepFilePath(sdkName:string,sdkVer:string,boardName:string){

  let dbBoardsPath = join(extensionPath,boardDepFileRelativePath);

  let subBoards = fs.readdirSync(dbBoardsPath).filter(function (file) {
    return fs.statSync(dbBoardsPath+'/'+file).isDirectory();
  });

  for(let i = 0 ; i<subBoards.length ;i++)
  {
    if(subBoards[i]  ===  boardName)
    {
      let subBoardPath = join(extensionPath,boardDepFileRelativePath,boardName);
      let subSdks = fs.readdirSync(subBoardPath).filter(function (file) {
        return fs.statSync(subBoardPath+'/'+file).isFile();
      });

      for(let j = 0 ; j<subSdks.length ;j++)
      {
        // subSdks should like 1.1.0^qccsdk_qcc730.json
        const regex: RegExp = /^(.*?)\.json/;
        const matchArray: RegExpMatchArray | null = subSdks[j].match(regex);
        let sdkNameVer : string|undefined = matchArray ? matchArray[1]:undefined;
        if(sdkNameVer)
        {
          let sdkNameVerArray :string[] = sdkNameVer.split('^');
          if(sdkNameVerArray[0] === sdkVer && sdkNameVerArray[1] === sdkName)
          {
            return join(subBoardPath, subSdks[j]);
          }
        }
      }
    }
  }
  return undefined;
}

function installByDepfile(sdkName:string,sdkVer:string,depFilePath:string,boardName?:string){

  if(!fs.existsSync(depFilePath))
  {
    return;
  }

  let depContent = fs.readFileSync(depFilePath).toString();
  if(!depContent)
  {
    return;
  }

  let depFileJson = JSON.parse(depContent);

  let needLogin = 'false';

  let requirementPath = join(extensionPath,'downloadInstall','python');
  let pythonOutput = vscode.window.createOutputChannel('QCCIDE Dependency');
  let pyVenvPath =  ideUserPyVenvFolderPath;

  if("need_log_in" in depFileJson)
  {
    needLogin = depFileJson['need_log_in'] as string;
  }

  // 1. check python exist and its version, and install module
  vscode.window.withProgress({
    cancellable: true,
    location: vscode.ProgressLocation.Notification,
    title: "Install python venv..."},
    (progress,token)=>{
      pythonOutput.clear();
      pythonOutput.show();
      return utils.checkAndInstallPyVenv(pyVenvPath,requirementPath,pythonOutput);

  })
  .then(async() => {
       // 2. download sdk with progress
       let userName:string|undefined;
       let password:string|undefined;

      if(needLogin === 'true')
      {
        // 2.1 enter user name and user pass
        // input user:password for git clone sdk
        vscode.window.showInformationMessage("Please enter user name and password in above box.");
        userName  = await vscode.window.showInputBox({placeHolder:'User name provided by Qualcomm',ignoreFocusOut:false});
        password = await vscode.window.showInputBox({placeHolder:'User password provided by Qualcomm', ignoreFocusOut:false,password:true});
      }

      let pythonPath = qualConfig.readParameter('qualIoe.commonCfg.pythonBinPath');

      vscode.window.withProgress({
        cancellable: false,
        location: vscode.ProgressLocation.Notification,
        title: "Download SDK..."},
        (progress,token)=>{
          pythonOutput.clear();
          pythonOutput.show();

          return utils.usePythonToInstall(pythonPath as string, depFilePath,userName,password,pythonOutput);
      }).then(()=>{
        // 2.2 if success install, info user to open sdk again
        vscode.window.showInformationMessage(`${sdkName} ${sdkVer} install success, you can now open the SDK` );

        // 2.3 if dep installed request by local Project, need copy sdkScopeEnv.json from .qccide/Sdks/*/sdkScopeEnv.json to local path
        cpSdkScopeEnvFileToLocal(sdkName,sdkVer);
      });
  });
}

/**
 * @description this fun create uri json file from local database and install all dependency use python
 * @deprecated
 * @param sdkName 
 * @param sdkVer 
 */
function installByLocalDbWithBoardname(sdkName:string,sdkVer:string,boardName:string){
    // 0. get the cfg that provided by boardName in db
    if(boardName)
    {
        let sqlQuery = `SELECT * FROM BOARDLIST WHERE Board="${boardName}" `;
        let dbPath = join(extensionPath,dbRelativePath);
        let ideDb = new sqlite3.Database(dbPath,sqlite3.OPEN_READONLY);
        console.log(sqlQuery);
        ideDb.all(sqlQuery,(err,rows:any)=>{

            console.log(rows);
            let outBoardname;
            rows.forEach((row:any)=>{
                if(row.Sdk_Name === sdkName)
                {
                    let sdkCfg= JSON.parse(row.Sdk_Cfg);
                    outBoardname = sdkCfg['cfg']['demoChipBoard'];
                }
            });

            installByLocalDb(sdkName,sdkVer,ideDb,outBoardname);

        });
    }
}


/**
 * @description this fun create uri json file from local database and install all dependency use python
 * @deprecated
 * @param sdkName 
 * @param sdkVer 
 */
function installByLocalDbWithoutBoardname(sdkName:string,sdkVer:string){
    let dbPath = join(extensionPath,dbRelativePath);
    let ideDb = new sqlite3.Database(dbPath,sqlite3.OPEN_READONLY);
    installByLocalDb(sdkName,sdkVer,ideDb);
}

export function installSDKAndDep(sdkName:string,sdkVer:string,boardName:string){

    // use local db @deprecated
    // if(boardName){
    //   installByLocalDbWithBoardname(sdkName,sdkVer,boardName);
    // }
    // else
    // {
    //   installByLocalDbWithoutBoardname(sdkName,sdkVer);
    // }
    
    let depFilePath = getDepFilePath(sdkName,sdkVer,boardName);
    if(depFilePath)
    {
      installByDepfile(sdkName,sdkVer,depFilePath);
    }else{
      vscode.window.showWarningMessage('IDE do not contain information of ' + boardName + ' '+ sdkVer+'^'+sdkName);
    }

    // TODO:use remote db server
}