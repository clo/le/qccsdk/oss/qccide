/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import * as vscode from "vscode";
import *as fs from 'fs';

export function isWorkspaceFolderOpen(): boolean|undefined {
    return (
      vscode.workspace.workspaceFolders &&
      vscode.workspace.workspaceFolders.length > 0
    );
  }


export function isStrDirectory(dirPath:string):boolean{
  return fs.existsSync(dirPath) && fs.lstatSync(dirPath).isDirectory();
}