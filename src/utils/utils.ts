/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import * as fs from 'fs';
import * as os from 'os';
import { join,basename,delimiter } from "path";
import * as vscode from 'vscode';
import{GNAME} from '../config/globalName';
import * as cp from 'child_process';
import { compare } from 'compare-versions';
import * as qualConfig from '../config/globalConfigure';
import * as dbWrap from './dbWrap';
import {extensionPYDownloadPath} from '../extension';
import { CommandTask } from '../command/CommandTask';
import { TaskManager } from "../task/taskManager";

let idePyVirtualenv = 'idePyVenv';
let minPyVer = '3.6.0';
let minOCdVer = '20230111';

export function canAccessFile(filePath: string, mode?: number): boolean {
    try {
      // tslint:disable-next-line: no-bitwise
      mode = mode || fs.constants.R_OK | fs.constants.W_OK | fs.constants.X_OK;
      fs.accessSync(filePath, mode);
      return true;
    } catch (error) {
      return false;
    }
}

export function cpFileIfnotExist(srcPath:string, destPath:string){

    if(!canAccessFile(destPath)){
      console.info(`Cannot access filePath: ${destPath},copy it from ${srcPath}.`);
      fs.copyFileSync(srcPath, destPath);
    }
}

export function cpFileForce(srcPath:string, destPath:string){

  fs.copyFileSync(srcPath, destPath);
  
}

/**
 * @description update json
 * @param targetJson 
 * @param srcJson
 * @returns 
 */
export function updateJson(targetJson :any, srcJson:any )
{
  for (let key in srcJson) {
    if(key in targetJson){
      targetJson[key] = srcJson[key];
    }
  }
  return targetJson;
}

export function checkIsSdk(sdkPath:string, sdkParentDir:string)
{
    if(basename(sdkPath as string)!== sdkParentDir)
    {
        return false;
    }
    else
    {
      return true;
    }

}

export function delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}

// see https://stackoverflow.com/questions/39106516/node-fs-copy-a-folder
export async function copyDir(src:string, dest:string) {
  fs.mkdirSync(dest, { recursive: true });
  let entries =  fs.readdirSync(src, { withFileTypes: true });

  for (let entry of entries) {
      let srcPath = join(src, entry.name);
      let destPath = join(dest, entry.name);

      entry.isDirectory() ?
          await copyDir(srcPath, destPath) :
          fs.copyFileSync(srcPath, destPath);
  }
  return Promise.resolve("success");
}

/**
 * @description append self env to system env
 * @param nameInEnv the name in system env
 * @param selfEnv self env 
 * @returns the appended env
 */
export function appendPathToEnv(nameInEnv: string, selfEnv:any){

  const extEnv: { [key: string]: string } = <{ [key: string]: string }>(
    Object.assign({}, process.env)
  );

  if(selfEnv){
    for (let key in selfEnv) {
      if(key && key === 'export_paths'){
        for (let i=0; i < selfEnv['export_paths'].length; i++)
        {
          extEnv[nameInEnv] =
          selfEnv['export_paths'][i] + delimiter + extEnv[nameInEnv];
        }
      }
      else {
        extEnv[key] =  selfEnv[key];
      }
    }
  }
  return extEnv;
}

/**
 * @description      write PYTHONPATH to ._pth file and Uncomment import site
 * @param pyPath     The path need to write
 * @param pyExecPath the path that python is installed
 * @param pthFileName the file name that need be modified
 */
 function writePyPath(pyPath:string, pyExecPath:string,pthFileName:string) {

	let path = join(pyExecPath,pthFileName);
	let pathRows = fs.readFileSync(path).toString().split('\n');
	pathRows.indexOf(pyPath)===-1?pathRows.unshift(pyPath):console.log("This pyPath already exists");
	for(let i=0; i<pathRows.length;i++){
		if (pathRows[i].indexOf('#import site')!==-1){
			pathRows[i] = 'import site';
			break;
		}
	}
	fs.writeFileSync(path, pathRows.join('\n'));
}

/**
 * @description  read the sdkScopeEnv.json in workspace root path
 * @param workspacePath the sdk root path
 * @returns the env in JSON
 */
 export function getSdkEnv(workspacePath:string):JSON|undefined{

  // download with IDE case, use downloaded tool
  let sdkScopeFilePath = join(workspacePath, GNAME.sdkScopeEnvFileName);
  if(fs.existsSync(sdkScopeFilePath)){
    let cfgContent = fs.readFileSync(sdkScopeFilePath).toString();
    if(!cfgContent)
    {
      return undefined;
    }
    let cfgJson = JSON.parse(cfgContent);
    if(cfgJson)
    {
      // workspace has limit the sdkPath
      if("sdkPath" in cfgJson)
      {
        let paramName = 'qualIoe.staticCfg.sdkPath';
        const target = qualConfig.readParameter("qualIoe.commonCfg.saveScope");
        
        async ()=>{
          await qualConfig.writeParameter(
            paramName,
            cfgJson['sdkPath'],
            target as vscode.ConfigurationTarget
          );
        };
      }

      // preprocess PYTHONPATH
      if ("PYTHONPATH" in cfgJson){
        let pyPath = process.env.PYTHONPATH;
        let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath');
        for(let i =0; i< cfgJson["PYTHONPATH"].length;i++)
        {
          pyPath = join(sdkPath as string,cfgJson["PYTHONPATH"][i]) + delimiter + pyPath;
        }
        cfgJson['PYTHONPATH'] = pyPath;
      }
      return cfgJson;
    }
  }else{
    // just use PC env
    return undefined;
  }
}

 /**
  * @description check python version and then create python venv for ide
  * @param workspaceFolderPath  the venv path
  * @param requireFilePath  the require file path
  * @param logging  vscode output channel
  */
export function checkAndInstallPyVenv(workspaceFolderPath:string,requireFilePath:string,logging?: vscode.OutputChannel){
  // TODO:let user choose python version
  let pyPromise = new Promise<void>( (pyResolve,pyReject)=> {

    let promise = new Promise<string>( (resolve,reject)=> {

      cp.exec('python -c "import platform; print(platform.python_version())"',
      (err, stdout, stderr) => {
        if(stdout){
          console.log("system python version: "+stdout.toString().trim());
          logging?.appendLine("system python version: "+stdout.toString().trim());
          resolve(stdout.toString().trim());
        }
        else{
          reject(new Error("No python in system path!"));
        }
      });

    });

    promise.then((version:string)=>{

      if(compare(version,minPyVer,'>=')){

        console.log("create virtual python environment with python version: "+version);
        logging?.appendLine("create virtual python environment with python version: "+version);

        return new Promise<string>((resolve,reject)=>{

          if(!fs.existsSync(join(workspaceFolderPath,`${idePyVirtualenv}_${version}`))){

            cp.exec(`python -m venv ${idePyVirtualenv}_${version}`,{
              cwd:workspaceFolderPath
            },
            (err, stdout, stderr)=>{
              if(stderr || err){
                reject(new Error("Venv create failed."));
              }
              else{
                console.log("Venv create success at ",workspaceFolderPath);
                logging?.appendLine("Venv create success at "+workspaceFolderPath);
                resolve(`${idePyVirtualenv}_${version}`);
              }
            });

          }
          else{
            resolve(`${idePyVirtualenv}_${version}`);
          }

        });
      }else{
        return Promise.reject(new Error(`wrong python version: ${version}`));
      }

    })
    .then((venvName:string)=>{
      // install package needed by ide self
      console.log("install package needed by ide self. ");
      logging?.appendLine("install package needed by ide self. ");

      let venvPy = join(workspaceFolderPath,venvName,'Scripts','python');

      return new Promise<string>( (resolve,reject)=>{
        let cmdStr = `${venvPy} -m pip install -r requirements.txt`;

        let cmd = cmdStr.split(' ')[0];
        let cmdArray = cmdStr.split(' ');

        const spwanedProcess = cp.spawn(cmd,cmdArray.slice(1, cmdArray.length),{cwd:requireFilePath});

        console.log(`spawned pid ${spwanedProcess.pid} with command ${cmdStr}`);
        logging?.appendLine(`spawned pid ${spwanedProcess.pid} with command ${cmdStr}`);

        spwanedProcess.stdout.on('data', (data: any) => {
          logging?.appendLine("stdout:" + data);
        });

        spwanedProcess.stderr.on('data', (data: any) => {
          console.error(`spawned pid ${spwanedProcess.pid} pushed something to stderr`);
          logging?.appendLine(data);
        });

        spwanedProcess.on('exit', function(code: any) {
          if (code !== 0) {
          console.log('Failed: ' + code);
          reject(new Error('install python package failed'));
          }
          else {
          console.log(`pid ${spwanedProcess.pid} finished`);
          resolve(`${venvPy}`);
          }
      
        });

      });
    })
    .then(async (venvPy:string)=>{
      // python venv create successfully
      console.log(`python venv create successfully`);
      logging?.appendLine(`python venv create successfully`);

      let paramName = 'qualIoe.commonCfg.pythonBinPath';
      await qualConfig.writeParameter(
        paramName,
        venvPy,
        vscode.ConfigurationTarget.Global
      );

      pyResolve();
    })
    .catch((err)=>{
      console.error(err.message);
      logging?.appendLine(err.message);
      vscode.window.showWarningMessage("There is no python in system path, or wrong python version.");
      pyReject();
    });

  });

  return pyPromise;

}

export function checkGit(logging?: vscode.OutputChannel){

  try{
    let output = cp.execSync('git --version');
    if(output){
      console.log("git version is " + output.toString());
    }
  }
  catch(err){
    vscode.window.showErrorMessage("QCC IDE need git,pls intall");
  }

}

export function checkPy(logging?: vscode.OutputChannel){
  try{
    let output = cp.execSync('python -c "import platform; print(platform.python_version())"');
    if(output){
      console.log("python version is " + output.toString());
      if(compare(output.toString().trim(),minPyVer,'<')){
        vscode.window.showErrorMessage("Python version is < " + minPyVer);
      }
    }
  }
  catch(err){
    // console.log(err);
    vscode.window.showErrorMessage("QCC IDE need python3, pls intall it");
  }

}

/**
 * @description  read the sdkScopeEnv.json in sdk root path
 * @param workspacePath the sdk root path
 * @returns the env in JSON
 */
export function workDnldByIde(workspacePath:string){

  // download with IDE case
  let sdkScopeFilePath = join(workspacePath, GNAME.sdkScopeEnvFileName);
  if(fs.existsSync(sdkScopeFilePath)){
        return true;
  }
  else{
    return false;
  }

}

export function checkOpenocd(logging?: vscode.OutputChannel){
  try{
    let output = cp.execSync('openocd -v');
    if(output){
      console.log("openocd version is " + output.toString());
    }
  }
  catch(err){
    // console.log(err);
    // TODO:openocd version changes with different sdk
    vscode.window.showErrorMessage("QCC IDE need openocd, pls intall it");
  }

}

export function checkAndInstallSDKDep(activePrjCfg:any,boardName:string,logging?: vscode.OutputChannel){
  
  dbWrap.installSDKAndDep(activePrjCfg.sdkName,activePrjCfg.sdkVer,boardName);
}

export function checkIdeDep(logging?: vscode.OutputChannel){
  checkPy();
  checkGit();
}


/**
 *  @description call python to download and install sdk & tools
 *                may need user name and user password
 * 
 */
export function usePythonToInstall(venvPy:string,fileName:string, userName?:string, userPass?: string,logging?: vscode.OutputChannel){

  let pyPromise = new Promise<void>( (resolve,reject)=> {
      let cmdStr = '';

      if(userName && userPass)
      {
        cmdStr = `${venvPy} -u ${extensionPYDownloadPath} -f "${fileName}" -U ${userName} -P ${userPass}`;
      }else{
        cmdStr = `${venvPy} -u ${extensionPYDownloadPath} -f "${fileName}"`;
      }

      let  installTask = new CommandTask();
      installTask.addCommandTask(join(extensionPYDownloadPath,'../') as string, cmdStr,'Install Dep')
      .then(()=>{
        if(TaskManager.isRunning)
        {
          reject(new Error('Task is running'));
        }

        TaskManager.runTasks()
        .then(()=>{
          resolve();
        })
        .catch(()=>{
          vscode.window.showErrorMessage("Download failed.");
          reject(new Error('use python to install SDK failed'));
        });

      });
  });

  return pyPromise;
}

/**
 *  @description check And Create File if not exist
 * 
 */
export function createFile(folderPath:string, fileName: string){
  if(fs.existsSync(join(folderPath,fileName)))
  {
    return;
  }
  else{
    fs.writeFileSync(join(folderPath,fileName),'{}');
  }

}


/**
 *  @description get the exe path in host
 * 
 */
export function getExePath(exeName:string){

  if(exeName==='undefined')
  {
    vscode.window.showWarningMessage('exeName is undefinded.');
    return Promise.reject();
  }

  return new Promise<string>( (resolve,reject)=> {

    let cmdExe;
    if(os.platform() === 'win32'){
      cmdExe = 'where.exe';
    }else{
      cmdExe = 'whereis';
    }

    cp.exec(`${cmdExe} ${exeName}`,
    (err, stdout, stderr) => {

      if(stdout){
        console.log(`${exeName} is in: `+ stdout.toString().trim());
        resolve(stdout.toString().trim());
      }
      else{
        vscode.window.showWarningMessage(`No ${exeName} in this host system env`);
        reject(new Error(`No ${exeName} in this host system env`));
      }
    });
  });

}

/**
 * @function getStaticCfgFileLocation
 * @description return the path of qccIdeProjConfig.json
 * @param path [in] the project path
 * @returns 
 */
export function getStaticCfgFileLocation(path:string)
{
  if(fs.existsSync(join(path,'tools','qccide','qccIdeProjConfig.json')))
  {
    return join(path,'tools','qccide','qccIdeProjConfig.json');
  }
  else{
    return join(path,'qccIdeProjConfig.json');
  }
}


/**
 * @function getPrj
 * @description return the project name in workspace configure file
 * @param jsonCfg [in] the configure file content
 * @returns 
 */
export function getPrj(jsonCfg:any) {
  let prjName:string[] = [];
  for( let i=0; i< jsonCfg.length; i++)
  {
    prjName.push(jsonCfg[i].prjName);
  }

  return prjName;
}

/**
 * @function getPrjIncPath
 * @description return the project name in workspace configure file
 * @param jsonCfg [in] the configure file content
 * @returns 
 */
export function getPrjIncPath(prjName:string, jsonCfg:any) {

  for( let i=0; i< jsonCfg.length; i++)
  {
    if(jsonCfg[i].prjName === prjName)
    {
      return  jsonCfg[i].incPaths;
    }
  }
}

/**
 * @function getActivePrjIndex
 * @description return the project name in workspace configure file
 * @param jsonCfg [in] the configure file content
 * @returns 
 */
export function getActivePrjIndex(jsonCfg:any) :number{

  for( let i=0; i< jsonCfg.length; i++)
  {
    if(jsonCfg[i].activated === true)
    {
      return  i;
    }
  }
  return 0;
}

/**
 * @function updateActivePrjStatus
 * @description return the project name in workspace configure file
 * @param jsonCfg [in] the configure file content
 * @param prjName [in] the new active prj name
 * @returns 
 */
export function updateActivePrjStatus(jsonCfg:any,prjName:string) :number{
  let index:number = -1;
  for( let i=0; i< jsonCfg.length; i++)
  {
    if(jsonCfg[i].prjName === prjName)
    {
      index = i;
      jsonCfg[i].activated = true;
    }else{
      jsonCfg[i].activated = false;
    }
  }
  return index;
}

/**
 * @function getBoardList
 * @description return the board list
 * @returns 
 */
export function getBoardList(activePrjCfg:any) {

  return activePrjCfg.boardList;
}

/**
 * @function getChipIdList
 * @description return the chip id list
 * @returns 
 */
export function getChipIdList(activePrjCfg:any) {
  if(activePrjCfg.chipIdList)
  {
    return activePrjCfg.chipIdList;
  }
  else{
    return undefined;
  }
}

/**
 * @function parseJsonWithVar
 * @description parse json object and replace placeholders ${} with according value if has
 * @returns 
 */
export function parseJsonWithVar(jsonObject:any){
  // Replace placeholders in the JSON 
  function replaceInValue(value:any):any {
    if (typeof value === 'string') {
        return value.replace(/\$\{([^}]+)\}/g, (match, key) => {
            return jsonObject[key] || match; // Replace with the value or keep the original if not found
        });
    } else if (Array.isArray(value)) {
        return value.map(replaceInValue);
    } else if (typeof value === 'object') {
        const replacedObject:any = {};
        for (const key in value) {
            replacedObject[key] = replaceInValue(value[key]);
        }
        return replacedObject;
    }
    return value;
  }

  return replaceInValue(jsonObject);
}

export function parseJsonWithVarAndPrjName(jsonCfg:any,prjName:string){

  for( let i=0; i< jsonCfg.length; i++)
  {
    if(jsonCfg[i].prjName === prjName)
    {
      jsonCfg[i] = parseJsonWithVar(jsonCfg[i]);
    }
  }
 
}

export function updateAllBoardCfg(jsonCfg:any,newBoardName:string){
  for( let i=0; i< jsonCfg.length; i++)
  {
    jsonCfg[i].demoChipBoard = newBoardName;  
  }
}
