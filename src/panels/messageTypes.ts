/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

export type MessageType = 'NONE'|'COMMON' |'TYPE'| 'NAMELIST' |'NAMECHANGE'  | 'SDKLIST' | 'VERCHANGE' | 'VERCHANGERSP'|'OPENSDK'|'DEMOLISTRSP'|'READDEMOINFO'|'DEMOINFORSP'|'CREATEDEMO'|'CREATEDEMORSP';

export interface Message {
  type: MessageType;
  payload?: any;
}

// used to determin which page show, like home page, cfg page ...
export interface CommonMessage extends Message {
  type: 'COMMON';
  payload: string;
}

// used to request type identify, react -> extension
export interface TypeMessage extends Message {
  type: 'TYPE';
  payload: string;
}

// used to response name list of chip or board, extension => react
export interface NameListMessage extends Message {
  type: 'NAMELIST';
  payload: string;
}

// used to request sdk list, react -> extension
export interface NameChangeMessage extends Message {
  type: 'NAMECHANGE';
  payload: string;
}

// used to response sdk list of specify name, extension => react
export interface SdkListMessage extends Message {
    type: 'SDKLIST';
    payload: string;
}

// used to request that sdk version has changed, react => extension
export interface VerChanegMessage extends Message {
    type: 'VERCHANGE';
    payload: string;
}

// used to respond the message of sdk version has changed, extension => react
export interface VerChangeRspMessage extends Message {
    type: 'VERCHANGERSP';
    payload: string;
}

// used to request open sdk, react => extension
export interface OpenSdkMessage extends Message {
  type: 'OPENSDK';
  payload: string;
}

// used to respond sdk opend status, extension => react
export interface DemoListRspMessage extends Message {
  type: 'DEMOLISTRSP';
  payload: string;
}


// used to request demo info, react => extension
export interface ReadDemoInfoMessage extends Message {
  type: 'READDEMOINFO';
  payload: string;
}

// used to respond demo info, extension => react
export interface DemoInfoRspMessage extends Message {
  type: 'DEMOINFORSP';
  payload: string;
}


// used to request demo info, react => extension
export interface CreateDemoMessage extends Message {
  type: 'CREATEDEMO';
  payload: string;
}

// used to respond demo info, extension => react
export interface CreateDemoRspMessage extends Message {
  type: 'CREATEDEMORSP';
  payload: string;
}

export type SdkItem = {
  name: string;
  intr: string;
  ver: string;
  verIntr: string;
};

export type DemoInfo = {
  name: string;
  info: string;
};
