/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import * as vscode from 'vscode';
import { Uri,CancellationTokenSource  } from "vscode";
import { getUri } from "../utils/getUri";
import { CommonMessage,Message,TypeMessage,NameListMessage,SdkListMessage, SdkItem,OpenSdkMessage,DemoListRspMessage,DemoInfoRspMessage, DemoInfo,CreateDemoMessage } from './messageTypes';
import * as sqlite3 from 'sqlite3';
import { basename, join } from 'path';
import * as fs from 'fs';
import * as cp from "child_process";
import {ideUserPath,extensionPath, ideUserCfgFolderPath,ideUserDownloadCfgFolderPath,ideUserDownloadCfgURIFolderPath,ideUserPyVenvFolderPath,cCppPropertiesName} from '../extension';
import {boardDepFileRelativePath,installSDKAndDep} from '../utils/dbWrap';
import * as qualConfig from '../config/globalConfigure';
import{GNAME} from '../config/globalName';
import * as utils from '../utils/utils'; 

let dbRelativePath='database/ide.db';
let infoFileName = 'Readme.md';


enum DbSrc{
  dBFile = 0,
  dBSqlite3,
  dBRemote
}

export class IdeHomePanel {
  public static currentPanel: IdeHomePanel | undefined;
  private readonly _panel: vscode.WebviewPanel;
  private _disposables: vscode.Disposable[] = [];
  private _ideDb;
  private _choosenType:string='Board';
  private _choosenChipName:string='';
  private _choosenBoardName:string='';
  private _choosenSdk:string|undefined= undefined;
  private _choosenSdkDemoSuffix:string|undefined= undefined;
  private _choosenSdkRootPath:string = '';
  private _choosenSdkDemoPath:string|undefined= undefined;
  private _sdkCfgCache:any ={};
  private _dbSrc = DbSrc.dBFile;

  private constructor(panel: vscode.WebviewPanel,extensionUri: vscode.Uri) {
    this._panel = panel;
    this._panel.onDidDispose(this.dispose, null, this._disposables);
    this._panel.webview.html = this._getWebviewContent(this._panel.webview, extensionUri);

    let dbPath = join(extensionUri.fsPath,dbRelativePath);
    this._ideDb = new sqlite3.Database(dbPath,sqlite3.OPEN_READONLY);

    // listen messages from webview
    this._panel.webview.onDidReceiveMessage(
      (message: Message) => {

        if (message.type === 'TYPE') {
          const type = (message as TypeMessage).payload.trim();

          console.log(`Received type from Webview: ${type}`);

          this._choosenType = type;

          if(this._dbSrc === DbSrc.dBFile)
          {
            // Note: dBFile only support Board type
            if(this._choosenType === 'Board')
            {
              let dbBoardsPath = join(extensionPath,boardDepFileRelativePath);

              let subBoards = fs.readdirSync(dbBoardsPath).filter(function (file) {
                return fs.statSync(dbBoardsPath+'/'+file).isDirectory();
              });
              if (subBoards.length >0){
                this.postMessageToWebview<NameListMessage>(
                  {
                    type: 'NAMELIST',
                    payload: subBoards.toString(),
                  }
                );
              }
            }
          }

          if(this._dbSrc === DbSrc.dBSqlite3)
          {
            // fetch name list and send to react app
            this._ideDb.all(`SELECT DISTINCT ${type} FROM BOARDLIST`,(err, rows)=>{

              if(rows.length > 0){

                let strArray:string[] = [];
                if (type === 'Chip'){
                  rows.forEach((row:any)=>{
                    strArray.push(row.Chip);
                  });
                }
                if (type === 'Board'){
                  rows.forEach((row:any)=>{
                    strArray.push(row.Board);
                  });
                }

                console.log("Support type name list: ",strArray.toString());

                if (strArray.length >0){
                  this.postMessageToWebview<NameListMessage>(
                    {
                      type: 'NAMELIST',
                      payload: strArray.toString(),
                    }
                  );
                }
              }
            });
          }
        }

        if(message.type === 'NAMECHANGE'){
          const name = (message as TypeMessage).payload.trim();

          console.log('received name from react: ',name);

          let sdkContentList:SdkItem[] =[];
          let sdkListName :string[]=[];
          let sdkListIntr :string[]=[];
          let sdkNameCache:any ={};

          if(this._dbSrc === DbSrc.dBFile)
          {
            // Note: dBFile only support Board type
            if(this._choosenType === 'Board')
            {
              let boardName = name;
              let subBoardPath = join(extensionPath,boardDepFileRelativePath,boardName);
              let subSdks = fs.readdirSync(subBoardPath).filter(function (file) {
                return fs.statSync(subBoardPath+'/'+file).isFile();
              });

              this._choosenBoardName = boardName;
              console.log("subSdks: " + subSdks);

              for(let j = 0 ; j<subSdks.length ;j++)
              {
                // subSdks should like 1.1.0^qccsdk_qcc730.json
                const regex: RegExp = /^(.*?)\.json/;
                const matchArray: RegExpMatchArray | null = subSdks[j].match(regex);
                let sdkNameVer : string|undefined = matchArray ? matchArray[1]:undefined;
                if(sdkNameVer)
                {
                  // need read intr from the file
                  let cfgContent = fs.readFileSync(join(subBoardPath, subSdks[j])).toString();
                  if(!cfgContent)
                  {
                    continue;
                  }
                  let cfgJson = JSON.parse(cfgContent);

                  if(cfgJson['Sdk_Uri'] === 'null' || !cfgJson['Sdk_Uri'])
                  {
                    continue;
                  }

                  let sdkNameVerArray :string[] = sdkNameVer.split('^');

                  sdkContentList.push({
                    name:sdkNameVerArray[1],
                    intr:cfgJson['intr']?cfgJson['intr']:'null',
                    ver:sdkNameVerArray[0],
                    verIntr:cfgJson['ver_intr']?cfgJson['ver_intr']:'',
                  });

                }
              }

              // send to react app to display
              if(sdkContentList.length >0)
              {
                this.postMessageToWebview<SdkListMessage>(
                  {
                    type: 'SDKLIST',
                    payload: JSON.stringify(sdkContentList),
                  }
                );
              }else{
                sdkContentList.push({
                  name:"No SDK for now",
                  intr:"None",
                  ver:"",
                  verIntr:"",
                });
                this.postMessageToWebview<SdkListMessage>(
                {
                  type: 'SDKLIST',
                  payload: JSON.stringify(sdkContentList),
                });
              }
            }
          }

          if(this._dbSrc === DbSrc.dBSqlite3)
          {
            // fetch sdk list and send to react app
            let sqlQuery = `SELECT * FROM BOARDLIST WHERE ${this._choosenType}="${name}" `;
            console.log(sqlQuery);

            this._ideDb.all(sqlQuery,(err, rows:any)=>{
            
              if (rows.length>0){
                this._choosenChipName = rows[0].Chip;

                rows.forEach((row:any)=>{
                    sdkListName.push(row.Sdk_Name);
                    sdkListIntr.push(row.Sdk_Intr);
                    sdkNameCache[row.Sdk_Name] =  row.Sdk_Intr;
                    this._sdkCfgCache[row.Sdk_Name] = JSON.parse(row.Sdk_Cfg);
                });

                let sqlQuery = `SELECT Sdk_Name, Sdk_Ver, Key_Value FROM SDKLIST WHERE Sdk_Name in ("${sdkListName.join("\",\"")}") `;

                console.log(sqlQuery);

                this._ideDb.all(sqlQuery,(err,rows)=>{
                  if(rows.length>0){
                    
                    
                    console.log(rows);

                    rows.forEach((row:any)=>{

                      let keyValue = JSON.parse(row.Key_Value);
                      sdkContentList.push({
                        name:row.Sdk_Name,
                        intr:sdkNameCache[row.Sdk_Name]?sdkNameCache[row.Sdk_Name]:null,
                        ver:row.Sdk_Ver,
                        verIntr:keyValue?keyValue["ver_intr"]:"",
                      });

                    });
                  
                    
                    // send to react app to display
                    this.postMessageToWebview<SdkListMessage>(
                      {
                        type: 'SDKLIST',
                        payload: JSON.stringify(sdkContentList),
                      }
                    );

                  }else{
                    // send to react app to display
                    sdkContentList.push({
                      name:"Not supported",
                      intr:"None",
                      ver:"",
                      verIntr:"",
                    });
                    this.postMessageToWebview<SdkListMessage>(
                    {
                      type: 'SDKLIST',
                      payload: JSON.stringify(sdkContentList),
                    }
                  );
                  }
                });

              }

            });
          }

        }

        if(message.type === 'OPENSDK'){

          let msg:SdkItem = JSON.parse((message as OpenSdkMessage).payload);
          if(!msg.ver)
          {
            return;
          }

          let openSDKmode = qualConfig.readParameter('qualIoe.commonCfg.openSdkMode');
          this._choosenSdk = `${msg.ver}^${msg.name}`;

          // should check user space's cfg
          let sdkStatusFileName = this._choosenSdk+'.json';
          utils.createFile(ideUserDownloadCfgFolderPath,sdkStatusFileName);
          let sdkStatusContent = fs.readFileSync(join(ideUserDownloadCfgFolderPath,sdkStatusFileName)).toString();
          let sdkStatusJson = JSON.parse(sdkStatusContent);

          if(this._dbSrc === DbSrc.dBFile)
          {
            if(sdkStatusJson.Sdk_Status===undefined ||  sdkStatusJson.Sdk_Status !== "installed"){
              installSDKAndDep(msg.name,msg.ver, this._choosenBoardName);
            }
            else if(sdkStatusJson.Sdk_Status !==undefined &&  sdkStatusJson.Sdk_Status === "installed"){
           
              console.log(`has installed, just copy and open ${msg.name} ${msg.ver} ...` );
              // 1. read the qccIdeProjConfig.json

              let ideSdkUserDownloadPath = join(ideUserPath,'Sdks');
              let sdkDowloadPath = join(ideSdkUserDownloadPath,`${msg.ver}^${msg.name}`,GNAME.sdkDnldEnvFileName);
              
              let cfgContent = fs.readFileSync(sdkDowloadPath).toString();
              let cfgJson = JSON.parse(cfgContent);
              let sdkDnldPath:string|undefined;
              if(cfgJson && "sdk_root_path_from_download_path" in cfgJson){
                sdkDnldPath = join(ideSdkUserDownloadPath, msg.ver + '^'+ msg.name,cfgJson['sdk_root_path_from_download_path']);
              }
              
              if(!sdkDnldPath)
              {
                vscode.window.showWarningMessage(`${msg.name} ${msg.ver} not installed.`);
                return;
              }
              
              let destSdkPath :string;
              let srcSdkDnldPath = sdkDnldPath;

              const options: vscode.OpenDialogOptions = {
                canSelectMany: false,
                openLabel: 'Select placed folder',
                canSelectFiles: false,
                canSelectFolders: true
              };
              vscode.window.showOpenDialog(options).then(async (fileUri) => {
                if (fileUri && fileUri[0]) {
                  destSdkPath = fileUri[0].fsPath;
                  destSdkPath = join(destSdkPath, basename(srcSdkDnldPath));
                  // copy downloaded sdk for secure backup
                  vscode.window.withProgress({ 
                    cancellable: false,
                    location: vscode.ProgressLocation.Notification,
                    title: "Creating new Project..."},
                    (progress, token) => { 
      
                      const p = new Promise<void>((resolve,reject)=>{ 
                        setTimeout(async () => {
                          // Note: since utils.copyDir is sync function in some degree, do not run it Immediately
                          if(fs.existsSync(destSdkPath as string))
                          {
                            vscode.window.showWarningMessage(`The dircectory ${destSdkPath} exist.`);
                            reject();
                          }
                          await utils.copyDir(srcSdkDnldPath,destSdkPath as string);
                          resolve();
                        }, 100);
                      });
      
                      return  p;
                  }).then(
                    ()=>{
                      let sdkPath = destSdkPath;
                      if(openSDKmode === 'SDK' && sdkPath)
                      {
                        let destSDKUri = Uri.file(sdkPath);
        
                        vscode.commands.executeCommand("vscode.openFolder", destSDKUri,true);
                      }
    
                      if(openSDKmode === 'DEMOLIST' && sdkPath)
                      {
                        let cfgFilePath = utils.getStaticCfgFileLocation(sdkPath);
                        if(!fs.existsSync(cfgFilePath))
                        {
                          vscode.window.showWarningMessage(`${msg.name} ${msg.ver} not has QCCIDE Project file.`);
                          return;
                        }
          
                        let cfgFileContent = fs.readFileSync(cfgFilePath).toString();
                        if(!cfgFileContent)
                        {
                          return;
                        }
                        let cfgFileJson = JSON.parse(cfgFileContent);
          
                        let activePrjIndex = utils.getActivePrjIndex(cfgFileJson);
                        let activeFileJson = cfgFileJson[activePrjIndex];
                        // Should just use the active prj cfg
                        if(activeFileJson && 'demoPath' in activeFileJson)
                        {
                          let demoPath = activeFileJson['demoPath'];
                          demoPath = join(sdkPath,demoPath);
                          this._choosenSdkDemoPath = demoPath;
                          this._choosenSdkRootPath = sdkPath;
            
                          if(fs.existsSync(demoPath)){
            
                            console.log("Find demopath at " + demoPath);
            
                            let demoList = fs.readdirSync(demoPath).filter(function (file) {
                              return fs.statSync(demoPath+'/'+file).isDirectory();
                            });
                            
                            console.log("demo list " + demoList.toString());
                            // send to react app to display
                            this.postMessageToWebview<DemoListRspMessage>(
                              {
                                type: 'DEMOLISTRSP',
                                payload: demoList.toString(),
                              }
                            );
            
                          }
                          else{
                            // TODO:  reset the install status to 0 in user space
                            console.error(demoPath + " is not exist when try to open. ");
                            vscode.window.showWarningMessage(demoPath + " is not exist when try to open.");
                          }
                        }
                      }
    
                    }
                  );

                }
              });
            }
          }

          if(this._dbSrc === DbSrc.dBSqlite3)
          { 
            // 1. check install status from user space's cfg
            let sqlQuery = `SELECT * FROM SDKLIST WHERE Sdk_Name="${msg.name}" AND Sdk_Ver="${msg.ver}"`;
            console.log(sqlQuery);

            let sdkCfgJson =  this._sdkCfgCache[msg.name];

            this._ideDb.all(sqlQuery,(err,rows:any)=>{

              console.log(rows);

              if(rows.length === 1){

                
                
                if(sdkStatusJson.Sdk_Status===undefined ||  sdkStatusJson.Sdk_Status !== "installed"){

                  // 2. if not installed, call python script to install it
                  console.log(`Start install ${msg.name} ${msg.ver} ...` );
                  vscode.window.showInformationMessage(`Start install ${msg.name} ${msg.ver} ...` );

                  // 2.1 generate download json file to user space's cfg for python use
                  // 2.1.1 generate sdk download uri json
                  let downloadInfoJson:any = {} ;
                  downloadInfoJson['Sdk_Name'] = rows[0].Sdk_Name;
                  downloadInfoJson['Sdk_Ver'] = rows[0].Sdk_Ver;
                  downloadInfoJson['Sdk_Uri'] = rows[0].Sdk_Uri;
                  downloadInfoJson['Sdk_Cfg'] = sdkCfgJson;
                  let sdkKeyValueJson  =  JSON.parse(rows[0].Key_Value);
                  for (let key in sdkKeyValueJson) {
                    downloadInfoJson[key] = sdkKeyValueJson[key];
                  }
                  
                  // 2.1.2 generate tools download uri json
                  let toosNeedDownloadJson = JSON.parse(rows[0].Tools);
                  let toolsNeedNameList = Object.keys(toosNeedDownloadJson);

                  let toolSqlQuery = `SELECT * FROM TOOLLIST WHERE Tool_Name in ("${toolsNeedNameList.join("\",\"")}")  `;
                  console.log(toolSqlQuery); 
                  
                  this._ideDb.all(toolSqlQuery,(err,rows)=>{

                    console.log(rows);

                    let toolsList :any= [];

                    rows.forEach((row:any)=>{
                      if(row.Tool_Ver === toosNeedDownloadJson[`${row.Tool_Name}`]){
                        let toolInfoJson:any = {};
                        toolInfoJson['name'] = row.Tool_Name;
                        toolInfoJson['ver'] = row.Tool_Ver;
                        toolInfoJson['uri'] = JSON.parse(row.Tool_Uri);
                        let toolKeyValueJson  =  JSON.parse(row.Key_Value);
                        for (let key in toolKeyValueJson) {
                          toolInfoJson[key] = toolKeyValueJson[key];
                        }

                        toolsList.push(toolInfoJson);
                      }
                    });
                    
                    downloadInfoJson['Tools'] = toolsList;

                    // 2.1.3 write to json file
                    fs.writeFileSync(join(ideUserDownloadCfgURIFolderPath,sdkStatusFileName),JSON.stringify(downloadInfoJson,null,2));

                    let requirementPath = join(extensionPath,'downloadInstall','python');
                    let pythonOutput = vscode.window.createOutputChannel('install python');
                    let pyVenvPath =  ideUserPyVenvFolderPath;
    
                    // 2.2 check python exist and its version, and install module
                    vscode.window.withProgress({
                      cancellable: false,
                      location: vscode.ProgressLocation.Notification,
                      title: "Install python venv..."},
                      (progress,token)=>{
                        pythonOutput.clear();
                        pythonOutput.show();
                        return utils.checkAndInstallPyVenv(pyVenvPath,requirementPath,pythonOutput);
    
                    })
                    .then(async() => {
                      // 2.3 download sdk with progress
                      vscode.window.showInformationMessage("Please enter user name and password in above box.");
                        // 2.3.1 enter user name and user pass
                        // input user:password for git clone sdk
                      let userName  = await vscode.window.showInputBox({placeHolder:'User name provided by Qualcomm',ignoreFocusOut:false});
                      let password = await vscode.window.showInputBox({placeHolder:'User password provided by Qualcomm', ignoreFocusOut:false,password:true});

                      let pythonPath = qualConfig.readParameter('qualIoe.commonCfg.pythonBinPath');

                      vscode.window.withProgress({
                        cancellable: false,
                        location: vscode.ProgressLocation.Notification,
                        title: "Download SDK..."},
                        (progress,token)=>{
                          pythonOutput.clear();
                          pythonOutput.show();

                          return utils.usePythonToInstall(pythonPath as string,join(ideUserDownloadCfgURIFolderPath,sdkStatusFileName),userName,password,pythonOutput);
                      }).then(()=>{
                        // 2.4 if success install, info user to open sdk again
                        vscode.window.showInformationMessage(`${msg.name} ${msg.ver} install success, you can now open the SDK` );
                      });

                    });

                  });
                }

                if(sdkStatusJson.Sdk_Status !==undefined &&  sdkStatusJson.Sdk_Status === "installed"){
                  // 3. if installed, get the example demo and send to react to display
                  console.log(`has installed, just open ${msg.name} ${msg.ver} ...` );
                  
                  let ideSdkUserDownloadPath = join(ideUserPath,'Sdks');
                  let demoRelativePath = JSON.parse(rows[0].Key_Value)['demoPath'];
                  let sdkRelativePathFromDownload = JSON.parse(rows[0].Key_Value)['sdk_root_path_from_download_path'];
                  let regex = new RegExp(JSON.parse(rows[0].Key_Value)['demoRegex']);
                  this._choosenSdkDemoSuffix = JSON.parse(rows[0].Key_Value)['demoRegex'];

                  let sdkDowloadPath = join(ideSdkUserDownloadPath,`${msg.ver}_${msg.name}`);
                  let sdkPathFromDownload = join(sdkDowloadPath,sdkRelativePathFromDownload);
                  let demoPath = join(sdkPathFromDownload,demoRelativePath);
                  this._choosenSdkDemoPath = demoPath;
                  this._choosenSdkRootPath = sdkPathFromDownload;

                  if(fs.existsSync(demoPath)){

                    console.log("Find demopath at " + demoPath);

                    let contentList = fs.readdirSync(demoPath);
                    let demoList = contentList.filter( function( elm ) {return elm.match(regex);});
                    
                    console.log("demo list " + demoList.toString());
                    // send to react app to display
                    this.postMessageToWebview<DemoListRspMessage>(
                      {
                        type: 'DEMOLISTRSP',
                        payload: demoList.toString(),
                      }
                    );

                  }
                  else{
                    // TODO:  reset the install status to 0 in user space
                    console.error(demoPath + " is not exist when try to open. reset the install status in user space");
                    vscode.window.showWarningMessage(demoPath + " is not exist when try to open. reset the install status in user space");
                  }

                }
              }
              else{
                vscode.window.showWarningMessage("Has two record for "+msg.name+" "+msg.ver+" in database, Pls help report to developer");
              }
            });
          }

        }


        if(message.type === 'READDEMOINFO'){
          const demoName = (message as TypeMessage).payload.trim();

          if(this._choosenSdkDemoPath){

            let path = join(this._choosenSdkDemoPath,demoName,infoFileName);

            if(fs.existsSync(path)){

                let content  = fs.readFileSync(path,"utf8");

                let demoInfo:DemoInfo = {
                  name:demoName,
                  info:content
                };

                // send to react app to display
                this.postMessageToWebview<DemoInfoRspMessage>(
                  {
                    type: 'DEMOINFORSP',
                    payload: JSON.stringify(demoInfo),
                  }
                );

            }else{

              console.log("There is no information file for this demo.");
              let demoInfo:DemoInfo = {
                name:demoName,
                info:"There is no information file for this demo."
              };
              this.postMessageToWebview<DemoInfoRspMessage>(
                {
                  type: 'DEMOINFORSP',
                  payload:JSON.stringify(demoInfo),
                }
              );
            }

          }else{
            console.error("_choosenSdkDemoPath is undefined.");
          }
        }

        if(message.type === 'CREATEDEMO'){
          
          const demoName = (message as CreateDemoMessage).payload.trim();

          if(demoName){
            console.log("start create project with ", demoName);

            // show inputbox to let user imput new demo name
						vscode.window.showInputBox({placeHolder:'New projet name',ignoreFocusOut:true}).then((newName)=>{
              if(this._choosenSdkDemoPath && newName ){

                let destDemoPath = join(this._choosenSdkDemoPath,newName);
                let srcDemoPath = join(this._choosenSdkDemoPath,demoName);

                if(!fs.existsSync(destDemoPath)){

                    // copy example demo as user new project, and then open it 
                    vscode.window.withProgress({ 
                      cancellable: false,
                      location: vscode.ProgressLocation.Notification,
                      title: "Creating new Project..."},
                      (progress, token) => { 

                        const p = new Promise<void>((resolve,reject)=>{ 
                          setTimeout(async () => {
                            // Note: since utils.copyDir is sync function in some degree, do not run it Immediately
                            if(fs.existsSync(destDemoPath))
                            {
                              vscode.window.showWarningMessage(`The dircectory ${destDemoPath} exist.`);
                              reject();
                            }
                            await utils.copyDir(srcDemoPath,destDemoPath);
                            resolve();
                          }, 100);
                        });

                        return  p;
                      })
                      .then( ()=>{

                        let qccIdeProjConfigFileSrcPath = utils.getStaticCfgFileLocation(this._choosenSdkRootPath);
                        let qccIdeProjConfigFileDstPath = join(destDemoPath,"qccIdeProjConfig.json");

                        let qccIdeSdkScopeEnvFileSrcPath = join(this._choosenSdkRootPath,GNAME.sdkScopeEnvFileName);
                        let qccIdeSdkScopeEnvFileDstPath = join(destDemoPath,GNAME.sdkScopeEnvFileName);
                        
                        let vsSetFolder = join(destDemoPath,'.vscode');
                        if(!fs.existsSync(vsSetFolder)){
                          fs.mkdirSync(vsSetFolder,{recursive:true});
                        }

                        try{
                          fs.copyFileSync(qccIdeProjConfigFileSrcPath,qccIdeProjConfigFileDstPath);
                          //TODO: for download demo opened, just need one project of qccIdeProjConfig.json in sdk workspace
                          let cfgContent = fs.readFileSync(qccIdeProjConfigFileDstPath).toString();
                          if(!cfgContent)
                          {
                            vscode.window.showWarningMessage("workspace project file created failed.");
                            return;
                          }
                          let cfgJson = JSON.parse(cfgContent);
                          for(let i=0 ;i<cfgJson.length;i++)
                          {
                            if(cfgJson[i].defaultDemoPathName === demoName)
                            {
                              cfgJson[i].activated = true;
                              let newCfgJson = [];
                              newCfgJson.push(cfgJson[i]);
                              let cfgStr = JSON.stringify(newCfgJson,null,2);
                              fs.writeFileSync(qccIdeProjConfigFileDstPath, cfgStr);
                              break;
                            }
                          }

                         
                          fs.copyFileSync(qccIdeSdkScopeEnvFileSrcPath,qccIdeSdkScopeEnvFileDstPath);

                          // for download demo opened, need append "sdkPath" in  sdkScopeEnv.json
                          let content = fs.readFileSync(qccIdeSdkScopeEnvFileDstPath).toString();
                          if(!content)
                          {
                            vscode.window.showWarningMessage("Append sdkPath to created project failed.");
                            return;
                          }
                          let sdkScopeEnvJson = JSON.parse(content);
                          sdkScopeEnvJson['sdkPath'] = this._choosenSdkRootPath;
                          let cfgStr = JSON.stringify(sdkScopeEnvJson,null,2);
                          fs.writeFileSync(qccIdeSdkScopeEnvFileDstPath, cfgStr);

                          let destDemoUri = Uri.file(destDemoPath);

                          vscode.commands.executeCommand("vscode.openFolder", destDemoUri,true);

                        }catch (error){
                          console.error(error);
                        }

                    });
                }
                else{
	                  vscode.window.showErrorMessage('The new project name exists: ', destDemoPath);
                }
              }

            });
          

          }else{
            vscode.window.showWarningMessage('please choose one example demo.');
          }
        }

      },
      null,
      this._disposables
    );
  }

  postMessageToWebview<T extends Message = Message>(message: T) {
    // post message from extension to webview
    this._panel.webview.postMessage(message);
  }

  public static render(extensionUri: vscode.Uri) {
    if (IdeHomePanel.currentPanel) {
      IdeHomePanel.currentPanel._panel.reveal(vscode.ViewColumn.One);
    } else {
      const panel = vscode.window.createWebviewPanel("qcc_ide", "QCC Home", vscode.ViewColumn.One, {
        enableScripts: true,
        retainContextWhenHidden: true,
      });

      IdeHomePanel.currentPanel = new IdeHomePanel(panel,extensionUri);
    }
  }

  
  private _getWebviewContent(webview: vscode.Webview, extensionUri: vscode.Uri) {

    const styleUri = getUri(webview, extensionUri, ["webView_media", "homeView","style.css"]);
    const mainUri = getUri(webview, extensionUri, ["webView_media","homeView", "main.js"]);
    const codiconsUri =getUri(webview,extensionUri, ['node_modules', '@vscode/codicons', 'dist', 'codicon.css']);

    const toolkitUri = getUri(webview, extensionUri, [
      "node_modules",
      "@vscode",
      "webview-ui-toolkit",
      "dist",
      "toolkit.js", // A toolkit.min.js file is also available
    ]);
    
    const bundleReactScriptPath = getUri(
      webview, extensionUri, ["out","panels","react_app", "bundle.js"]);

    // Tip: Install the es6-string-html VS Code extension to enable code highlighting below
    return /*html*/ `
      <!DOCTYPE html>
      <html lang="en">
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <script type="module" src="${toolkitUri}"></script>
          <script type = "text/javascript" src="${mainUri}"></script>
          <link rel="stylesheet" href="${styleUri}">
          <link href="${codiconsUri}" rel="stylesheet" />
          <title>IDE Home!</title>
        </head>
        <body>

          <div id="root"></div>
          <script>
            const vscode = acquireVsCodeApi();
          </script>
          <script src="${bundleReactScriptPath}"></script>

        </body>
      </html>
    `;
  }


  public dispose() {
    IdeHomePanel.currentPanel = undefined;

    this._ideDb.close();
    this._panel.dispose();

    while (this._disposables.length) {
      const disposable = this._disposables.pop();
      if (disposable) {
        disposable.dispose();
      }
    }
  }


}