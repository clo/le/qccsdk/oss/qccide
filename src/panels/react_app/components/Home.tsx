/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import React, {useContext,useEffect} from 'react';
import {VSCodeTag, VSCodeDropdown,VSCodeOption, VSCodeButton} from '@vscode/webview-ui-toolkit/react';
import {Sdkcard} from './subcomponents/Sdkcard';
import {DemoLink} from './subcomponents/DemoLink';
import { MessagesContext } from "../context/MessageContext";
import ReactMarkdown from 'react-markdown';

enum ListType {
  demoList,
  sdkList
}

// eslint-disable-next-line @typescript-eslint/naming-convention
function SdkListComponent(props:any){
  return(
  <div >
    <p>SDK list </p>
    <section className="component-row">
      {props.sdkList}
    </section>
  </div>
  );
}

// eslint-disable-next-line @typescript-eslint/naming-convention
function DemoListComponent(props:any){

  return(
  <div >
    <p>Demo list </p>
    <VSCodeButton appearance="icon" aria-label="Return" onClick={props.demoListHandleReturnClick}>
      <span className="codicon codicon-chevron-left"></span>
    </VSCodeButton>

    <div className='component-row'>

      <section className='demoList'>
          {props.demoList}
      </section>

      <section className='demoDetail'>
        <VSCodeButton  onClick={props.demoListHandleCreateClick}>Create project with this Demo</VSCodeButton>
        <ReactMarkdown>{props.demoDetailInfo}</ReactMarkdown>
      </section>
     
    </div>
  </div>
  );
}

// eslint-disable-next-line @typescript-eslint/naming-convention
function ListComponent(props:any){
  const listType = props.listType;
  
  if(listType === ListType.demoList){
    return <DemoListComponent demoList={props.contentList} demoDetailInfo={props.demoDetailInfo} demoListHandleReturnClick={props.demoListHandleReturnClick} demoListHandleCreateClick={props.demoListHandleCreateClick}/>;
  }

  if(listType === ListType.sdkList){
    return <SdkListComponent sdkList={props.contentList}/>;
  }

  return null;
}

interface ISdkcard {
  sdkName:string,
  sdkIntr:string,
  sdkVerList:string[],
  sdkInstalled?:boolean
}

interface Props {
  msgContext:Message
}

interface State {
    iSdkCards:ISdkcard[],
    idemoList:string[],
    type:string,
    typeNameList:string[],
    listType:ListType,
    currentDemo:string,
    demoInfo:string,
    preprops:any,
    // internalUpdate indicate whether rerender by external props or internal state cahange
    internalUpdate:boolean
}

class HomeImpl extends React.Component<Props, State> {
    constructor(props:any) {
      super(props);
      this.state={
        iSdkCards:[
          {sdkName:'2',sdkIntr:'test',sdkInstalled:false,sdkVerList:["2","3"]},
        ],
        idemoList:[],
        type:'Board',
        typeNameList:['24','20'],
        listType:ListType.sdkList,
        currentDemo:'',
        demoInfo:'',
        preprops:'',
        internalUpdate:false
      };
      this.demoListHandleReturnClick = this.demoListHandleReturnClick.bind(this);
      this.demoListHandleCreateClick = this.demoListHandleCreateClick.bind(this);
    }

    static getDerivedStateFromProps(props:Props, state:State) {

      // render trigged by props
        if(state.internalUpdate === false){

        console.log("received payload from extension",props.msgContext.payload);

        if(props.msgContext.type === "NAMELIST"){
          console.log('request new sdk list: ',props.msgContext.payload.split(',')[0]);
          vscode.postMessage<Message>({
            type: 'NAMECHANGE',
            payload: props.msgContext.payload.split(',')[0],
          });
          return {
            typeNameList:props.msgContext.payload.split(',')
          };
        }

        if(props.msgContext.type === "SDKLIST"){

          let sdkContentList:SdkItem[] = JSON.parse(props.msgContext.payload);
          let tempSdkCards:ISdkcard[] = [];
          let sdkNameCache:any={};
          let sdkCardIndex = 0;

          sdkContentList.forEach((sdkItem)=>{
            
            if( sdkNameCache[sdkItem.name]  === undefined){

              tempSdkCards.push({
                sdkName:sdkItem.name,
                sdkIntr:sdkItem.intr,
                sdkVerList:[sdkItem.ver],
              });
              
              sdkNameCache[sdkItem.name]  = sdkCardIndex;
              sdkCardIndex++;

            }
            else{
              let index =  sdkNameCache[sdkItem.name];
              tempSdkCards[index].sdkVerList.push(sdkItem.ver);
            }
          
          });

          return {
            iSdkCards:tempSdkCards,
            listType:ListType.sdkList
          };
        }

        if(props.msgContext.type === "DEMOLISTRSP"){
          
          return {
            idemoList:props.msgContext.payload.split(','),
            currentDemo:'',
            demoInfo:'',
            listType:ListType.demoList
          };
        }

        if(props.msgContext.type === "DEMOINFORSP"){
          let demoInfo:DemoInfo = JSON.parse(props.msgContext.payload);
          return {
            demoInfo:demoInfo.info,
            currentDemo:demoInfo.name
          };

        }

      }

      return {
        internalUpdate:false
      };
      
    
    }

    // should update sdk intro and select version here
    handleVerChange(event:any){

    }

    // should request install sdk or open sdk
    handleOpenButtonClick(event:any){

    }

    // should update typeNameList here
    handleTypeChange(event:any){
      // request new type name list
      console.log('request new type name list: ',event.target.value);
      vscode.postMessage<Message>({
        type: 'TYPE',
        payload: event.target.value,
      });
    }

    // should update iSdkCards here
    handleTypeNameChange(event:any){
      // request new sdk list
      console.log('request new sdk list: ',event.target.value);
      vscode.postMessage<Message>({
        type: 'NAMECHANGE',
        payload: event.target.value,
      });
    }

    demoListHandleReturnClick(){
      console.log("return to sdk card list.");
      this.setState((state,props)=>({
       listType:ListType.sdkList,
       internalUpdate:true
      }));
    }

    demoListHandleCreateClick(){
      console.log('create new project with: ',this.state.currentDemo);
      vscode.postMessage<Message>({
        type: 'CREATEDEMO',
        payload: this.state.currentDemo,
      });
    }

    render() {

      let contentList,demoDetailInfo;

      if(this.state.listType === ListType.sdkList){
        console.log("render sdk list.");

        if(this.state.iSdkCards && this.state.iSdkCards.length > 0){
          contentList=this.state.iSdkCards.map((iSdkCard)=>
            <Sdkcard 
            sdkVersionList={iSdkCard.sdkVerList} 
            sdkName={iSdkCard.sdkName}
            sdkIntr={iSdkCard.sdkIntr} 
            sdkInstallStatus={true} 

            handleOpenButtonClick={this.handleOpenButtonClick}
            handleVerChange={this.handleVerChange}

            key={iSdkCard.sdkName}
            />
          );
        }else{
          contentList=(0);
        }
      }

      if(this.state.listType === ListType.demoList){
        console.log("render demo list.");
        if(this.state.idemoList && this.state.idemoList.length > 0){
          contentList = this.state.idemoList.map((demoItem)=>
            <DemoLink key={demoItem}  demoName={demoItem} isCurrentDemoName={demoItem === this.state.currentDemo}/>
          );
        }
        else{
          contentList=(0);
        }
      }

      // update the demo information
      demoDetailInfo = this.state.demoInfo;

      let typeNameList = this.state.typeNameList.map((typeName)=>
        <VSCodeOption key={typeName}> {typeName} </VSCodeOption>
      );

      return (
        <div>
          
          <h1>Welcome to QCC IDE!</h1>

          <div>
            <p>
              Choose type to identify your device. Now only support Board.
            </p>
            <VSCodeTag>Type :  </VSCodeTag>

            <VSCodeDropdown onChange={this.handleTypeChange}>
              <VSCodeOption >Board</VSCodeOption>
              {/* <VSCodeOption >Chip</VSCodeOption> */}
            </VSCodeDropdown>
          </div>

          <div>
            <p>
              Choose the relative board name that you want to work on.
            </p>
            <VSCodeTag>Name : </VSCodeTag>

            <VSCodeDropdown onChange={this.handleTypeNameChange}>
              {typeNameList}
            </VSCodeDropdown>

          </div>

          <ListComponent listType={this.state.listType} contentList={contentList} demoDetailInfo={demoDetailInfo} 
          demoListHandleReturnClick={this.demoListHandleReturnClick} demoListHandleCreateClick={this.demoListHandleCreateClick}/>

        </div>
      );
    }
}

// eslint-disable-next-line @typescript-eslint/naming-convention
export const Home = ()=>{
  let msgContext = useContext(MessagesContext);

  const sendMessage = () => {
    vscode.postMessage<Message>({
      // the default Type is Board
      type: 'TYPE',
      payload: "Board",
    });
  };

  // when component mount, request the type name list
  useEffect(() => {
    sendMessage();
  },[]);

  return <HomeImpl msgContext={msgContext}/>;

};