/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import React, { useEffect, useState, useCallback } from 'react';
import {Home} from "./Home";
import { MessagesContext } from "../context/MessageContext";

// eslint-disable-next-line @typescript-eslint/naming-convention
export const App = () =>{

  const [messagesFromExtension, setMessagesFromExtension] = useState<Message>({
    type: 'COMMON',
    payload: "",
  });

  const handleMessagesFromExtension = useCallback(
      (event: MessageEvent<Message>) => {
        setMessagesFromExtension(event.data);
      },
      [messagesFromExtension]
    );

  useEffect(() => {
      window.addEventListener('message', (event: MessageEvent<Message>) => {
        handleMessagesFromExtension(event);
      });
  
      return () => {
        window.removeEventListener('message', handleMessagesFromExtension);
      };
    }, [handleMessagesFromExtension]);
  

  return(
      <MessagesContext.Provider value={messagesFromExtension}>
        <Home />
      </MessagesContext.Provider>
  );
};
