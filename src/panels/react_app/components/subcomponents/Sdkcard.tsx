/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import React from 'react';
import {VSCodeDropdown,VSCodeOption,VSCodeButton} from '@vscode/webview-ui-toolkit/react';

interface Props {
    // version list for each sdk
    sdkVersionList:string[];

    // sdk name
    sdkName:string;
    
    // sdk introduction of this version
    sdkIntr:string;

    // sdk install status
    sdkInstallStatus:boolean;

    // callback happedned when VSCodeDropdown changed
    handleVerChange:((e: Event) => unknown) & React.FormEventHandler<HTMLElement>;

    // callback happedned when VSCodeButton clicked
    handleOpenButtonClick:React.MouseEventHandler<HTMLElement>;

}

interface State {
    sdkName:string;
    currentVer: string;

}

export class Sdkcard extends React.Component<Props, State> {
    constructor(props:any) {
        super(props);
        this.state = {currentVer: this.props.sdkVersionList[0],
                    sdkName:this.props.sdkName
            
                    };
    
        // This binding is necessary to make `this` work in the callback
        this.onOpenButtonClick = this.onOpenButtonClick.bind(this);
        this.onVerChange = this.onVerChange.bind(this);
      }


    // should request open sdk, the extension will decide whether open or install
    onOpenButtonClick(event:any){
        console.log("open "+this.state.sdkName+" "+this.state.currentVer);

        let msgPayload :SdkItem= {
            name:this.state.sdkName,
            intr:'',
            ver: this.state.currentVer,
            verIntr:''
        };
        vscode.postMessage<Message>({
            type: 'OPENSDK',
            payload: JSON.stringify(msgPayload),
        });

        this.props.handleOpenButtonClick;
    }

    // should update current version
    onVerChange(event:any){
        this.setState((state)=>{
            return {currentVer: event.target.value};
        });
        console.log("Change "+this.state.sdkName+" version to "+event.target.value);
    }

    render() {

        const versionList =this.props.sdkVersionList.map((ver)=>
            <VSCodeOption key={ver.toString()}>
            {ver}
            </VSCodeOption>
        );

        return (
            <section className="card-container">
            <h2>{this.props.sdkName}</h2>
        
            <p>{this.props.sdkIntr}</p>
        
            <section className="component-example">
                <VSCodeDropdown onChange={this.onVerChange}>
                    {versionList}
                </VSCodeDropdown>
    
                <VSCodeButton appearance="primary" onClick = {this.onOpenButtonClick}>Open SDK</VSCodeButton>
            </section>
        
          </section>
        );
      }
    }
