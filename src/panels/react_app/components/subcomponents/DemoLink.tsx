/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import React from 'react';


// eslint-disable-next-line @typescript-eslint/naming-convention
function LinkComponent(props:any){
    if (props.isCurrentDemoName){
        return (
            <a className='demoCurrentItem' onClick={props.handleClick}> {props.demoName}</a>
        ); 
    }
    else{
        return (
            <a className='demoItem' onClick={props.handleClick}> {props.demoName}</a>
        ); 
    }
}

interface Props {
    // demo name
    demoName:string;
    isCurrentDemoName:boolean

}

interface State {

}

export class DemoLink extends React.Component<Props, State> {

    constructor(props:any) {
        super(props);
    
        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(){
        console.log("read demo information: "+ this.props.demoName);
        vscode.postMessage<Message>({
            type: 'READDEMOINFO',
            payload: this.props.demoName,
        });
    }

    render() {

        return (
            <LinkComponent handleClick={this.handleClick} demoName={this.props.demoName} isCurrentDemoName={this.props.isCurrentDemoName}/>
        );
    }

}