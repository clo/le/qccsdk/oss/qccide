/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

type Message = import('../messageTypes').Message;
type SdkItem = import('../messageTypes').SdkItem;
type DemoInfo = import('../messageTypes').DemoInfo;

type VSCode = {
  postMessage<T extends Message = Message>(message: T): void;
  getState(): any;
  setState(state: any): void;
};

declare const vscode: VSCode;

declare const apiUserGender: string;
