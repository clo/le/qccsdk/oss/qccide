/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import React from 'react';

// eslint-disable-next-line @typescript-eslint/naming-convention
export const MessagesContext = React.createContext<Message>({
    type: 'COMMON',
    payload: "",
  });
