/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';
import { mkdirp } from 'mkdirp';
import { rimraf } from 'rimraf';
import * as utils from '../utils/utils';
import {activePrjCfg} from '../extension';
//#region Utilities

namespace _ {

	function handleResult<T>(resolve: (result: T) => void, reject: (error: Error) => void, error: Error | null | undefined, result: T): void {
		if (error) {
			reject(massageError(error));
		} else {
			resolve(result);
		}
	}

	function massageError(error: Error & { code?: string }): Error {
		if (error.code === 'ENOENT') {
			return vscode.FileSystemError.FileNotFound();
		}

		if (error.code === 'EISDIR') {
			return vscode.FileSystemError.FileIsADirectory();
		}

		if (error.code === 'EEXIST') {
			return vscode.FileSystemError.FileExists();
		}

		if (error.code === 'EPERM' || error.code === 'EACCESS') {
			return vscode.FileSystemError.NoPermissions();
		}

		return error;
	}

	export function checkCancellation(token: vscode.CancellationToken): void {
		if (token.isCancellationRequested) {
			throw new Error('Operation cancelled');
		}
	}

	export function normalizeNFC(items: string): string;
	export function normalizeNFC(items: string[]): string[];
	export function normalizeNFC(items: string | string[]): string | string[] {
		if (process.platform !== 'darwin') {
			return items;
		}

		if (Array.isArray(items)) {
			return items.map(item => item.normalize('NFC'));
		}

		return items.normalize('NFC');
	}

	export function readdir(path: string): Promise<string[]> {
		return new Promise<string[]>((resolve, reject) => {
			fs.readdir(path, (error, children) => handleResult(resolve, reject, error, normalizeNFC(children)));
		});
	}

	export function stat(path: string): Promise<fs.Stats> {
		return new Promise<fs.Stats>((resolve, reject) => {
			fs.stat(path, (error, stat) => handleResult(resolve, reject, error, stat));
		});
	}

	export function readfile(path: string): Promise<Buffer> {
		return new Promise<Buffer>((resolve, reject) => {
			fs.readFile(path, (error, buffer) => handleResult(resolve, reject, error, buffer));
		});
	}

	export function writefile(path: string, content: Buffer): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			fs.writeFile(path, content, error => handleResult(resolve, reject, error, void 0));
		});
	}

	export function exists(path: string): Promise<boolean> {
		return new Promise<boolean>((resolve, reject) => {
			fs.exists(path, exists => handleResult(resolve, reject, null, exists));
		});
	}

	export function rmrf(path: string): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			rimraf(path);
		});
	}

	export function mkdir(path: string): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			mkdirp(path);
		});
	}

	export function rename(oldPath: string, newPath: string): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			fs.rename(oldPath, newPath, error => handleResult(resolve, reject, error, void 0));
		});
	}

	export function unlink(path: string): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			fs.unlink(path, error => handleResult(resolve, reject, error, void 0));
		});
	}
}

export class FileStat implements vscode.FileStat {

	constructor(private fsStat: fs.Stats) { }

	get type(): vscode.FileType {
		return this.fsStat.isFile() ? vscode.FileType.File : this.fsStat.isDirectory() ? vscode.FileType.Directory : this.fsStat.isSymbolicLink() ? vscode.FileType.SymbolicLink : vscode.FileType.Unknown;
	}

	get isFile(): boolean | undefined {
		return this.fsStat.isFile();
	}

	get isDirectory(): boolean | undefined {
		return this.fsStat.isDirectory();
	}

	get isSymbolicLink(): boolean | undefined {
		return this.fsStat.isSymbolicLink();
	}

	get size(): number {
		return this.fsStat.size;
	}

	get ctime(): number {
		return this.fsStat.ctime.getTime();
	}

	get mtime(): number {
		return this.fsStat.mtime.getTime();
	}
}


interface Entry {
	label:string;
	uri: vscode.Uri;
	type: vscode.FileType | "PRJNAME";
}

//#endregion

export class FileSystemProvider implements vscode.TreeDataProvider<Entry>, vscode.FileSystemProvider {

	private _onDidChangeFile: vscode.EventEmitter<vscode.FileChangeEvent[]>;
	private _onDidChangeTreeData: vscode.EventEmitter<any> = new vscode.EventEmitter<any>();
	readonly onDidChangeTreeData: vscode.Event<any> = this._onDidChangeTreeData.event;

	constructor() {
		this._onDidChangeFile = new vscode.EventEmitter<vscode.FileChangeEvent[]>();
	}

	public refresh(): any {
		this._onDidChangeTreeData.fire(undefined);
	}

	get onDidChangeFile(): vscode.Event<vscode.FileChangeEvent[]> {
		return this._onDidChangeFile.event;
	}

	watch(uri: vscode.Uri, options: { recursive: boolean; excludes: string[]; }): vscode.Disposable {
		// const watcher = fs.watch(uri.fsPath, { recursive: options.recursive }, async (event: string, filename: string | Buffer) => {
		// 	const filepath = path.join(uri.fsPath, _.normalizeNFC(filename.toString()));

		// 	// TODO support excludes (using minimatch library?)

		// 	this._onDidChangeFile.fire([{
		// 		type: event === 'change' ? vscode.FileChangeType.Changed : await _.exists(filepath) ? vscode.FileChangeType.Created : vscode.FileChangeType.Deleted,
		// 		uri: uri.with({ path: filepath })
		// 	} as vscode.FileChangeEvent]);
		// });

		return { dispose: () => {} };
	}

	stat(uri: vscode.Uri): vscode.FileStat | Thenable<vscode.FileStat> {
		return this._stat(uri.fsPath);
	}

	async _stat(path: string): Promise<vscode.FileStat> {
		return new FileStat(await _.stat(path));
	}

	readDirectory(uri: vscode.Uri): [string, vscode.FileType][] | Thenable<[string, vscode.FileType][]> {
		return this._readDirectory(uri);
	}

	async _readDirectory(uri: vscode.Uri): Promise<[string, vscode.FileType][]> {
		const children = await _.readdir(uri.fsPath);

		const result: [string, vscode.FileType][] = [];
		for (let i = 0; i < children.length; i++) {
			const child = children[i];
			const stat = await this._stat(path.join(uri.fsPath, child));
			result.push([child, stat.type]);
		}

		return Promise.resolve(result);
	}

	createDirectory(uri: vscode.Uri): void | Thenable<void> {
		return _.mkdir(uri.fsPath);
	}

	readFile(uri: vscode.Uri): Uint8Array | Thenable<Uint8Array> {
		return _.readfile(uri.fsPath);
	}

	writeFile(uri: vscode.Uri, content: Uint8Array, options: { create: boolean; overwrite: boolean; }): void | Thenable<void> {
		return this._writeFile(uri, content, options);
	}

	async _writeFile(uri: vscode.Uri, content: Uint8Array, options: { create: boolean; overwrite: boolean; }): Promise<void> {
		const exists = await _.exists(uri.fsPath);
		if (!exists) {
			if (!options.create) {
				throw vscode.FileSystemError.FileNotFound();
			}

			await _.mkdir(path.dirname(uri.fsPath));
		} else {
			if (!options.overwrite) {
				throw vscode.FileSystemError.FileExists();
			}
		}

		return _.writefile(uri.fsPath, content as Buffer);
	}

	delete(uri: vscode.Uri, options: { recursive: boolean; }): void | Thenable<void> {
		if (options.recursive) {
			return _.rmrf(uri.fsPath);
		}

		return _.unlink(uri.fsPath);
	}

	rename(oldUri: vscode.Uri, newUri: vscode.Uri, options: { overwrite: boolean; }): void | Thenable<void> {
		return this._rename(oldUri, newUri, options);
	}

	async _rename(oldUri: vscode.Uri, newUri: vscode.Uri, options: { overwrite: boolean; }): Promise<void> {
		const exists = await _.exists(newUri.fsPath);
		if (exists) {
			if (!options.overwrite) {
				throw vscode.FileSystemError.FileExists();
			} else {
				await _.rmrf(newUri.fsPath);
			}
		}

		const parentExists = await _.exists(path.dirname(newUri.fsPath));
		if (!parentExists) {
			await _.mkdir(path.dirname(newUri.fsPath));
		}

		return _.rename(oldUri.fsPath, newUri.fsPath);
	}

	// tree data provider
	async getChildren(element?: Entry): Promise<Entry[]> {
		if(element){
			if (element?.type === "PRJNAME") {

				if(vscode.workspace.workspaceFolders)
				{
					const workspaceFolder = vscode.workspace.workspaceFolders.filter(folder => folder.uri.scheme === 'file')[0];
					if (workspaceFolder) {
						let workspacefspath = vscode.workspace.workspaceFolders[0].uri.fsPath;
						let fspath = utils.getStaticCfgFileLocation(workspacefspath);
						try{
							let cfgContent = fs.readFileSync(fspath).toString();
							if(!cfgContent)
							{
								return [];
							}
							let cfgJson = JSON.parse(cfgContent);
							if(cfgJson)
							{
								// update cfgJson first if has ${} in it 
								utils.parseJsonWithVarAndPrjName(cfgJson,element.label);

								const incPaths = utils.getPrjIncPath(element.label, cfgJson);
								
								const children: [string, vscode.FileType][] = [];

								for (let i = 0; i < incPaths.length; i++) {

									// Check whether path exist
									if(fs.existsSync(path.join(workspacefspath, incPaths[i])))
									{
										const stat = await this._stat(path.join(workspacefspath, incPaths[i]));
										children.push([incPaths[i], stat.type]);
									}
								}

								children.sort((a, b) => {
									if (a[1] === b[1]) {
										return a[0].localeCompare(b[0]);
									}
									return a[1] === vscode.FileType.Directory ? -1 : 1;
								});
								return children.map(([name, type]) => ({label:"", uri: vscode.Uri.file(path.join(workspaceFolder.uri.fsPath, name)), type }));
							}
						}	
						catch (err){
								console.error(err);
								return [];
						}
					}
				}
			}
			
			else {
				const children = await this.readDirectory(element?.uri);
				children.sort((a, b) => {
					if (a[1] === b[1]) {
						return a[0].localeCompare(b[0]);
					}
					return a[1] === vscode.FileType.Directory ? -1 : 1;
				});
				return children.map(([name, type]) => ({label:'', uri: vscode.Uri.file(path.join(element.uri.fsPath, name)), type }));
			}
		}

		// root element
		if(vscode.workspace.workspaceFolders && !element)
		{
			const workspaceFolder = vscode.workspace.workspaceFolders.filter(folder => folder.uri.scheme === 'file')[0];
			if (workspaceFolder) {
				// 1. read qccide workspace configure file
				let path = vscode.workspace.workspaceFolders[0].uri.fsPath;
				path = utils.getStaticCfgFileLocation(path);
				try{
					let cfgContent = fs.readFileSync(path).toString();
					if(!cfgContent)
					{
						return [];
					}
					let cfgJson = JSON.parse(cfgContent);
					if(cfgJson)
					{
						// 2. return name of each project
						let children = utils.getPrj(cfgJson);

						children.sort((a, b) => {
							return a.localeCompare(b);
						});
						return children.map((name) => ({label: name, uri:vscode.Uri.file(''),type:"PRJNAME" }));
					}
				}
				catch (err){
					console.error(err);
					return [];
				}
			}
		}

		return [];
	}

	getTreeItem(element: Entry): vscode.TreeItem {
		const treeItem = new vscode.TreeItem(element.uri, (element.type === vscode.FileType.Directory || element.type === "PRJNAME")? vscode.TreeItemCollapsibleState.Collapsed : vscode.TreeItemCollapsibleState.None);
		if (element.type === "PRJNAME") {
			treeItem.label = element.label;
			treeItem.contextValue = "QCCIDE_PRJ";
			treeItem.iconPath = new vscode.ThemeIcon('project');
		}
		if (element.type === vscode.FileType.File) {
			treeItem.command = { command: 'qualIoe.ideWorkspaceOpenFile', title: "Open File", arguments: [element.uri], };
			treeItem.contextValue = 'file';
		}
		return treeItem;
	}

	// This is a recursive called function
	getParent(element: Entry): Entry|undefined {
		const parent = element.uri.with({ path: path.dirname(element.uri.path) });
		if(element.type !== "PRJNAME")
		{
			// Check if meet the root folders in active project configure first
			if(activePrjCfg)
			{
				// Parse ${} first
				let activePrjCfgCopy = utils.parseJsonWithVar(activePrjCfg);
				// use the copy version activePrjCfgCopy instead
				for(let i=0; i<activePrjCfgCopy.incPaths.length;i++)
				{
					if(vscode.workspace.workspaceFolders)
					{
						if(element.uri.fsPath === path.join(vscode.workspace.workspaceFolders[0].uri.fsPath,activePrjCfgCopy.incPaths[i]))
						{
							return {label:activePrjCfgCopy.prjName, uri: vscode.Uri.file(''),  type:"PRJNAME"};
						}
					}
				}
			}

			// Do not meet the root folders in activePrjCfg, check other project configure in order
			if(vscode.workspace.workspaceFolders)
			{
				const workspaceFolder = vscode.workspace.workspaceFolders.filter(folder => folder.uri.scheme === 'file')[0];
				if (workspaceFolder) {
					let workspacefspath = vscode.workspace.workspaceFolders[0].uri.fsPath;
					let fspath = utils.getStaticCfgFileLocation(workspacefspath);
					try{
						let cfgContent = fs.readFileSync(fspath).toString();
						if(!cfgContent)
						{
							return undefined;
						}
						let cfgJson = JSON.parse(cfgContent);
						if(cfgJson)
						{
						
							for(let i=0;i<cfgJson.length;i++)
							{
								// Parse ${} first
								utils.parseJsonWithVarAndPrjName(cfgJson,cfgJson[i].prjName);
								for(let j=0;j<cfgJson[i].incPaths.length;j++)
								{
									if(element.uri.fsPath === path.join(vscode.workspace.workspaceFolders[0].uri.fsPath,cfgJson[i].incPaths[j]))
									{
										return {label:cfgJson[i].prjName, uri: vscode.Uri.file(''),  type:"PRJNAME"};
									}
								}
							}
						}
					}
					catch (err){
						console.error(err);
						return undefined;
					}
				}
			}

			// Not the root folders in project configure,just return the path parent
			return parent.path !== '/' ? {label:path.basename(parent.path), uri: parent,  type:vscode.FileType.Directory} : undefined;
		}else{
			return undefined;
		}
		
	}
}

export class IdeWorkspace {
	constructor(context: vscode.ExtensionContext) {
		const treeDataProvider = new FileSystemProvider();
		const ideView = vscode.window.createTreeView('ideWorkspace', { treeDataProvider });
		context.subscriptions.push(ideView);
		vscode.commands.registerCommand('qualIoe.ideWorkspaceOpenFile', (resource) => this.openResource(resource));

		vscode.commands.registerCommand('qualIoe.revealInQccIdeWorkspace', () => {
			if (vscode.window.activeTextEditor) {
				let resource = vscode.window.activeTextEditor.document.uri;
				let label = path.basename(resource.path);
				ideView.reveal({ label:label,uri:resource, type:vscode.FileType.File}, { focus: true, select: false, expand: true });
			}
		});

		vscode.commands.registerCommand('qualIoe.refreshAllWorkspaceEntry', () => treeDataProvider.refresh());
	}

	private openResource(resource: vscode.Uri): void {
		vscode.window.showTextDocument(resource);
	}
}