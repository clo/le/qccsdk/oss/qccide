/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import * as vscode from "vscode";
import { join } from "path";
import * as qualConfig from '../config/globalConfigure';
import * as os from 'os';
import * as precheck from '../utils/check';
import { TaskManager } from "../task/taskManager";
import {appendPathToEnv} from "../utils/utils";

/**
 * use command line interface
 */
export class CommandTask {
    

    constructor() {
    }
  
    private async saveBeforeRun() {
      const shallsaveBeforeRun = qualConfig.readParameter("qualIoe.commonCfg.saveBeforeRun");
      if (shallsaveBeforeRun) {
        if(vscode.workspace.workspaceFolders){
          await vscode.workspace.saveAll();
        }
      }
    }
  
    private getShellExecution(
      comandLine: string,
      options?: vscode.ShellExecutionOptions
    ) {
      return new vscode.ShellExecution(comandLine, options);
    }

/**
 * 
 * @param curWorkspace the path of execute script
 * @param commandLine  the entrie command line to be executed
 * @param taskName    the name of this task
 * @param selfEnv     the environment that need specified
 */
    public async addCommandTask(curWorkspace: string, commandLine: string, taskName:string,selfEnv?:any) {
        try {
            await this.saveBeforeRun();
          } catch (error) {
            const errorMessage =
              "Failed to save unsaved files, ignoring and continuing with the command run";
            console.log(errorMessage, error);
          }

        const isSilentMode = qualConfig.readParameter("qualIoe.commonCfg.notificationSilentMode");
        const showTaskOutput = isSilentMode
          ? vscode.TaskRevealKind.Silent
          : vscode.TaskRevealKind.Always;

        if(!precheck.isStrDirectory(curWorkspace))
        {
            throw new Error("The path is not a directory");
        }
        else
        {
            let nameInEnv;
            if (process.platform === "win32") {
              nameInEnv = "Path";
            } else {
              nameInEnv = "PATH";
            }
            let extEnv = appendPathToEnv(nameInEnv,selfEnv);
            const options: vscode.ShellExecutionOptions = {
                cwd: curWorkspace,
                env: extEnv,
              };

            const compileExecution = this.getShellExecution(
                commandLine,
                options
              );

            TaskManager.addTask(
              //the format of this json need be same within package.json "taskDefinitions"
              { type: "qual-ioe", command: "${commandLine}" },  
              vscode.TaskScope.Workspace,
              taskName,
              compileExecution,
              //TODO: change to according regex in package.json, problemMatchers
              //https://www.allisonthackston.com/articles/vscode-tasks-problemmatcher.html
              ["qualIoeRelative", "qualIoeAbsolute"],
              showTaskOutput
            );

        }

        
    }

    

}