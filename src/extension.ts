/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as qualConfig from './config/globalConfigure';
import * as os from 'os';
import { CommandTask } from './command/CommandTask';
import { join,basename,extname } from "path";
import * as cp from "child_process";
import * as fs from 'fs';
import { TaskManager } from "./task/taskManager";
import { kill } from "process";
import * as utils from './utils/utils'; 
import {isStrDirectory} from './utils/check';
import {activateQcaDebugAdapter} from './debugAdapter/activateQcaDebugAdapter';
import{GNAME} from './config/globalName';
import{RAMDUMP} from './ramdump/ramdump';
import { IdeHomePanel } from "./panels/ideHomePanel";
import * as sqlite3 from 'sqlite3';
import { SerialPort } from "serialport";
import * as colors from 'colors';

import { Uri,CancellationTokenSource  } from "vscode";
import { getApi, FileDownloader } from "@microsoft/vscode-file-downloader-api";

import { IdeWorkspace } from './explorer/fileExplorer';
import {PrjCfgSetTypeIndex,PrjCfgBuildTypeIndex, PrjCfgCleanTypeIndex,PrjCfgFlashTypeIndex,isSdkRootPath,isDemoPath,getSdkFromDemoPath}  from './cfgInterpreter/cfgInterpreter';


// bar item
const statusBarItems: vscode.StatusBarItem[] = [];


let ramdumpTerminal: vscode.Terminal | undefined = undefined;
let consoleDownloadTerminal: vscode.Terminal | undefined = undefined;

let extensionPath:string;
let extensionPYdebugBackendPath:string;
let extensionPYDownloadPath:string;
let extensionGdbWithPyPath:string;
let ideUserPath:string;
let ideUserCfgFolderPath:string;
let ideUserDownloadCfgFolderPath:string;
let ideUserDownloadCfgURIFolderPath:string;
let ideUserSdkDownloadFolderPath:string;
let ideUserPyVenvFolderPath:string;
let ideName:string = '.qcc_ide';
let vscodeTemplateFolderFromExtension = 'config/template/.vscode';
let sdkCfgFolderFromExtension = 'config/sdk_config';
let sdkfolderForIde = 'tools/qccide/';
let cCppPropertiesName = 'c_cpp_properties.json';
let gitignoreFileName = '.gitignore';
let sdkScopeEnv :JSON|undefined ;

let extensionDbPath:string;
let extensionDbRelativePath='database/ide.db';
let extensionIdeDb:any;

const barCommandIdTotalCount = 100;
let barCommandId:number = barCommandIdTotalCount;
let bUpdateLocalStaticCfg = true;
// activePrjCfg dont parse ${}, just parsed when be used
let activePrjCfg:any = {};
// allPrjCfg dont parse ${}
let allPrjCfg:any = {};
let visibleTextEditors :vscode.TextEditor[];
let ramdumpFileCnt = 0;

export {extensionPYdebugBackendPath, extensionPath, ideUserPath,sdkCfgFolderFromExtension,ideName,ideUserCfgFolderPath,ideUserDownloadCfgFolderPath,ideUserDownloadCfgURIFolderPath,ideUserSdkDownloadFolderPath,ideUserPyVenvFolderPath,extensionPYDownloadPath,cCppPropertiesName,activePrjCfg,sdkScopeEnv};


interface SdkInfo{
	sdkVer:string,
	sdkName:string,
	sdkStatus:string
}

function createStatusBarItem(
	icon: string,
	tooltip: string,
	cmd: string,
	priority: number
  ) {
	const alignment: vscode.StatusBarAlignment = vscode.StatusBarAlignment.Left;
	const statusBarItem = vscode.window.createStatusBarItem(alignment, priority);
	statusBarItem.text = icon;
	statusBarItem.tooltip = tooltip;
	statusBarItem.command = cmd;
	statusBarItem.show();
	statusBarItems.push(statusBarItem);
  }



function creatCmdsStatusBarItems() {
createStatusBarItem(
	"$(gear)",
	"QCC-IDE Launch GUI Configuration tool",
	"qualIoe.config",
	barCommandId--
);
createStatusBarItem(
	"$(plug)",
	"QCC-IDE Select device port",
	"qualIoe.selectPort",
	barCommandId--
);
createStatusBarItem(
	"$(database)",
	"QCC-IDE Build Image",
	"qualIoe.buildImage",
	barCommandId--
);
createStatusBarItem(
	"$(trash)", 
	"QCC-IDE Clean Image", 
	"qualIoe.cleanImage", 
	barCommandId--
);
createStatusBarItem(
	"$(zap)",
	"QCC-IDE Flash device",
	"qualIoe.flash",
	barCommandId--
);
createStatusBarItem(
	"$(device-desktop)",
	"QCC-IDE console",
	"qualIoe.console",
	barCommandId--
);
createStatusBarItem(
	"$(flame)",
	"QCC-IDE Build, Flash and console",
	"qualIoe.flashAndConsole",
	barCommandId--
);
}
  
function getCurrentBarCommandCount():number{
	return barCommandIdTotalCount - barCommandId + 1; 
}

function isTaskRunning()
{
	if (TaskManager.isRunning)
	{
		console.log("Please wait for previous task to finish");
		vscode.window.showInformationMessage('Please wait for previous task to finish');
		return true;
	}
	return  false;
}

async function runCmdTasks()
{
	if (isTaskRunning())
	{
		return Promise.reject();
	}
	else
	{
		await TaskManager.runTasks()
		.then(()=>{
			return Promise.resolve();
		})
		.catch(()=>{
			return Promise.reject();
		});

	}
}

/**
 * @name getDefaultDemo
 * @description get the default demo folder in current work space, if not exist, choose it
 * @param currentPath [in] current work space including demo folders
 * @param regexMatch  [in] the regex to match demo folder like '.*demo$' for QCA402X
 * @returns currentPath append demo folder
 */
async function getDefaultDemo(currentPath:string , regexMatch:string){
	// Chose demo if not default in configure
	let defaultDemo = qualConfig.readParameter("qualIoe.staticCfg.defaultDemo") as string;
	let buildPath = currentPath; 
	let regex = new RegExp(regexMatch);
	if(defaultDemo===null || defaultDemo===undefined || defaultDemo==='')
	{
		let contentList = fs.readdirSync(buildPath);
		let demos = contentList.filter( function( elm ) {return elm.match(regex);});
		await vscode.window.showQuickPick(demos).then(
			async (chosenDemo)=>{
				buildPath = join(buildPath,chosenDemo as string);
		});
	}
	else
	{
		buildPath = join(buildPath,defaultDemo as string);
	}
	return buildPath;
}


/**
 * @description get demo name from path
 * @param path the path contain demo
 */
function getDemoNameFromPath(path:string){
	return '';
}

/**
 * @description create console for port
 * @returns 
 */
function createPortConsole(){
	
	let chosenPort;
	if(os.platform() === 'win32')
	{
		chosenPort = qualConfig.readParameter('qualIoe.commonCfg.consolePortWin') as string;
	}
	else
	{
		chosenPort = qualConfig.readParameter('qualIoe.commonCfg.consolePortLinux') as string;
	}

	if(chosenPort === '')
	{
		vscode.window.showErrorMessage('Please set an com port first.');
		return;
	}

	let consoleTerminal: vscode.Terminal | undefined = undefined;
	if (consoleTerminal === undefined) {

		const writeEmitter = new vscode.EventEmitter<string>();
		let baudrate = qualConfig.readParameter('qualIoe.commonCfg.consolePortBaudrate');
		console.log("Create console for port: %s at %s", chosenPort,baudrate);
	
		var noticeMessage: string;
		let port =  new SerialPort({ path: chosenPort, baudRate: Number(baudrate) }, () => {
			noticeMessage = port.isOpen ?
				colors.green.bold(port.path + ' CONNECTED at baudrate: ' +  baudrate + '\r\n\r\n')
				: colors.red.bold(port.path + ' OPEN FAILED! \r\n\r\n');
		});
	
		port.addListener("data", (data) => {
			writeEmitter.fire(data.toString());
		});

		port.on("close", () => {
            writeEmitter.fire(colors.red.bold(port.path + " CLOSED! \r\n\r\n"));
        });

        consoleTerminal = vscode.window.createTerminal({
          name: "PORT console",
		  pty: {
			onDidWrite: writeEmitter.event,
			open: () => {
				writeEmitter.fire(noticeMessage);
			},
			close: () => {
				port.close();
			},
			handleInput: (data:any) => {
				port.write(data);
			}
		},
        });

		consoleTerminal.show();
      }
	  else
	  {
		vscode.window.showInformationMessage('Port console has been opened before.');
	  }
	  
}


/**
 * @function checkSdkPath
 * @param sdkPath      [in] the checked path
 * @param sdkParentDir [in] the basename of a sdk path 
 * @returns true: the path is sdk path
 * 			false: the path is not a valid sdk path
 */
function checkSdkPath(sdkPath :string, sdkParentDir :string){

	if(utils.checkIsSdk(sdkPath as string, sdkParentDir as string) === false)
	{
		vscode.window.showErrorMessage('The sdkPath is wrong: ' + sdkPath as string +', do you missed the sdk parent directory name: '+ sdkParentDir as string +'?');
		console.error('The sdkPath is wrong: %s, do you missed the sdk parent directory name: %s ?',sdkPath as string, sdkParentDir as string);
		return false;
	}
	return true;
}


/**
 * @description update sdk path to config json file qccIdeProjConfig.json
 * @param cfgJson the content of config json file
 */
async function updateSdkPathOnceOpenFolder(cfgJson:any,demoPathFromRoot:string,demoFolderName:string)
{
	// update sdkpath
	if(vscode.workspace.workspaceFolders !== undefined)
	{
		let path = vscode.workspace.workspaceFolders[0].uri.fsPath;
		let sdkPath;
		let sdkParentDir = qualConfig.readParameter('qualIoe.staticCfg.sdkParentDir');
		let vscodeKey = 'qualIoe.staticCfg.sdkPath';
		const target = qualConfig.readParameter("qualIoe.commonCfg.saveScope");

		if(isSdkRootPath(path,demoPathFromRoot))
		{
			// this folder opened from root path 
			sdkPath = path;
			console.log('this folder opened from sdk root path ');
			sdkParentDir = basename(path);
		}
		else if(isDemoPath(path,demoFolderName))
		{
			// this folder opened from demo path, just match the sdk path 
			// TODO: if demo is not in sdk folder
			sdkPath = getSdkFromDemoPath(path,demoPathFromRoot,demoFolderName) as string;
			sdkParentDir = basename(sdkPath);
		}
		else{
			vscode.window.showErrorMessage('The opened project path is not valid '+ path);
			console.error('The opened project path is not valid '+ path);
			return;
		}

		vscodeKey = 'qualIoe.staticCfg.sdkParentDir';
		await qualConfig.writeParameter(
			vscodeKey,
			sdkParentDir,
			target as vscode.ConfigurationTarget
		);

		vscodeKey = 'qualIoe.staticCfg.sdkPath';
		cfgJson['sdkPath'] = sdkPath;
		await qualConfig.writeParameter(
			vscodeKey,
			sdkPath,
			target as vscode.ConfigurationTarget
		);

	}
}



/**
 * @function initStaticCfg
 * @description init parameter according the local qccIdeProjConfig.json file
 * @returns 
 */
async function initStaticCfg(prjName?:string)
{
	if(vscode.workspace.workspaceFolders !== undefined)
	{

		let path = vscode.workspace.workspaceFolders[0].uri.fsPath;
		path = utils.getStaticCfgFileLocation(path);
		try{
			let cfgContent = fs.readFileSync(path).toString();
			if(!cfgContent)
			{
				return Promise.reject();
			}
			let cfgJson = JSON.parse(cfgContent);
			let activePrjIndex:number;
			if(cfgJson)
			{
				allPrjCfg = cfgJson;
				bUpdateLocalStaticCfg = false;

				// If prjName exist
				if(prjName)
				{
					utils.updateActivePrjStatus(cfgJson,prjName);
				}

				activePrjIndex = utils.getActivePrjIndex(cfgJson);

				//Note: just read qccIdeProjConfig.json here, but do not change it, except 'sdkPath'
				for(let key in cfgJson[activePrjIndex])
				{
					if(key === 'sdkPath')
					{
						await updateSdkPathOnceOpenFolder(cfgJson[activePrjIndex],cfgJson[activePrjIndex].demoPath,cfgJson[activePrjIndex].defaultDemoPathName);
						let cfgStr = JSON.stringify(cfgJson,null,2);
				
						try{
							fs.writeFileSync(path, cfgStr);
						}catch (error){
							console.error(error);
						}
						continue;
					}

					let vscodeKey = 'qualIoe.staticCfg.' + key;
					let currentValue = qualConfig.readParameter(vscodeKey);
					const target = qualConfig.readParameter("qualIoe.commonCfg.saveScope");
					// use value in parsedCfgJson to update
					if(currentValue !== undefined)
					{
						await qualConfig.writeParameter(
							vscodeKey,
							cfgJson[activePrjIndex][key],
							target as vscode.ConfigurationTarget
						);
					}
				}

				bUpdateLocalStaticCfg = true;
				activePrjCfg = cfgJson[activePrjIndex];
				allPrjCfg = cfgJson;
				return true;
			}
			else
			{
				return Promise.reject();
			}
		}catch (err){
			console.error(err);
			return Promise.reject();
		}
	}
	else
	{
		// if we do not open a folder, we do not need init automatically
		return true;
	}

}

export function updateSdkScopeEnv(){
	if(vscode.workspace.workspaceFolders){
		sdkScopeEnv = utils.getSdkEnv(vscode.workspace.workspaceFolders[0].uri.fsPath as string);
	}
}

async function checkBoardSetFromSdkScopeEnv() {
	if(sdkScopeEnv && "demoChipBoard" in sdkScopeEnv)
	{
		const showNotify = qualConfig.readParameter("qualIoe.commonCfg.showSetBoardNotify");
		if(showNotify)
		{
			const selection = await vscode.window.showInformationMessage('Do you want set the board ?', `Set boardto ${sdkScopeEnv['demoChipBoard']}`,  "Don't show again");

			if(!selection)
			{
				return;
			}
			if (selection !== "Don't show again") {
				if(sdkScopeEnv && "demoChipBoard" in sdkScopeEnv)
				{
					vscode.commands.executeCommand("qualIoe.setBoard", sdkScopeEnv['demoChipBoard'],true);
				
				}
			}else{
				let paramName = "qualIoe.commonCfg.showSetBoardNotify";
				const target = qualConfig.readParameter("qualIoe.commonCfg.saveScope");
				qualConfig.writeParameter(
					paramName,
					false,
					target as vscode.ConfigurationTarget
				);
			}
		}

	}else{
		console.log("demoChipBoard not in sdkScopeEnv");
	}
}
/**
 *  @function ideEnvInit
 *  @description init ide environment, like cmd shell, c reference
 *  @return 
 * 
 */
//  add download environment to self env (extEnv used current) here
function ideEnvInit(){

	// download tool dependency if need
	if(vscode.workspace.workspaceFolders){
		let workDnldByIde = vscode.workspace.workspaceFolders[0].uri.fsPath;
		if(!utils.workDnldByIde(workDnldByIde as string))
		{
			vscode.window.showQuickPick([
				{description: "Install sdk dependency", label: "Install"},
				{description: "Skip Install sdk dependency", label: "Not Install"},
			])
			.then((installDep)=>{
				if(installDep?.label === "Install")
				{
					utils.checkAndInstallSDKDep(activePrjCfg,activePrjCfg.demoChipBoard);
				}
			});
		}
	}

	updateSdkScopeEnv();

	checkBoardSetFromSdkScopeEnv();
	
	// update env to c_cpp_properties.json
	if(vscode.workspace.workspaceFolders){
		let path = vscode.workspace.workspaceFolders[0].uri.fsPath;
		let vsSetFolder = join(path,'.vscode');
		let ideCppToolFilePath = join(vsSetFolder,cCppPropertiesName);

		// cp c_cpp_properties.json from tool/qccide/c_cpp_properties.json for sdk workspace to .vscode if not exist
		// cp c_cpp_properties.json from ${sdk}/tool/qccide/c_cpp_properties.json for demo to .vscode if not exist
		let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath');
		if(!fs.existsSync(ideCppToolFilePath)){
			let srcPath = join(sdkPath as string,sdkfolderForIde,cCppPropertiesName);
			let destPath = ideCppToolFilePath;
			fs.copyFileSync(srcPath, destPath);
		}
		
		if(fs.existsSync(ideCppToolFilePath)){
			let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath');
			let cfgContent = fs.readFileSync(ideCppToolFilePath).toString();
			let cfgJson = JSON.parse(cfgContent);
			if(sdkPath){
				cfgJson['env']['mySdkPath'] = sdkPath;
			}
			if(sdkScopeEnv && "compiler_path" in sdkScopeEnv)
			{
				cfgJson['env']['myCompilerPath'] = sdkScopeEnv['compiler_path'];
			}
			fs.writeFileSync(ideCppToolFilePath,JSON.stringify(cfgJson,null,2));
		}
	}
	
}


function executeWithVsChannel(cmdStr: string,logging?: vscode.OutputChannel,workspaceFolder?:string) {
	let cmd = cmdStr.split(' ')[0];
	let cmdArray = cmdStr.split(' ');
	const spwanedProcess = cp.spawn(cmd,cmdArray.slice(1, cmdArray.length),{cwd:workspaceFolder});
	console.log(`spawned pid ${spwanedProcess.pid} with command ${cmdStr}`);
	spwanedProcess.stdout.on('data', (data: any) => {
	  console.log(data);
	  logging?.appendLine("stdout:" + data);
	});
	spwanedProcess.stderr.on('data', (data: any) => {
	  console.error(`spawned pid ${spwanedProcess.pid} pushed something to stderr`);
	  logging?.appendLine(data);
	});
  
	spwanedProcess.on('exit', function(code: any) {
	  if (code !== 0) {
		console.log('Failed: ' + code);
	  }
	  else {
		console.log(`pid ${spwanedProcess.pid} finished`);
	  }

	});
}


// https://stackoverflow.com/questions/62224311/vscode-api-run-a-command-in-a-terminal-and-use-output
const execShell = (cmd: string, workspaceFolder?:string) =>
new Promise<string>((resolve, reject) => {
  cp.exec(cmd, {
	  cwd:workspaceFolder
  },(err, out) => {
	if (err) {
	  return resolve(cmd+' error!');
	  //or,  reject(err);
	}
	return resolve(out);
  });
});


/**
 * @description check the path whether valid for the QCC_IDE, assume not has 
 * @param fsPath the path to be check
 */
function isValidProject(fsPath:string,demoPathFromRoot:string, demoFolderName:string){
	if(!isStrDirectory(fsPath)){
        return false;
    }
	if(demoPathFromRoot && isSdkRootPath(fsPath,demoPathFromRoot)){
		return true;
	}
	else if(demoFolderName && isDemoPath(fsPath,demoFolderName)){
		return true;
	}
	else {
		return false;
	}
}


/**
 * @description TODO:get supported chip in local qccIdeProjConfig.json
 * @returns return supported chip
 */
function getSupportedChipName(){
	let path = join(extensionPath,sdkCfgFolderFromExtension);
	if(fs.existsSync(path)){
		let chipList = fs.readdirSync(path).filter(function (file) {
			return fs.statSync(path+'/'+file).isDirectory();
		});
		
		return chipList;
	}
	else{
		return undefined;
	}
}

/**
 * @description get supported Board in ${extensionpath}/database/ide.db
 * @returns return supported Board and its "demoChipBoard" value in ${extensionpath}/database/ide.db
 */
function getSupportedBoardNameFromDb(chipID?:string){

	return new Promise<any>((resolve,reject)=>{
		let sqlQuery;
		if(chipID)
		{
			sqlQuery= `SELECT * FROM BOARDLIST WHERE Chip="${chipID}" `;
		}
		else{
			sqlQuery= `SELECT * FROM BOARDLIST`;
		}

		console.log(sqlQuery);

		let boardNameCache:any = {}; 

		extensionIdeDb.all(sqlQuery,(err:any, rows:any)=>{

			console.log(rows);
			rows.forEach((row:any)=>{
				if(JSON.parse(row.Sdk_Cfg)['cfg']['demoChipBoard'])
				{
					// Just For history usage for QCA402x
					boardNameCache[row.Board] = JSON.parse(row.Sdk_Cfg)['cfg']['demoChipBoard'];
				}else{
					boardNameCache[row.Board] =  row.Board;
				}
			});

			resolve(boardNameCache);
		});
	
	});

}

/**
 * @description get specify Board supported SDK information in ${extensionpath}/database/ide.db
 * @returns return specify Board supported SDK information in ${extensionpath}/database/ide.db
 */
function getBoardSupportedSdkInfoFromDb(boardName:String){

	return new Promise<any>((resolve,reject)=>{

		let sqlQuery = `SELECT * FROM BOARDLIST where Board="${boardName}"`;
		console.log(sqlQuery);

		let sdkInfoCache:any = {}; 

		extensionIdeDb.all(sqlQuery,(err:any, rows:any)=>{

			console.log(rows);
			rows.forEach((row:any)=>{
				// Sdk_Name should be different from each other in BOARDLIST Table in ide.db 
				// since select Board
				sdkInfoCache[row.Sdk_Name] =  JSON.parse(row.Sdk_Cfg);
			});

			resolve(sdkInfoCache);
		});
	
	});
}

/**
 * @description get supported Chip in ${extensionpath}/database/ide.db
 * @returns return supported Chip and its "chipID" value in ${extensionpath}/database/ide.db
 */
function getSupportedChipFromDb(){

	return new Promise<any>((resolve,reject)=>{

		let sqlQuery = `SELECT * FROM BOARDLIST `;
		console.log(sqlQuery);

		let chipIDCache:any = {}; 

		extensionIdeDb.all(sqlQuery,(err:any, rows:any)=>{

			console.log(rows);
			rows.forEach((row:any)=>{
				chipIDCache[row.Chip]['chipid'] =  JSON.parse(row.Sdk_Cfg)['cfg']['chipid'];
				chipIDCache[row.Chip]['chipTarget'] =  JSON.parse(row.Sdk_Cfg)['cfg']['chipTarget'];
			});

			resolve(chipIDCache);
		});
	
	});
}



function getInstalledSdkList(){
	let path = join(ideUserPath, 'Sdks');
	let sdkList = fs.readdirSync(path).filter(function (file) {
		return fs.statSync(path+'/'+file).isDirectory();
	  });
	
	  return sdkList;
}

function getSupportChip(){
	let path = join(extensionPath, 'downloadInstall','downloadJsonConfig','chip');
	let chipList = fs.readdirSync(path).filter(function (file) {
		return fs.statSync(path+'/'+file).isDirectory();
	  });
	
	  return chipList;
}

/**
 * @description update sdk install status with same version
 * @param userSdkJson certain sdk info in user space
 * @param ideSdkJson  certain sdk info in ide space
 */
function updateSdkStatusWithSameVer(userSdkJson:any, ideSdkJson:any){
	let userSdkNames = userSdkJson['names'];
	let ideSdkNames = ideSdkJson['names'];
	for(let i =0; i<ideSdkNames?.length;i++){
		let ideSdkStatus = ideSdkNames[i]['status'];
		let ideSdkName = ideSdkNames[i]['name'];
		for(let j = 0;j<userSdkNames?.length;j++){
			let userSdkStatus = userSdkNames[j]['status'];
			let userSdkName = userSdkNames[j]['name'];
			if(userSdkName === ideSdkName){
				if(ideSdkStatus !== userSdkStatus){
					ideSdkNames[i]['status'] = userSdkStatus;
				}
				break;
			}
		}
	}
}

/**
 * @description merge userJson and ideJson, update userJson value to idejson,and return idejson
 * @param userJson the user json that in user space
 * @param ideJson the ide json thar in ide space
 */
function mergeSdkJson(userJson:any, ideJson:any){

	if('sdks' in userJson && 'sdks' in ideJson){

		if(userJson['version'] >= ideJson['version']){
			return userJson;
		}
		// 1. update sdks install status
		let userSdks = userJson['sdks'];
		let ideSdks = ideJson['sdks'];
		for(let i=0; i < ideSdks?.length;i++){
			let ideSdkversion = ideSdks[i]['version'];
			for(let j =0;j < userSdks?.length;j++){
				let userSdkVersion = userSdks[j]['version'];
				if(ideSdkversion === userSdkVersion){
					updateSdkStatusWithSameVer( userSdks[j],ideSdks[i]);
					break;
				}
			}
		}

	}
    return ideJson;

}

function getSupportSdkList(chipName:string){

	let sdkList = new Array<SdkInfo>(); 
	let sdkInfoItem:SdkInfo ={
		sdkVer:'',
		sdkName:GNAME.noneSdkSupported,
		sdkStatus:''
	};
	
	if(!chipName)
	{
		sdkList.push(sdkInfoItem);
		return sdkList;
	}
	let path = join(extensionPath, 'downloadInstall','downloadJsonConfig','chip',`${chipName}`);
	let userPath = join(ideUserPath,'CfgFiles','downloadJsonConfig','chip',`${chipName}`);
	if(fs.existsSync(userPath) && fs.existsSync(path)){
		
		// compare the file version between user storage and IDE, 
		// if need update, update here, may update the supported sdk list
		let sdkContentIde = fs.readFileSync(join(path,'SDK.json')).toString();
		let sdkContentUser = fs.readFileSync(join(userPath,'SDK.json')).toString();
		if(sdkContentIde===null || sdkContentIde ===undefined || sdkContentIde ===''
	|| sdkContentUser===null || sdkContentUser ===undefined || sdkContentUser ==='')
		{
			sdkList.push(sdkInfoItem);
			return sdkList;
		}
		else{
			let sdkJsonUser = JSON.parse(sdkContentUser);
			let sdkJsonIde = JSON.parse(sdkContentIde);

			if (sdkJsonUser['version'] < sdkJsonIde['version']){
				let resultJson = mergeSdkJson(sdkJsonUser,sdkJsonIde);
				let cfgStr = JSON.stringify(resultJson,null,2);
				fs.writeFileSync(join(userPath,'SDK.json'),cfgStr);
			}
		}

		path = userPath;
	}

	if(fs.statSync(path).isDirectory()){
		let sdkContent = fs.readFileSync(join(path,'SDK.json')).toString();
		if(sdkContent===null || sdkContent ===undefined || sdkContent ==='')
		{
			sdkList.push(sdkInfoItem);
			return sdkList;
		}
		else{
			let sdkJson = JSON.parse(sdkContent);
			let sdks = sdkJson['sdks'];
			for(let i=0; i < sdks?.length;i++){
				let version = sdks[i]['version'];
				let sdkNames = sdks[i]['names'];
				for(let k = 0; k<sdkNames.length; k++){
					if(sdkNames[k]['download_cmd_or_url'] && sdkNames[k]['download_cmd_or_url'] !== 'null'){
						let sdkInfoItemValid:SdkInfo ={
							sdkVer:version,
							sdkName:sdkNames[k]['name'],
							sdkStatus:sdkNames[k]['status']
						};
						sdkList.push(sdkInfoItemValid);
					}
				}
			}
			if(sdkList.length>0){
				return sdkList;
			}	
			else{
				sdkList.push(sdkInfoItem);
				return sdkList;
			}
		}
	}
	else{
		sdkList.push(sdkInfoItem);
		return sdkList;
	}
	
}


/**
 * @description open the project
 * @param projPath the Uri of project
 */
export async function openProjectFolder(projUri:vscode.Uri, newWindow:boolean){

	await vscode.commands.executeCommand("vscode.openFolder", projUri,newWindow).then( ()=>{

		// reinit here for the case: we open the same floder as we have already in
		if(vscode.workspace.workspaceFolders && projUri.fsPath === vscode.workspace.workspaceFolders[0].uri.fsPath){
			vscode.window.withProgress({cancellable: false,
				location: vscode.ProgressLocation.Notification,
				title: "QCC IDE: Init Project..."},
				(progress,token)=>{
				  return initStaticCfg();
			})
			.then( async ()=>{
				// 1. path may be root sdk path
				// 2. path may be specific demo path
				// this should be placed after initStaticCfg() since need bUpdateLocalStaticCfg be true
				if(vscode.workspace.workspaceFolders){
					let path = vscode.workspace.workspaceFolders[0].uri.fsPath;
					let demo = getDemoNameFromPath(path);
					if(demo !=='')
					{
						vscode.window.showInformationMessage('opened demo:' + demo);
						let paramName = 'qualIoe.staticCfg.defaultDemo';
						const target = qualConfig.readParameter("qualIoe.commonCfg.saveScope");
						
						await qualConfig.writeParameter(
							paramName,
							demo,
							target as vscode.ConfigurationTarget
						);
					}
				}

				ideEnvInit();

			});
		
		}
	});
}


/**
 * @description check ramdump files are ready
 * @param path the ramdump folder path
 */
function ramdumpFileOK(path:string):boolean{
	let files = fs.readdirSync(path,{withFileTypes:true})
	.filter(file => file.isFile()).map(file => file.name);

	let chipTarget  = qualConfig.readParameter('qualIoe.staticCfg.chipTarget');

	switch(chipTarget){

		case 'QCA402X':
			if( files.includes(RAMDUMP.quartzM4) &&
			files.includes(RAMDUMP.quartzM0) &&
			files.includes(RAMDUMP.quartzXIP) &&
			files.includes(RAMDUMP.quartzInfo) &&
			files.includes(RAMDUMP.quartzLoad)){
			   return true;
			}
			break;

		default:
			break;
	}
	return false;

}

/**
 * @description copy ramdump files to target sdk path
 * @param path the ramdump folder path
 */
function cpRamdumpFiles(path:string){
	let files = fs.readdirSync(path,{withFileTypes:true})
	.filter(file => file.isFile()).map(file => file.name);

	let chipTarget  = qualConfig.readParameter('qualIoe.staticCfg.chipTarget');

	files.forEach((fileName)=>{
		let srcPath = join(path,fileName);
		let destPath ;
		
		switch(chipTarget){

			case 'QCA402X':
				let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath');
				destPath = join(sdkPath as string,'quartz/gdb',fileName);
				break;

			default:
				break;
		}
		if(destPath && fs.lstatSync(srcPath as string).isFile()){
			utils.cpFileForce(srcPath, destPath);
		}
	});
}

/**
 * @description copy elf file to target sdk path
 */
function cpAppElf(){
	let elfPath: string = 'qualIoe.staticCfg.elfPath';
	let chipTarget  = qualConfig.readParameter('qualIoe.staticCfg.chipTarget');
	let elfName: string = 'qualIoe.staticCfg.elfName';
	
	let preElfName = qualConfig.readParameter(elfName);
	let preElfPath = qualConfig.readParameter(elfPath);
	let destPath,srcPath ;
	if(preElfName && preElfPath){
		switch(chipTarget){

			case 'QCA402X':
				let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath');
				srcPath = join(preElfPath as string, preElfName as string);
				destPath = join(sdkPath as string,'quartz/gdb',preElfName as string);

				if(destPath && fs.lstatSync(srcPath as string).isFile()){
					utils.cpFileForce(srcPath, destPath);
				}

				break;

			default:
				vscode.window.showWarningMessage("chip target is invalid when copy elf file.");
				console.error("chip target is invalid when copy elf file.");
				break;
		}
	}

}

function runRamdump(){

	if (ramdumpTerminal === undefined) {

		let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath') as string;
		
		let nameInEnv;
		let selfEnv:JSON|undefined = utils.getSdkEnv(sdkPath as string);
		if (process.platform === "win32") {
		nameInEnv = "Path";
		} else {
		nameInEnv = "PATH";
		}
		let extEnv = utils.appendPathToEnv(nameInEnv,selfEnv);

		//   here need replace backslash to slash
		sdkPath = sdkPath.replace(/\\/g, "/");
		extEnv['SDK'] = sdkPath as string;

		let chipTarget  = qualConfig.readParameter('qualIoe.staticCfg.chipTarget');

		switch(chipTarget){

			case 'QCA402X': 
				let currentWorkspace = join(sdkPath as string,'quartz/gdb');

				ramdumpTerminal = vscode.window.createTerminal({
				name: "Ramdump console",
				shellArgs: [],
				shellPath: vscode.env.shell,
				cwd:currentWorkspace,
				env:extEnv,
				//Note: use self env
				strictEnv: false,
				});
				
				let gdbWithPy = join(extensionGdbWithPyPath,'arm-none-eabi-gdb.exe');
				// call arm-none-eabi-gdb
				ramdumpTerminal.sendText(` ${gdbWithPy} -x app_ramdump.gdbinit Quartz.elf  \n`);
				ramdumpTerminal.show();

				break;

			default:
				vscode.window.showWarningMessage("chip target is invalid when run ramdump.");
				console.error("chip target is invalid when run ramdump.");
				break;
		}
      }
	  else
	  {
		vscode.window.showInformationMessage('ramdump Terminal has been opened before, may close it first.');
	  }
}

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {


	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "QCA-IDE" is now active!');

	//Note: checek ide dependency first
	utils.checkIdeDep();

	extensionPath = context.extensionPath;
	extensionPYdebugBackendPath = join(extensionPath,'debugger_adapter_backend','backend_main.py');
	extensionPYDownloadPath = join(extensionPath,'downloadInstall','python','download_install.py');
	extensionGdbWithPyPath = join(extensionPath,'ramdump','gdbWithPython');
	extensionDbPath = join(extensionPath,extensionDbRelativePath);
	extensionIdeDb = new sqlite3.Database(extensionDbPath,sqlite3.OPEN_READONLY);
	ideUserPath = join(os.homedir(),ideName);
	ideUserCfgFolderPath = join(ideUserPath,'CfgFiles');
	ideUserDownloadCfgFolderPath = join(ideUserCfgFolderPath,'download');
	ideUserDownloadCfgURIFolderPath = join(ideUserDownloadCfgFolderPath,'URI');
	ideUserSdkDownloadFolderPath = join(ideUserPath,'Sdks');
	ideUserPyVenvFolderPath = join(ideUserPath,'pyvenv');

	if(!fs.existsSync(ideUserPath)){
		fs.mkdir(ideUserPath, (err) => {
			if (err) {
				return console.error(err);
			}
			console.log('Directory created successfully!');
			});
	}

	if(!fs.existsSync(ideUserDownloadCfgFolderPath)){
		fs.mkdir(ideUserDownloadCfgFolderPath, { recursive: true },(err) => {
			if (err) {
				return console.error(err);
			}
			console.log('Directory created successfully!');
			});
	}

	if(!fs.existsSync(ideUserDownloadCfgURIFolderPath)){
		fs.mkdir(ideUserDownloadCfgURIFolderPath, { recursive: true },(err) => {
			if (err) {
				return console.error(err);
			}
			console.log('Directory created successfully!');
			});
	}

	if(!fs.existsSync(ideUserPyVenvFolderPath)){
		fs.mkdir(ideUserPyVenvFolderPath, (err) => {
			if (err) {
				return console.error(err);
			}
			console.log('Directory created successfully!');
			});
	}

	// check python exist and its version, and install module
	let pythonBinPathForIde = qualConfig.readParameter('qualIoe.commonCfg.pythonBinPath');
	if(!pythonBinPathForIde){
		let pythonOutput = vscode.window.createOutputChannel('install python');
		let requirementPath = join(extensionPath,'downloadInstall','python');
		vscode.window.withProgress({
		cancellable: false,
		location: vscode.ProgressLocation.Notification,
		title: "Install python venv..."},
		(progress,token)=>{
			pythonOutput.clear();
			pythonOutput.show();
			return utils.checkAndInstallPyVenv(ideUserPyVenvFolderPath,requirementPath,pythonOutput);
	
		});
	}else{
		// vscode.window.showInformationMessage('commonCfg.pythonBinPath not empty.');
	}

	const registerExtensionCommand = (
		name: string,
		callback: (...args: any[]) => any
	  ): number => {
		const telemetryCallback = (...args: any[]): any => {
		  const startTime = Date.now();
		  const cbResult = callback.apply(null, args);
		  const timeSpent = Date.now() - startTime;
		  return cbResult;
		};
		return context.subscriptions.push(
		  vscode.commands.registerCommand(name, telemetryCallback)
		);
	  };
	
	/**
	* Start of event callback
	*/

	// fired when close terminal
	vscode.window.onDidCloseTerminal(async (terminal: vscode.Terminal) => {
		const terminalPid = await terminal.processId;

		if(terminalPid === -1)
		{
			return;
		}

		const consoleDownloadTerminalPid:number|undefined= consoleDownloadTerminal
		? await consoleDownloadTerminal.processId
		: -1;
		const consoleRamdumpTerminalPid:number|undefined= ramdumpTerminal
		? await ramdumpTerminal.processId
		: -1;
		
		// kill download console
		if (consoleDownloadTerminalPid === terminalPid) {
			consoleDownloadTerminal = undefined;
			if(consoleDownloadTerminalPid !== undefined)
			{
				kill(consoleDownloadTerminalPid, "SIGKILL");
			}
		}

		// kill ramdump connsole
		if (consoleRamdumpTerminalPid === terminalPid) {
			ramdumpTerminal = undefined;
			if(consoleRamdumpTerminalPid !== undefined)
			{
				kill(consoleRamdumpTerminalPid, "SIGKILL");
			}
		}


	});
	// update the local static project configure file
	vscode.workspace.onDidChangeConfiguration(event => {
		let affectedStaticCfg = event.affectsConfiguration("qualIoe.staticCfg");
		if (affectedStaticCfg && bUpdateLocalStaticCfg) {
			console.log("Setting changed from vscode setting UI, saved to configure file on disk ");
			let config = vscode.workspace.getConfiguration('qualIoe').get('staticCfg');
			if(config)
			{
				let myCfg = JSON.parse(JSON.stringify(config));

				if(vscode.workspace.workspaceFolders !== undefined)
				{
					// the path of static project configure file
					let path = vscode.workspace.workspaceFolders[0].uri.fsPath;
					path = utils.getStaticCfgFileLocation(path);
					try{
						let cfgContent = fs.readFileSync(path).toString();
						if(!cfgContent)
						{
							return;
						}
						let cfgJson = JSON.parse(cfgContent);
						// Should just change the active prj cfg
						if(cfgJson)
						{
							let activePrjIndex = utils.getActivePrjIndex(cfgJson);

							utils.updateJson(cfgJson[activePrjIndex],myCfg);

							// update activePrjCfg & allPrjCfg here according setting
							allPrjCfg = cfgJson;
							utils.updateJson(activePrjCfg,myCfg);

							// update active demoChipBoard to all project
							// TODO: ALL project share same cfg
							utils.updateAllBoardCfg(cfgJson,activePrjCfg.demoChipBoard);

							let cfgStr = JSON.stringify(cfgJson,null,2);
							fs.writeFileSync(path, cfgStr);
								//file written successfully
						}
					}catch (err){
						console.error(err);
					}
					
				}
			}
		}
	});

	/**
	* End of event callback
	*/

	// display and register command bar items
	creatCmdsStatusBarItems();
	
	/**
	 * start of init 
	 */
	// TODO: put in vscode.window.withProgress
	vscode.window.withProgress({cancellable: false,
		location: vscode.ProgressLocation.Notification,
		title: "QCC IDE: Init Project..."},
		(progress,token)=>{
		  return initStaticCfg();
	})
	.then(async ()=>{
		// 1. path may be root sdk path
		// 2. path may be specific demo path
		// this should be placed after initStaticCfg() since need bUpdateLocalStaticCfg be true
		if(vscode.workspace.workspaceFolders){
			let path = vscode.workspace.workspaceFolders[0].uri.fsPath;
			let demo = getDemoNameFromPath(path);
			if(demo !=='')
			{
				vscode.window.showInformationMessage('opened demo:' + demo);
				let paramName = 'qualIoe.staticCfg.defaultDemo';
				const target = qualConfig.readParameter("qualIoe.commonCfg.saveScope");
				
				await qualConfig.writeParameter(
					paramName,
					demo,
					target as vscode.ConfigurationTarget
				);
			}

			ideEnvInit();
		}
	});

	
	/**
	 * end of init
	 */


	/**
	 * Start of command register
	 */
	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json

	registerExtensionCommand(
		'qualIoe.helloWorld',
		()=>{
			// The code you place here will be executed every time your command is executed
			// Display a message box to the user
			vscode.window.showInformationMessage('Hello World from Qualcomm IOE Embedded device IDE!');
		}
	);


	registerExtensionCommand(
		'qualIoe.selectPort',
		async ()=>{

			SerialPort.list()
			.then((ports) => {

				let portItemList :vscode.QuickPickItem[] = [] ;

				ports.map((port) => {
					portItemList.push({label: port.path, description: port.manufacturer});
				});

				vscode.window.showQuickPick(portItemList).then(
					async function(chosenPort){
						if (chosenPort !== undefined)
						{
							let chosenPortArray = chosenPort.label.match(/COM\d{1,3}/ig);
							if(chosenPortArray)
							{
								let chosenPortStr = chosenPortArray[0];
									// add com port to config
								let paramName:string = '';
								if(os.platform() === 'win32')
								{
									paramName = 'qualIoe.commonCfg.consolePortWin';
								}
								else
								{
									paramName = 'qualIoe.commonCfg.consolePortLinux';
								}
								const target = qualConfig.readParameter("qualIoe.commonCfg.saveScope");
								await qualConfig.writeParameter(
									paramName,
									chosenPortStr,
									target as vscode.ConfigurationTarget
								);
								console.log("chosen Port: "+ chosenPortStr);
								vscode.window.showInformationMessage("chosen Port: "+ chosenPortStr);
							}
						}
					}
				);
			});
		}
	);


	registerExtensionCommand(
		'qualIoe.config',
		()=>{

			// open the setting editor of vscode
			// TODO:rich config
			vscode.window.showInformationMessage('Rich config is not support yet, use settings first.');
		}
	);

	registerExtensionCommand(
		'qualIoe.installDep',
		()=>{
			utils.checkAndInstallSDKDep(activePrjCfg,activePrjCfg.demoChipBoard);
			updateSdkScopeEnv();
		}
	);


	registerExtensionCommand(
		'qualIoe.cleanImage' ,
		async (item:vscode.TreeItem)=>{

			if(isTaskRunning())
			{
				return;
			}

			let prjName;
			if(item)
			{
				// click from ideWorkspace view
				prjName= item.label;
				// 1. whether need change active project
				if(prjName !== activePrjCfg.prjName)
				{
					// 2. If need, update according vscode settings
					await initStaticCfg(prjName as string);
				}
			}
			prjName = activePrjCfg.prjName;

			let chipID  = qualConfig.readParameter('qualIoe.staticCfg.chipID');
			let chipTarget  = qualConfig.readParameter('qualIoe.staticCfg.chipTarget');
			let sdkParentDir = qualConfig.readParameter('qualIoe.staticCfg.sdkParentDir');
			let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath');
			let chipVersion  = qualConfig.readParameter('qualIoe.staticCfg.chipVersion');
			let chipBoard  = qualConfig.readParameter('qualIoe.staticCfg.demoChipBoard');

			if(!checkSdkPath(sdkPath as string,sdkParentDir as string))
			{
				return;
			}

			vscode.window.showInformationMessage('Clean ' + prjName + ' for '+chipID + ' ' + chipBoard);
			console.log('[Command:qualIoe.cleanImage] read chipID: '+ chipID );

			let activePrjCfgCopy = utils.parseJsonWithVar(activePrjCfg);

			if(os.platform() === 'win32')
			{	
				let cleanCmd = activePrjCfgCopy.CLEAN_IMAGE_PATTERN.win[PrjCfgCleanTypeIndex.cleanIndex];
				let buildPath = activePrjCfgCopy.buildRunPath;
				if(!cleanCmd)
				{
					vscode.window.showWarningMessage('The project ' + prjName +' do not support clean.');
					return;
				}
				if(vscode.workspace.workspaceFolders){
					buildPath = join(vscode.workspace.workspaceFolders[0].uri.fsPath, buildPath);
					const buildTask = new CommandTask();
					await buildTask.addCommandTask(buildPath as string, cleanCmd,'clean image',sdkScopeEnv);

					await runCmdTasks();
				}
			}
			else{

			}
		}
	);

	registerExtensionCommand(
		'qualIoe.buildImage',
		async (item:vscode.TreeItem)=>{

			if (isTaskRunning())
			{
				return;
			}

			let prjName;
			if(item)
			{
				// click from ideWorkspace view
				prjName= item.label;
				// 1. whether need change active project
				if(prjName !== activePrjCfg.prjName)
				{
					// 2. If need, update according vscode settings
					await initStaticCfg(prjName as string);
				}
			}
			prjName = activePrjCfg.prjName;

			let chipID  = qualConfig.readParameter('qualIoe.staticCfg.chipID');
			let chipTarget  = qualConfig.readParameter('qualIoe.staticCfg.chipTarget');
			let chipBoard  = qualConfig.readParameter('qualIoe.staticCfg.demoChipBoard');
			let sdkParentDir = qualConfig.readParameter('qualIoe.staticCfg.sdkParentDir');
			let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath');
			let demoName = qualConfig.readParameter('qualIoe.staticCfg.defaultDemo');

			if(!sdkPath || !checkSdkPath(sdkPath as string,sdkParentDir as string))
			{
				vscode.window.showWarningMessage('sdkPath is not valid: ' + sdkPath);
				return;
			}

			vscode.window.showInformationMessage('Build '+ prjName +' for '+chipID +' '+ chipBoard);
			console.log('[Command:qualIoe.buildImage] read chipID: '+ chipID );
			console.log('[Command:qualIoe.buildImage] read sdkPath: '+ sdkPath );

			let activePrjCfgCopy = utils.parseJsonWithVar(activePrjCfg);

			// TODO: let choose build type, build, rebuild
			if(os.platform() === 'win32')
			{	
				let buildCmd = activePrjCfgCopy.BUILD_IMAGE_PATTERN.win[PrjCfgBuildTypeIndex.buildIndex];
				let buildPath = activePrjCfgCopy.buildRunPath;
				if(!buildCmd)
				{
					vscode.window.showWarningMessage('The project ' + prjName +' do not support build.');
					return;
				}
				if(vscode.workspace.workspaceFolders){
					buildPath = join(vscode.workspace.workspaceFolders[0].uri.fsPath, buildPath);
					const buildTask = new CommandTask();
					await buildTask.addCommandTask(buildPath as string, buildCmd,'build image',sdkScopeEnv);

					await runCmdTasks();
				}
			}
			else{

			}
		}
	);


	registerExtensionCommand(
		'qualIoe.flash',
		async (item:vscode.TreeItem)=>{
			if (isTaskRunning())
			{
				return;
			}

			let prjName;
			if(item)
			{
				// click from ideWorkspace view
				prjName= item.label;
				// 1. whether need change active project
				if(prjName !== activePrjCfg.prjName)
				{
					// 2. If need, update according vscode settings
					await initStaticCfg(prjName as string);
				}
			}
			prjName = activePrjCfg.prjName;

			let chipID  = qualConfig.readParameter('qualIoe.staticCfg.chipID');
			let chipTarget  = qualConfig.readParameter('qualIoe.staticCfg.chipTarget');
			let chipBoard  = qualConfig.readParameter('qualIoe.staticCfg.demoChipBoard');
			let chosenPort ='';
			let sdkParentDir = qualConfig.readParameter('qualIoe.staticCfg.sdkParentDir');
			let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath');
			let demoName = qualConfig.readParameter('qualIoe.staticCfg.defaultDemo');

			if(!checkSdkPath(sdkPath as string,sdkParentDir as string))
			{
				return;
			}

			vscode.window.showInformationMessage('Flash ' + prjName + ' for '+chipID +' '+ chipBoard);
			console.log('[Command:qualIoe.flash] read chipID: '+ chipID );
			console.log('[Command:qualIoe.flash] read sdkPath: '+ sdkPath );
			
			switch(chipTarget){
				// TODO: move auto detect port for QCA402X to itself SDK.
				case 'QCA402X':
					
					let script:string = '';
					let cmdStr = '';
					let flashPort = '';

					if(os.platform() === 'win32')
					{
						let pythonPath = qualConfig.readParameter('qualIoe.commonCfg.pythonBinPath');
						let cmdStr  = '-m serial.tools.list_ports -v';
			
						const output = await execShell(`${pythonPath} ${cmdStr}`);
						let portList = output.split('\n');
			
						for(let i =0 ;i<portList.length - 3;i=i+3){
			
							if(portList[i+1].indexOf(GNAME.quartzFlashUsbName)!== -1){
								let flashPortArray = portList[i+1].match(/COM\d{1,3}/ig);
								if(!flashPortArray)
								{
									vscode.window.showInformationMessage('QDLoader port is not valid when flash.' );
									console.error('QDLoader port is not valid when flash.');
									return ;
								}
								flashPortArray = flashPortArray[0].match(/\d{1,3}$/);
								if(flashPortArray){
									flashPort = flashPortArray[0];
								}
								break;
							}
						}
					}
					else
					{
						vscode.window.showInformationMessage('Flash just support windows for QCA402X.' );
						return;
					}

					if(!flashPort){
						vscode.window.showWarningMessage('Did not find QDLoader port when flash, are you in EDL mode ?' );
						return;
					}else{
							// add com port to config
							let paramName:string = '';
							if(os.platform() === 'win32')
							{
								paramName = 'qualIoe.commonCfg.flashPortWin';
							}
							else
							{
								paramName = 'qualIoe.commonCfg.flashPortLinux';
							}
							const target = qualConfig.readParameter("qualIoe.commonCfg.saveScope");
							await qualConfig.writeParameter(
								paramName,
								flashPort,
								target as vscode.ConfigurationTarget
							);
					}
	
					// qflash.py
					script = qualConfig.readParameter("qualIoe.staticCfg.demoFlashScriptWin") as string;
					//TODO: additional flags
					cmdStr  = 'python ' + script + ' --comm '  + flashPort;

					// get default demo 
					let demoRelativePathToSdkRoot = qualConfig.readParameter('qualIoe.staticCfg.demoPath');
					let buildPath = join(sdkPath as string,demoRelativePathToSdkRoot as string); 
					buildPath = await getDefaultDemo(buildPath,'.*demo$');
					if(!fs.existsSync(buildPath))
					{
						vscode.window.showErrorMessage('The buildPath is not exist: '+ buildPath);
						return;
					}
					buildPath = join(buildPath ,'build','gcc');

					// check if qflash.py exist, if not copy it 
					let flashScript = join(buildPath,'qflash.py');
					if(!fs.existsSync(flashScript))
					{	
						let srcPath = join(sdkPath as string,'build','tools','flash','qflash.py');
						fs.copyFile(srcPath,flashScript,(error)=>{
							if(error)
							{
								console.error('copy qflash.py failed:',error);
								// without qflash.py, we just return
								return;
							}
						});
					}

					// flash just use python itself, do not need other third party package
					let selfEnv:JSON|undefined = utils.getSdkEnv(sdkPath as string);
					const buildTask = new CommandTask();
					await buildTask.addCommandTask(buildPath as string, cmdStr,'flash demo',selfEnv);

					await runCmdTasks();

					break;

				default:
					// TODO: let choose flash type, flash, flash reset
					let activePrjCfgCopy = utils.parseJsonWithVar(activePrjCfg);
					
					let reset = qualConfig.readParameter("qualIoe.commonCfg.resetAfterFlash") as boolean;
					if(os.platform() === 'win32')
					{	
						let flashCmd = activePrjCfgCopy.FLASH_PATTERN.win[PrjCfgFlashTypeIndex.flashIndex];
						if(reset)
						{
							flashCmd = activePrjCfgCopy.FLASH_PATTERN.win[PrjCfgFlashTypeIndex.flashResetIndex];
						}
						if(!flashCmd)
						{
							vscode.window.showWarningMessage('The project ' + prjName +' do not support flash.');
							return;
						}
						let flashPath = activePrjCfgCopy.flashRunPath;
						if(vscode.workspace.workspaceFolders){
							flashPath = join(vscode.workspace.workspaceFolders[0].uri.fsPath, flashPath);
							const buildTask = new CommandTask();
							await buildTask.addCommandTask(flashPath as string, flashCmd,'flash image',sdkScopeEnv);

							await runCmdTasks();
						}
					}
					else{

					}
			}


		}
	);

	registerExtensionCommand(
		'qualIoe.chooseAsActiveWorkspacePrj',
		async (item:vscode.TreeItem)=>{
			
			let prjName;
			if(item)
			{
				// click from ideWorkspace view
				prjName= item.label;
				// 1. always need change active project
		
				// 2. If need, update according vscode settings
				await initStaticCfg(prjName as string);
				vscode.window.showInformationMessage('Choose ' + prjName +' as active Project.');
			}
		}
	);

	registerExtensionCommand(
		'qualIoe.collapseAllWorkspaceEntry',
		async ()=>{
			vscode.commands.executeCommand('workbench.actions.treeView.ideWorkspace.collapseAll');
		}
	);

	registerExtensionCommand(
		'qualIoe.showActiveWorkspaceEntry',
		async ()=>{
			let prjName = activePrjCfg.prjName;
			vscode.window.showInformationMessage('Active project is ' + prjName);
		}
	);


	registerExtensionCommand(
		'qualIoe.console',
		()=>{
			
			createPortConsole();
		}
	);


	registerExtensionCommand(
		'qualIoe.flashAndConsole',
		()=>{
			// The code you place here will be executed every time your command is executed
			// Display a message box to the user
			vscode.window.showInformationMessage('Not implemented');
		}
	);


	registerExtensionCommand(
		'qualIoe.setPath',
		async ()=>{
			// set SDK path
			const option = await vscode.window.showQuickPick(
				[
				  { description: "Set the absolute Sdk Path", label: "SDK_PATH", target: "sdkPath" },
				  { description: "Set the absolute tools Path", label: "TOOLS_PATH", target: "toolsPath" },

				  {
					description: "Set paths to append to PATH",
					label: "Custom extra paths",
					target: "customExtraPath",
				  },

				],
			  );

			if (!option) {
				console.log("No option selected.");
				return;
			}
			
			let msg: string | undefined;
			let paramName: string | undefined;   // must be dot-separated identifier for configure
			let currentValue;
			switch (option.target) {
				case "sdkPath":
					msg = "Enter SDK_PATH Path";
					paramName = "qualIoe.staticCfg.sdkPath";      // this should be same with package.json
					break;

				case "toolsPath":
					msg = "Enter TOOLS_PATH path";
					paramName = "qualIoe.commonCfg.toolsPath";
					break;
			
				case "customExtraPath":
					msg = "Enter extra paths to append to PATH";
					paramName = "qualIoe.commonCfg.customExtraPaths";
					break;

				default:
					msg = "No path has been updated";
					console.error("set path error !");
					break;
			}

			if (msg && paramName) {
				currentValue = qualConfig.readParameter(paramName);
				console.log("current SDK path: "+currentValue);
				await qualConfig.updateConfParameter(
				  paramName,
				  msg,
				  currentValue,
				  option.label
				);
			}
		}
	);

	
	registerExtensionCommand(
		'qualIoe.setChip',
		async()=>{

			// get supported chip ID list from  activePrjCfg & show to user to choose
			let chipIdList = utils.getChipIdList(activePrjCfg);
			let chosenChipId ;
			if(chipIdList)
			{
				chosenChipId = await vscode.window.showQuickPick(chipIdList);
			}
			else{
				vscode.window.showWarningMessage("This project do not support setChip.");
			}

			let paramName: string = "qualIoe.staticCfg.chipID"; 
			if(chosenChipId)
			{
				const target = qualConfig.readParameter("qualIoe.commonCfg.saveScope");

				// set chip ID
				await qualConfig.writeParameter(
					paramName,
					chosenChipId,
					target as vscode.ConfigurationTarget
				);
				// TODO: call the interface to set to project
				
				// set chip target
			 	paramName = "qualIoe.staticCfg.chipTarget";
				let chipTarget = activePrjCfg.chipTarget; 
				await qualConfig.writeParameter(
				paramName,
				chipTarget,
				target as vscode.ConfigurationTarget
				);

				vscode.window.showInformationMessage("chosen chip: "+ chosenChipId);
			}
		}
	);

	registerExtensionCommand(
		'qualIoe.setConsoleBaudrate',
		async()=>{
			let baudrateList = qualConfig.readParameter('qualIoe.commonCfg.consolePortBaudrateList') as Array<number>;
			if(baudrateList)
			{
				const boundRateItems: vscode.QuickPickItem[] = baudrateList.map((value) => { return { label: value.toString() }; });
				let boudRate = await vscode.window.showQuickPick(boundRateItems, { placeHolder: ("please select a boud rate") });

				if(boudRate?.label)
				{
					let paramName = 'qualIoe.commonCfg.consolePortBaudrate';
					const target = qualConfig.readParameter("qualIoe.commonCfg.saveScope");
					await qualConfig.writeParameter(
						paramName,
						boudRate?.label,
						target as vscode.ConfigurationTarget
					);
				}
			}
	});


	registerExtensionCommand(
		'qualIoe.setActiveProject',
		async ()=>{

			let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath');
			console.log('[Command:qualIoe.setActiveProject] read sdkPath: '+ sdkPath );
			let chipTarget  = qualConfig.readParameter('qualIoe.staticCfg.chipTarget');
			let sdkParentDir = qualConfig.readParameter('qualIoe.staticCfg.sdkParentDir');
			
			if(!checkSdkPath(sdkPath as string,sdkParentDir as string))
			{
				return;
			}
			
			if(allPrjCfg)
			{
				let prjName = utils.getPrj(allPrjCfg);
				vscode.window.showQuickPick(prjName).then(
					(chosenPrj)=>{
						if(chosenPrj){
							let activePrj =  chosenPrj as string;
							// 1. whether need change active project
							if(activePrj !== activePrjCfg.prjName)
							{
								vscode.window.showInformationMessage('Active project changed to ' + activePrj);
								// 2. If need, update according vscode settings
								initStaticCfg(activePrj as string);
							}
						}
				});
			}
	
		}
	);

	registerExtensionCommand(
		'qualIoe.setBoard',
		async (provideBoardName)=>{		
			let choosenBoardName;	
			if(!provideBoardName)
			{
				// get supported board list from  activePrjCfg & show to user to choose
				let boardNameList = utils.getBoardList(activePrjCfg);
				choosenBoardName = await vscode.window.showQuickPick(boardNameList);
			}else{
				choosenBoardName = provideBoardName;
			}

			let paramName = 'qualIoe.staticCfg.demoChipBoard';
			let prjName = activePrjCfg.prjName;

			if(choosenBoardName)
			{
				let boardName :string = choosenBoardName as string;

				if(os.platform() === 'win32')
				{	
					let setCmd = activePrjCfg.SET_CFG_PATTERN.win[PrjCfgSetTypeIndex.boardIndex];
					let sdkPath = qualConfig.readParameter('qualIoe.staticCfg.sdkPath');
					let buildPath = activePrjCfg.buildRunPath;
					if(!setCmd)
					{
						vscode.window.showWarningMessage('The project ' + prjName +' do not support set on SDK level.');
						return;
					}

					let activePrjCfgCopy = JSON.parse(JSON.stringify(activePrjCfg));
					activePrjCfgCopy.demoChipBoard = boardName;
					activePrjCfgCopy = utils.parseJsonWithVar(activePrjCfgCopy);
					setCmd = activePrjCfgCopy.SET_CFG_PATTERN.win[PrjCfgSetTypeIndex.boardIndex];

					if(vscode.workspace.workspaceFolders){
						buildPath = join(vscode.workspace.workspaceFolders[0].uri.fsPath, buildPath);
						const buildTask = new CommandTask();
						await buildTask.addCommandTask(buildPath as string, setCmd,'set board',sdkScopeEnv);
	
						runCmdTasks()
						.then(async ()=>{
							
							vscode.window.showInformationMessage(`Set board to ${boardName} for ${prjName}`);
							const target = qualConfig.readParameter("qualIoe.commonCfg.saveScope");
							await qualConfig.writeParameter(
								paramName,
								boardName,
								target as vscode.ConfigurationTarget
							);
							// update activePrjCfg.demoChipBoard here 
							activePrjCfg.demoChipBoard = boardName;
						});
					}
				}
				else{
	
				}

			}
		}
	);

	registerExtensionCommand(
		'qualIoe.setGdbServer',
		async ()=>{
	
			let gdbServerNameList = ['openocd','jlink'];
			let choosenGdbServer = await vscode.window.showQuickPick(gdbServerNameList);

			let paramName = 'qualIoe.commonCfg.defaultGdbServer';

			if(choosenGdbServer)
			{
			
				vscode.window.showInformationMessage(`Set Gdb server to ${choosenGdbServer}`);
				const target = qualConfig.readParameter("qualIoe.commonCfg.saveScope");
				await qualConfig.writeParameter(
					paramName,
					choosenGdbServer,
					target as vscode.ConfigurationTarget
				);
			}

		}
	);

	registerExtensionCommand('qualIoe.readMem',async ()=>{
		// show inputbox to let user imput address
		let args  = await vscode.window.showInputBox({placeHolder:'address and bytes count to read,like \'&v 100\'',ignoreFocusOut:true});

		if(args && vscode.debug.activeDebugSession && vscode.debug.activeDebugSession.type === 'qcaIde' ){
			await vscode.debug.activeDebugSession.customRequest('readMem',args);
		}
		else{
			vscode.window.showInformationMessage('No debug session is available or need input address to read.');
		}

	});

	registerExtensionCommand('qualIoe.readDisassemble',async ()=>{
		let args  = await vscode.window.showInputBox({placeHolder:'address to read disassemble,like \'func_name\'',ignoreFocusOut:true});

		if(args && vscode.debug.activeDebugSession && vscode.debug.activeDebugSession.type === 'qcaIde'){
			await vscode.debug.activeDebugSession.customRequest('readDisassemble',args);
		}
		else{
			vscode.window.showInformationMessage('No debug session is available or need input address to read.');
		}

	});

	registerExtensionCommand('qualIoe.importRamdump',async ()=>{
		// customize choose project
		const options: vscode.OpenDialogOptions = {
			canSelectMany: false,
			openLabel: 'Select ramdump folder', 
			canSelectFiles: false,
			canSelectFolders: true,
			defaultUri: Uri.file(GNAME.ramdumpQPSTLogPath)
		};
		await vscode.window.showOpenDialog(options).then(async (fileUri) => {
			if (fileUri && fileUri[0]) {
				// 1. check ramdump
				if(!ramdumpFileOK(fileUri[0].fsPath)){
					vscode.window.showErrorMessage('ramdump files are not ready in: ' + fileUri[0].fsPath);
					console.error('ramdump files are not ready in: ' + fileUri[0].fsPath);
					return;
				}

				// 2. copy ramdump
				cpRamdumpFiles(fileUri[0].fsPath);

				// 3. copy elf 
				cpAppElf();

				// 4. run ramdump cmommand
				runRamdump();

			}
		});

	});

	registerExtensionCommand('qualIoe.openNewProject',async ()=>{
		IdeHomePanel.render(context.extensionUri);
	});

	registerExtensionCommand('qualIoe.openExistQcaProject',async ()=>{
		

		// customize choose project
		const options: vscode.OpenDialogOptions = {
			canSelectMany: false,
			openLabel: 'Select Qcc project folder',
			canSelectFiles: false,
			canSelectFolders: true
		};
		vscode.window.showOpenDialog(options).then(async (fileUri) => {
			if (fileUri && fileUri[0]) {
				console.log('Selected project: ' + fileUri[0].fsPath);

				let projCfgJsonFilePath = utils.getStaticCfgFileLocation(fileUri[0].fsPath);
				if(!fs.existsSync(projCfgJsonFilePath)){
					vscode.window.showWarningMessage('project is not valid: ' +  fileUri[0].fsPath);
					return;
				}

				// open in new window directly
				await openProjectFolder(fileUri[0],true);
				
			}
		});
	});

	// walkthrough command start

	// walkthrough command stop


	/**
 	* End of command register
	*/

	/**
	 * activate debug adapter 
	 */

	 activateQcaDebugAdapter(context);
	 
	/**
	 * end of activate debug adapter 
	 */


	/**
	 * activate ide explorer
	 */

	new IdeWorkspace(context);

	/**
	 * end of activate ide explorer
	 */

}

// this method is called when your extension is deactivated
export function deactivate() {
	for (const statusItem of statusBarItems) {
		statusItem.dispose();
	  }
}
