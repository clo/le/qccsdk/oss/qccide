/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

/*
 * Project: ESP-IDF VSCode Extension
 * File Created: Friday, 27th September 2019 9:59:57 pm
 * Copyright 2019 Espressif Systems (Shanghai) CO LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as vscode from 'vscode'; 
import{GNAME} from '../config/globalName';


export class TaskManager {
    private static tasks: vscode.Task[] = [];
    private static disposables: vscode.Disposable[] = [];
    public static isRunning: boolean = false;

    public static setRunning(flag: boolean) {
      this.isRunning = flag;
    }

    public static addTask(
      taskDefinition: vscode.TaskDefinition,
      scope: vscode.TaskScope,
      name: string,
      execution:
        | vscode.ShellExecution
        | vscode.ProcessExecution
        | vscode.CustomExecution,
      problemMatchers: string | string[],
      revealTask: vscode.TaskRevealKind
    ) {

      if(this.isRunning)
      {
        return new Promise((resolve, reject) => {
              return reject(new Error("Intend to add task when is running"));
        });
      }

      const newTask: vscode.Task = new vscode.Task(
        taskDefinition,
        scope,
        name,
        GNAME.extensionID,
        execution,
        problemMatchers
      );
      newTask.presentationOptions = {
        reveal: revealTask,
      };
      TaskManager.tasks.push(newTask);
      console.log('push new task '+ name +' in task manager');
      return new Promise<void>((resolve, reject) => {
        vscode.tasks.onDidEndTask((e) => {
          if (e.execution.task.name === newTask.name) {
            return resolve();
          }
        });
      });
    }

    public static disposeListeners() {

      if(this.isRunning)
      {
        throw new Error('Intend to dispose tasks when running');
      }

      for (const disposable of TaskManager.disposables) {
        disposable.dispose();
      }
      TaskManager.disposables = [];
    }

    public static cancelTasks() {
        for (const task of TaskManager.tasks) {
          const execution = vscode.tasks.taskExecutions.find((t) => {
            return t.task.name === task.name;
          });
          if (execution) {
            execution.terminate();
          }
        }
        TaskManager.tasks = [];
    }

    public static async runTasks() {
        return new Promise<void>(async (resolve, reject) => {
          this.setRunning(true);
          if (TaskManager.tasks && TaskManager.tasks.length >= 1) {
            let lastExecution = await vscode.tasks.executeTask(
              TaskManager.tasks[0]
            );
            let lastTask = lastExecution.task;
            for (let i = 0; i < TaskManager.tasks.length; i++) {
              const taskDisposable = vscode.tasks.onDidEndTaskProcess(async (e) => {
                if (
                  TaskManager.tasks.length > 0 &&
                  e.execution.task.name === lastTask.name
                ) {
                  if (e.exitCode !== 0) {
                    console.log('The task run failed:', e.execution.task.name);
                    vscode.window.showErrorMessage(`Task ${lastTask.name} exited with code ${e.exitCode}`);
                    this.setRunning(false);
                    this.cancelTasks();
                    this.disposeListeners();
                    return reject(
                      new Error(
                        `Task ${lastTask.name} exited with code ${e.exitCode}`
                      )
                    );
                  }
                  // if is the final task
                  if (
                    e.execution.task.name ===
                    TaskManager.tasks[TaskManager.tasks.length - 1].name
                  ) {
                    TaskManager.tasks = [];
                    this.setRunning(false);
                    e.execution.terminate();
                    this.disposeListeners();
                    return resolve();
                  } else {
                    // must note overflow
                    if((i+1)< TaskManager.tasks.length)
                    {
                      lastExecution = await vscode.tasks.executeTask(
                        TaskManager.tasks[i+1]
                      );
                    }
                    lastTask = lastExecution.task;
                  }
                }
              });
              TaskManager.disposables.push(taskDisposable);
            }
          }
        });
      }
}