/* * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved. * SPDX-License-Identifier: BSD-3-Clause-Clear */

import * as path from 'path';
import {isStrDirectory} from '../utils/check';

export function is402xProj(fsPath:string){
    if(!isStrDirectory(fsPath)){
        return false;
    }
    if(is402xDemoProj(fsPath) ){
            return true;
    }
    else if(is402xSdkProj(fsPath)){
        return true;
    }

    return false;
    
}

export function is402xDemoProj(fsPath:string){

    if(fsPath.indexOf('quartz\\demo')!==-1 || fsPath.indexOf('quartz/demo')!==-1 ){
            return true;
    }

    return false;
    
}

export function is402xSdkProj(fsPath:string){

    if(isStrDirectory(path.join(fsPath,'quartz\\demo')) || isStrDirectory(path.join(fsPath,'quartz/demo')) ){
        return true;
    }

    return false;
}

export function getSdkFrom402xDemoPath(fsPath:string){

    let reg = new RegExp(`.*(?=quartz)`);
    let path  = fsPath.match(reg);

    if(path && path[0]){
        return path[0];
    }
    else {
        return '';
    }
    
}
